Engen Project
=============

This is the initial proposal for the **Engen Project** and other resources.

Please note that this software, resources and everything on this site is published for academic research purpose only.

For more details please see the [Wiki pages](https://gitlab.com/engen/public/wikis/home)

