package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.db.Index;
import mddprocessorplugin.db.IndexColumn;
import mddprocessorplugin.model.util.StringUtils;

import org.apache.commons.collections.CollectionUtils;

public class Entity extends BaseModel {

	transient org.slf4j.Logger log = org.slf4j.LoggerFactory
			.getLogger(Entity.class);
	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -7859359356792891833L;

	/*
	 * If defined, this entity should be generated as abstract class. This
	 * overrides the default project level behavior.
	 */
	private Boolean isEntityAbstract = Defaults.IS_ENTITIES_ABSTRACT;
	/*
	 * If defined, this entity should be generated as interfaces. This overrides
	 * the default project level behavior.
	 */
	private Boolean isInterface = Boolean.FALSE;
	/*
	 * If the this entity is abstract, this is the implementation suffix. This
	 * overrides the default project level behavior.
	 */
	private String entityImplementationSufix = StringUtils.EMPTY;
	/**
	 * This is the class name this class extends
	 */
	private String extendsClass = StringUtils.EMPTY;
	private List<String> imports = new ArrayList<String>();
	private String pkfield = StringUtils.EMPTY;
	private String module;
	/* This property is cached and read only. */
	private String namespace = StringUtils.EMPTY;
	/*
	 * Used in pages here a default representation is needed as in combo boxes
	 * or search pages
	 */
	private String defaultAttribute = StringUtils.EMPTY;
	private List<Attribute> attributes = new ArrayList<Attribute>();
	private List<Association> associations = new ArrayList<Association>();
	/* Define if this entity has multiple primary keys */
	private Boolean isCompositePrimaryKey = Boolean.FALSE;
	/* The table name for this entity for easy access */
	private String tableName;
	/** The Model Layer this entity is attached to **/
	private ModelLayer modelLayer;
	/** The list of interfaces this entity implements **/
	private List<String> superInterfaceNames;
	/** The access modifier for this entity **/
	private AccessModifier accessModifier = Defaults.DEFAULT_ACCESS_MODIFIER;
	/**
	 * If this class is a supper class of other class or classes this should be
	 * set to true. In the project configuration the hierarchy mapping type will
	 * decide what to implement based on the type hierarchy.
	 */
	private boolean supperClass;
	/**
	 * Defines the inheritanceStrategy for this particular entity as opposed in
	 * the project configuration. Thus the strategy can be changed after entity
	 * creation.
	 */
	private String inheritanceStrategy;
	private boolean superClassFromModel;
	/**
	 * Indicates whether this entity as created as a base class for a
	 * Implementation entity.
	 */
	private boolean hasDelegate;
	/**
	 * The documentation provided in the DSL. Can be used by templates
	 */
	private String documentation;
	/**
	 * These are ordered group names as defined in the DSL.
	 */
	private List<String> groups = new ArrayList<String>();
	/**
	 * Business keys defined for this entity
	 */
	private List<BusinessKey> businessKeys = new ArrayList<BusinessKey>();

	public Entity() {
		this.setStereotypeName("entity");
		setBusinessKeys(new ArrayList<BusinessKey>());
	}

	@SuppressWarnings("unchecked")
	public Entity(final Table t) {
		this();
		Entity.setNameWithSubstitutions(this, t);
		this.setTableNameWithScape(t);
		List<Column> columns = t.getColumns();
		Collections.sort(t.getColumns(), new Column.ColumnComparator());
		List<Attribute> primaryKeys = new ArrayList<Attribute>();
		for (final Column c : columns) {
			// TODO: Added this. Think the foreign key attribute
			// is good.
			final Attribute att = new Attribute(this, c);
			// If this is a foreign key
			if (hasForeignKeyForColumn(t, c)) {
				// in this case this attribute should not appear on views
				att.setShowOnView(Boolean.FALSE);
				// indicates that this attribute is originated from a foreign
				// key.
				att.setForeignKey(Boolean.TRUE);
			}
			if (att.isPrimaryKey()) {
				// If the attribute is a primary key then lets decide later
				primaryKeys.add(att);
			} else {
				// Will add an attribute to the attributes collection.
				this.addAttribute(att);
			}
		}
		if (primaryKeys.size() == 1) {
			this.addAttribute(primaryKeys.get(0));
		} else if (primaryKeys.size() > 1) {
			// If more than on primary key was found then set this entity as
			// a composite entity. Hence this will not be a first class entity
			this.setIsCompositePrimaryKey(true);
			// for (Attribute att : primaryKeys) {
			// // This should be an embedded id in other
			// // entity.
			// // TODO: Composite primary keys handling. See
			// // GEraDaoTableCookieImpl
			// }
		}

		// Relations. If a foreign key exists it has already been added.
		// Now lets include fields referencing other tables.
		List<Relation> relations = t.getRelations();
		for (Relation r : relations) {
			final Attribute att = new Attribute(this, r);
			this.addAttribute(att);
			// final Association ass = new Association(this, r);
			// this.addAssociation(ass);
		}

		// Define the default attribute.
		this.setDefaultAttributeForEntity();

		// Defining which attributes are business keys.

		List<Index> indexes = t.getIndexes();
		for (Index i : indexes) {
			// If this index is a unique index.
			if (i.isUnique()) {
				BusinessKey key = new BusinessKey();
				// Look for a match with an attribute in this entity.
				List<IndexColumn> ics = i.getIndexColumns();
				for (IndexColumn ic : ics) {
					Attribute at = this.getAttribute(ic.getName());
					if (at != null) {
						// If a match is found this attribute is a business key.
						at.setBusinessKey(Boolean.TRUE);
						at.setIndexName(ic.getName());
					}
					key.addAttribute(at);
				}
				this.addBusinessKey(key);
			} else {
				// Look for a match with an attribute in this entity.
				List<IndexColumn> ics = i.getIndexColumns();
				for (IndexColumn ic : ics) {
					Attribute at = this.getAttribute(ic.getName());
					if (at != null) {
						// If a match is found this attribute has an index.
						at.setIndexName(ic.getName());
					}
				}
			}
		}
	}

	public void addBusinessKey(BusinessKey key) {
		key.setParent(this);
		this.getBusinessKeys().add(key);
		this.notifyModelChangeListeners();
	}

	private void setTableNameWithScape(Table t) {
		// String mySQLEscape = "\u0027";
		// String mySQLEscape = "\u00B4";
		// String mySQLEscape = "\u0060";
		// String oracleEscape = "\"";
		// if (StringUtils.indexOf(t.getDatabase().getDialect(), "MySQL") > 0) {
		// this.setTableName(mySQLEscape + t.getName() + mySQLEscape);
		// } else if (StringUtils.indexOf(t.getDatabase().getDialect(), "MySQL")
		// > 0) {
		// this.setTableName(oracleEscape + t.getName() + oracleEscape);
		// } else {
		// this.setTableName(t.getName());
		// }
		String quote = "";
		this.setTableName(quote + t.getName() + quote);
	}

	/**
	 * Sets the entity name using substitution keys on database.
	 * 
	 * @param entity
	 *            the entity to set the name
	 * @param t
	 *            the table from which the name is taken
	 */
	private static void setNameWithSubstitutions(Entity entity, Table t) {
		Database db = t.getDatabase();
		// Get the substitution keys map.
		Map<String, String> substitutionsMap = Entity.getSubstitutionsMap(db);
		// The proposed name
		String proposedName = t.getName();
		// The new name
		String newName = proposedName;
		// for each key found in the table name make the substitution
		// with the value.
		for (String key : substitutionsMap.keySet()) {
			newName = Entity.makeSubstitution(proposedName, key,
					substitutionsMap.get(key));
		}
		// Transform to camel case the name (this_example -> thisExample)
		newName = StringUtils.toCamelCase(newName);
		entity.setName(newName);
	}

	private static Map<String, String> getSubstitutionsMap(Database db) {
		String substitutionString = db.getSubstitutionKeys();
		Map<String, String> substitutionMap = new HashMap<String, String>();
		if (StringUtils.trimToNull(substitutionString) != null) {
			String[] substitutionArray = substitutionString.split(",");
			for (int i = 0; i < substitutionArray.length; i += 2) {
				String key = substitutionArray[i];
				if (StringUtils.trimToNull(key) == null) {
					// If the key is empty jump to the next. There is no sence
					// in
					// replace empty strings to something.
					continue;
				}
				// Verify if the array have an element to substitute for
				if (substitutionArray.length >= i + 1) {
					String value = substitutionArray[i + 1];
					if (StringUtils.trimToNull(value) != null) {
						if ("''".equals(value)) {
							value = StringUtils.EMPTY;
						}
						// Create a new entry
						substitutionMap.put(key, value);
					}
				}
			}
		}
		return substitutionMap;
	}

	private static String makeSubstitution(String proposedName, String key,
			String value) {
		return StringUtils.replace(proposedName, key, value);
	}

	/**
	 * Set the default attribute for this entity. It will be the first column in
	 * the table after the primary keys.
	 */
	public void setDefaultAttributeForEntity() {
		Attribute proposed = null;
		List<Attribute> allAttributes = this.getAllAttributes();
		for (Attribute att : allAttributes) {
			// Set one if it doesn't exists yet.
			if (proposed == null) {
				proposed = att;
			}
			if (!att.isPrimaryKey()) {
				if (att.getOrdinalPosition() < proposed.getOrdinalPosition()) {
					proposed = att;
				} else if (proposed.isPrimaryKey()) {
					proposed = att;
				}
			}
		}
		if (proposed != null) {
			this.setDefaultAttribute(proposed.getName());
		}
	}

	public Attribute getAttribute(String name) {
		if (!BeanUtil.isNested(name)) {
			// This attribute is not nested, so look it up in
			// the current entity
			for (Attribute att : this.getAllAttributes()) {
				if (att.getName() != null && att.getName().equals(name)) {
					return att;
				}
			}
		} else {
			String[] props = StringUtils.split(name, '.');
			List<String> nestedProperty = new ArrayList<String>();
			CollectionUtils.addAll(nestedProperty, props);
			// Removes and return the first nested call
			String firstNestedCall = nestedProperty.remove(0);
			Attribute first = this.getAttribute(firstNestedCall);
			if (first == null) {
				return first;
			} else {
				// Found an attribute with the first token. Ask this attribute
				// its target. It should be a reference to another entity which
				// should contain the remaining part of nested name
				BaseModel target = first.getTarget();
				if (target == null) {
					// The target is not specified for this attribute.
					// So can't have a nested property claiming another
					// attribute.
					throw new IllegalArgumentException("The attribute " + first
							+ ", for entity " + this
							+ ", doesn't have a target defined."
							+ "So it can't have nested property calls.");
				}
				if (!Entity.class.isAssignableFrom(target.getClass())) {
					throw new IllegalArgumentException("The nested attribute "
							+ first + ", for entity " + this
							+ ", is not of Entity type.");
				}
				Entity entityRef = (Entity) target;
				// Ask this entity reference the remaining of nested call
				String remainingNestedCall = org.apache.commons.lang.StringUtils
						.join(nestedProperty, ".");
				return entityRef.getAttribute(remainingNestedCall);
			}
		}
		return null;
	}

	/**
	 * Returns an attribute that is the default attribute or the primary key for
	 * this entity.
	 * 
	 * @return the default attribute or the primary key
	 */
	public Attribute getDefaultOrKeyAttribute() {
		String strAttribute = this.getDefaultAttribute();
		if (StringUtils.isNotEmpty(strAttribute)) {
			Attribute att = this.getAttribute(strAttribute);
			return att;
		}
		return this.getPrimaryKeyAttribute();
	}

	/**
	 * Returns the attribute that is the primary key
	 * 
	 * @return the primary key for this entity
	 */
	public Attribute getPrimaryKeyAttribute() {
		Attribute pk = null;
		for (Attribute att : this.getAllAttributes()) {
			if (att.isPrimaryKey()
					&& (pk != null && att.getName().equals(pk.getName()))) {
				log.info("Found another primary key for this entity ["
						+ this.getName() + "] with the same name.");
				throw new IllegalArgumentException("We can't have an entity ["
						+ this.getName()
						+ "] with two attributes with the same name ["
						+ att.getName() + "]");
			}
			if (att.isPrimaryKey()) {
				pk = att;
			}
		}
		if (pk == null) {
			// Try its super class primary key
			Entity extend = this.getExtendEntity();
			// Verify if this entity is a subclass. If this is the
			// case then pick up the upper (super) class primary key
			if (extend != null) {
				pk = extend.getPrimaryKeyAttribute();
			}
		}
		return pk;
	}

	/**
	 * Returns the entity that is the super class for this class
	 * 
	 * @return the super class
	 */
	public Entity getExtendEntity() {
		if (StringUtils.isEmpty(this.getExtendsClass())) {
			return null;
		}
		if (this.getModelLayer() == null) {
			return null;
		}
		Entity extend = this.getModelLayer().getEntityByName(
				this.getExtendsClass());
		return extend;
	}

	private boolean hasForeignKeyForColumn(Table t, Column c) {
		for (ForeignKey fk : t.getForeignKeys()) {
			if (StringUtils.trimToEmpty(fk.getFkColumn()).equals(c.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Dynamically compute all attributes that are originated from a relation
	 * 
	 * @return map keyed by the attribute name.
	 */
	public Map<String, Attribute> getRelationAttributes() {
		Map<String, Attribute> relationAttributes = new HashMap<String, Attribute>();
		for (Attribute att : this.getAttributes()) {
			if (att.getFromRelation()) {
				relationAttributes.put(att.getName(), att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Dynamically compute all attributes that are originated from a relation
	 * The difference with the first method is that this computs all attribute
	 * including the ones from its supper classes
	 * 
	 * @return map keyed by the attribute name.
	 */
	public Map<String, Attribute> getAllRelationAttributes() {
		Map<String, Attribute> relationAttributes = new HashMap<String, Attribute>();
		for (Attribute att : this.getAllAttributes()) {
			if (att.getFromRelation()) {
				relationAttributes.put(att.getName(), att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Dynamically compute all attributes that are originated from a relation
	 * 
	 * @return list of relation attributes.
	 */
	public List<Attribute> getRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		for (Attribute att : this.getAttributes()) {
			if (att.getFromRelation()) {
				relationAttributes.add(att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Dynamically compute all attributes that are originated from a relation.
	 * It will order the fields such as the many-to-one relations comes first
	 * then the one-to-many relation fields.
	 * 
	 * @return list of relation attributes.
	 */
	public List<Attribute> getAllRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		relationAttributes.addAll(this.getAllManyToOneRelationAttributesList());
		relationAttributes.addAll(this.getAllOneToManyRelationAttributesList());
		return relationAttributes;
	}

	/**
	 * Dynamically compute all attributes that are NOT originated from a
	 * relation
	 * 
	 * @return map keyed by the attribute name.
	 */
	public Map<String, Attribute> getNonRelationAttributes() {
		Map<String, Attribute> relationAttributes = new HashMap<String, Attribute>();
		for (Attribute att : this.getAllAttributes()) {
			if (!att.getFromRelation() && !att.isPrimaryKey()) {
				relationAttributes.put(att.getName(), att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Dynamically compute all attributes that are NOT originated from a
	 * relation
	 * 
	 * @return list of relation attributes.
	 */
	public List<Attribute> getNonRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		for (Attribute att : this.getAllAttributes()) {
			if (!att.getFromRelation() && !att.isPrimaryKey()) {
				relationAttributes.add(att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as many-to-one relations
	 * 
	 * @return many-to-one attributes
	 */
	public Map<String, Attribute> getManyToOneRelationAttributes() {
		Map<String, Attribute> relationAttributes = new HashMap<String, Attribute>();
		for (Attribute att : this.getRelationAttributes().values()) {
			if (StringUtils.equals(att.getCardinality(),
					RelationCardinality.MANY_TO_ONE.toString())) {
				relationAttributes.put(att.getName(), att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as many-to-one relations
	 * 
	 * @return many-to-one attributes
	 */
	public List<Attribute> getManyToOneRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		for (Attribute att : this.getRelationAttributes().values()) {
			if (StringUtils.equals(att.getCardinality(),
					RelationCardinality.MANY_TO_ONE.toString())) {
				relationAttributes.add(att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as many-to-one relations. The
	 * difference with the first method is that this computes all attribute
	 * including the ones from its supper classes
	 * 
	 * @return many-to-one attributes
	 */
	public List<Attribute> getAllManyToOneRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		for (Attribute att : this.getAllRelationAttributes().values()) {
			if (StringUtils.equals(att.getCardinality(),
					RelationCardinality.MANY_TO_ONE.toString())) {
				relationAttributes.add(att);
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as one-to-many relations
	 * 
	 * @return one-to-many attributes
	 */
	public Map<String, Attribute> getOneToManyRelationAttributes() {
		Map<String, Attribute> relationAttributes = new HashMap<String, Attribute>();
		Map<String, Attribute> thisRelationAttributes = this
				.getRelationAttributes();
		if (thisRelationAttributes != null) {
			for (Attribute att : this.getRelationAttributes().values()) {
				if (StringUtils.equals(att.getCardinality(),
						RelationCardinality.ONE_TO_MANY.toString())) {
					relationAttributes.put(att.getName(), att);
				}
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as one-to-many relations
	 * 
	 * @return one-to-many attributes
	 */
	public List<Attribute> getOneToManyRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		Map<String, Attribute> thisRelationAttributes = this
				.getRelationAttributes();
		if (thisRelationAttributes != null) {
			for (Attribute att : this.getRelationAttributes().values()) {
				if (StringUtils.equals(att.getCardinality(),
						RelationCardinality.ONE_TO_MANY.toString())) {
					relationAttributes.add(att);
				}
			}
		}
		return relationAttributes;
	}

	/**
	 * Returns all attributes that are marked as one-to-many relations The
	 * difference with the first method is that this computes all attribute
	 * including the ones from its supper classes
	 * 
	 * @return one-to-many attributes
	 */
	public List<Attribute> getAllOneToManyRelationAttributesList() {
		List<Attribute> relationAttributes = new ArrayList<Attribute>();
		for (Attribute att : this.getAllRelationAttributes().values()) {
			if (StringUtils.equals(att.getCardinality(),
					RelationCardinality.ONE_TO_MANY.toString())) {
				relationAttributes.add(att);
			}
		}
		return relationAttributes;
	}

	/**
	 * @return the isEntityAbstract
	 */
	public Boolean getIsEntityAbstract() {
		return isEntityAbstract;
	}

	/**
	 * @param isEntityAbstract
	 *            the isEntityAbstract to set
	 */
	public void setIsEntityAbstract(final Boolean isEntityAbstract) {
		this.isEntityAbstract = isEntityAbstract;
	}

	/**
	 * @return the entityImplementationSufix
	 */
	public String getEntityImplementationSufix() {
		return entityImplementationSufix;
	}

	/**
	 * @param entityImplementationSufix
	 *            the entityImplementationSufix to set
	 */
	public void setEntityImplementationSufix(
			final String entityImplementationSufix) {
		this.entityImplementationSufix = entityImplementationSufix;
	}

	/**
	 * @return the extendsClass
	 */
	public String getExtendsClass() {
		return extendsClass;
	}

	/**
	 * @param extendsClass
	 *            the extendsClass to set
	 */
	public void setExtendsClass(final String extendsClass) {
		this.extendsClass = extendsClass;
	}

	public String getNamespace() {
		final StereotypedItem parent = this.getParent();
		if (parent instanceof ModelPackage) {
			final ModelPackage upperPackage = (ModelPackage) parent;
			namespace = upperPackage.getNamespace();
		}
		return namespace;
	}

	public void setNamespace(final String value) {
		this.namespace = value;
	}

	/**
	 * @return the imports
	 */
	public List<String> getImports() {
		return imports;
	}

	public void addImport(String importStr) {
		if (!this.getImports().contains(importStr)) {
			// only add this import if it doesn't exists already
			this.getImports().add(importStr);
		}

	}

	/**
	 * @param imports
	 *            the imports to set
	 */
	public void setImports(final List<String> imports) {
		this.imports = imports;
	}

	/**
	 * @return the pkfield
	 */
	public String getPkfield() {
		return pkfield;
	}

	/**
	 * @param pkfield
	 *            the pkfield to set
	 */
	public void setPkfield(final String pkfield) {
		this.pkfield = pkfield;
	}

	/**
	 * @return the module
	 */
	public String getModule() {
		if (!StringUtils.isEmpty(module)) {
			return module;
		} else {
			// try the namespace
			if (!StringUtils.isEmpty(this.getNamespace())) {
				this.module = this.getNamespace();
			}
			return this.module;
		}

	}

	/**
	 * @param module
	 *            the module to set
	 */
	public void setModule(final String module) {
		this.module = module;
	}

	/**
	 * @return the attributes
	 */
	public List<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void setAttributes(final List<Attribute> attributes) {
		for (final Attribute att : attributes) {
			att.setParent(this);
		}
		this.attributes = attributes;
		this.notifyModelChangeListeners();
	}

	public void addAttribute(final Attribute att) {
		if (att == null) {
			throw new IllegalArgumentException(
					"The attribute to add can't be null");
		}
		if (this.hasAttribute(att.getName())) {
			throw new IllegalArgumentException("This entity [" + this.getName()
					+ "] already has an attribute with this name ["
					+ att.getName() + "]. Please change the name first.");
		}
		att.setParent(this);
		this.getAttributes().add(att);
		this.notifyModelChangeListeners();
	}

	public void removeAttribute(final Attribute att) {
		this.getAttributes().remove(att);
		this.notifyModelChangeListeners();
	}

	@Override
	public List<BaseModel> getChildren() {
		List<BaseModel> children = new ArrayList<BaseModel>();
		children.addAll(this.getAttributes());
		// children.addAll(this.getAssociations());
		return children;
	}

	/**
	 * @param defaultAttribute
	 *            the defaultAttribute to set
	 */
	public void setDefaultAttribute(String defaultAttribute) {
		this.defaultAttribute = defaultAttribute;
	}

	/**
	 * @return the defaultAttribute
	 */
	public String getDefaultAttribute() {
		return defaultAttribute;
	}

	/**
	 * @return the isCompositePrimaryKey
	 */
	public Boolean getIsCompositePrimaryKey() {
		return isCompositePrimaryKey;
	}

	/**
	 * @param isCompositePrimaryKey
	 *            the isCompositePrimaryKey to set
	 */
	public void setIsCompositePrimaryKey(Boolean isCompositePrimaryKey) {
		this.isCompositePrimaryKey = isCompositePrimaryKey;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName
	 *            the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * The Model Layer this entity is attached to
	 * 
	 * @param modelLayer
	 *            the modelLayer to set
	 */
	public void setModelLayer(ModelLayer modelLayer) {
		this.modelLayer = modelLayer;
	}

	/**
	 * Recursively goes up in the hierarchy and when the model layer is found
	 * return it.
	 * 
	 * @param item
	 *            the item we want the model layer for. Usually this
	 * @return the model layer the item belongs to.
	 */
	private ModelLayer getModelLayerFor(StereotypedItem item) {
		if (item != null) {
			if (item instanceof ModelLayer) {
				return (ModelLayer) item;
			} else {
				return this.getModelLayerFor(item.getParent());
			}
		}
		return null;
	}

	/**
	 * The Model Layer this entity is attached to
	 * 
	 * @return the modelLayer
	 */
	public ModelLayer getModelLayer() {
		if (modelLayer == null) {
			// try its parent
			this.modelLayer = this.getModelLayerFor(this);
		}
		return modelLayer;
	}

	/**
	 * @return the associations
	 */
	public List<Association> getAssociations() {
		return associations;
	}

	/**
	 * @param associations
	 *            the associations to set
	 */
	public void setAssociations(List<Association> associations) {
		for (Association association : associations) {
			association.setParent(this);
		}
		this.associations = associations;
		this.notifyModelChangeListeners();
	}

	/**
	 * Adds an association to the entity
	 * 
	 * @param association
	 */
	public void addAssociation(Association association) {
		association.setParent(this);
		this.getAssociations().add(association);
		this.notifyModelChangeListeners();
	}

	public void removeAssociation(Association association) {
		this.getAssociations().remove(association);
		this.notifyModelChangeListeners();
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof Attribute) {
			this.addAttribute((Attribute) child);
			return true;
		}
		if (child instanceof Association) {
			this.addAssociation((Association) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof Attribute) {
			this.removeAttribute((Attribute) child);
			return true;
		}
		if (child instanceof Association) {
			this.removeAssociation((Association) child);
			return true;
		}
		return false;
	}

	/**
	 * @param superInterfaceNames
	 */
	public void setSuperInterfaceNames(String[] superInterfaceNames) {
		this.superInterfaceNames = Arrays.asList(superInterfaceNames);
	}

	public List<String> getSuperInterfaceNames() {
		return superInterfaceNames;
	}

	public void setSuperInterfaceNames(List<String> superInterfaceNames) {
		this.superInterfaceNames = superInterfaceNames;
	}

	public AccessModifier getAccessModifier() {
		return accessModifier;
	}

	public void setAccessModifier(AccessModifier accessModifier) {
		this.accessModifier = accessModifier;
	}

	public Boolean getIsInterface() {
		return isInterface;
	}

	public void setIsInterface(Boolean isInterface) {
		this.isInterface = isInterface;
	}

	/**
	 * @param supperClass
	 *            the supperClass to set
	 */
	public void setSupperClass(boolean supperClass) {
		this.supperClass = supperClass;
	}

	/**
	 * @return the supperClass
	 */
	public boolean isSupperClass() {
		return supperClass;
	}

	/**
	 * @param inheritanceStrategy
	 *            the inheritanceStrategy to set
	 */
	public void setInheritanceStrategy(String inheritanceStrategy) {
		this.inheritanceStrategy = inheritanceStrategy;
	}

	/**
	 * Gets the inheritance strategy used for this entity and its children
	 * 
	 * @return the inheritanceStrategy
	 */
	public String getInheritanceStrategy() {
		return inheritanceStrategy;
	}

	/**
	 * Returns all attributes of this entity including the ones in its
	 * hierarchy, ordered by their ordinal position.
	 * 
	 * @return all attributes for this entity including the inherited
	 */
	public List<Attribute> getAllAttributes() {
		List<Attribute> all = new ArrayList<Attribute>();
		// First check if this entity extends other
		if (this.getExtendEntity() != null) {
			// Get all attributes from the extended entity
			List<Attribute> extendedAttributes = this.getExtendEntity()
					.getAllAttributes();
			// Adds all attributes up in the hierarchy recursively
			all.addAll(extendedAttributes);
		}
		// All super classes attributes added. Add this class attributes
		all.addAll(this.getAttributes());
		// Sort attributes by their ordinal position
		Collections.sort(all);
		return all;
	}

	/**
	 * Returns whether this entity has an attribute with the given name
	 * 
	 * @param name
	 * @return
	 */
	public boolean hasAttribute(String name) {
		return this.getAttribute(name) != null;
	}

	/**
	 * Creates a default primary key attribute with the given name.
	 * 
	 * @param name
	 *            the name for this primary key attribute.s
	 */
	public void createDefaultPrimaryKeyAttributeForEntity(String name) {
		Attribute pk = Attribute.createDefaultAttribute(name,
				AccessModifier.PRIVATE);
		pk.setPrimaryKey(true);
		pk.setBusinessKey(true);
		pk.setRequired(true);
		pk.setIndexName(pk.getName());
		pk.setMaxDisplaySize("11");
		pk.setType("java.lang.Integer");
		pk.setTypeImpl(pk.getType());
		addAttribute(pk);
	}

	public void setSuperClassFromModel(boolean superClassFromModel) {
		this.superClassFromModel = superClassFromModel;
	}

	public boolean isSuperClassFromModel() {
		return superClassFromModel;
	}

	public String getDocumentation() {
		return documentation;
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}

	public boolean isHasDelegate() {
		return hasDelegate;
	}

	public void setHasDelegate(boolean hasDelegate) {
		this.hasDelegate = hasDelegate;
	}

	public void addGroup(String name) {
		this.groups.add(name);
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public List<BusinessKey> getBusinessKeys() {
		return businessKeys;
	}

	public void setBusinessKeys(List<BusinessKey> businessKeys) {
		this.businessKeys = businessKeys;
	}

}
