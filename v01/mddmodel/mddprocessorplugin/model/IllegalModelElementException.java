package mddprocessorplugin.model;

public class IllegalModelElementException extends IllegalArgumentException {

	public IllegalModelElementException(StereotypedItem model) {
		super("The model element: "
					+ model + " doesn't have a parent element. This is illegal in the current model version.");
	}
}
