package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.views.MddProcessorFixedProjectTreeViewHandler;

public class DataLayer extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -4385674950625737071L;
	private String name;
	private String namespace = Defaults.DATALAYER_NAMESPACE;
	private List<Database> databases = new ArrayList<Database>();

	public DataLayer() {
		this.setStereotypeForThis();
	}
	
	public DataLayer(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
	}

	private void setStereotypeForThis() {
		this.setStereotypeName("datalayer");
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public void addDatabase(Database database) {
		database.setParent(this);
		this.getDatabases().add(database);
		this.notifyModelChangeListeners();
	}
	
	public void removeDatabase(Database database) {
		if (this.databases.contains(database)) {
			this.databases.remove(database);
			this.notifyModelChangeListeners();
		}
	}

	/**
	 * @return the databases
	 */
	public List<Database> getDatabases() {
		return databases;
	}

	/**
	 * @param databases
	 *            the databases to set
	 */
	public void setDatabases(List<Database> databases) {
		for(Database database : databases) {
			database.setParent(this);
		}
		this.databases = databases;
		this.notifyModelChangeListeners();
	}

	@Override
	public List<Database> getChildren() {
		return this.getDatabases();
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Database) {
			this.addDatabase((Database) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Database) {
			this.removeDatabase((Database) child);
			return true;
		}
		return false;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

}
