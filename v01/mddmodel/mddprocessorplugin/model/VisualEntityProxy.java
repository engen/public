/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/
package mddprocessorplugin.model;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import org.eclipse.ui.views.properties.IPropertyDescriptor;

/**
 * This is a place holder for an entity inside a diagram. It will delegate all
 * method calls to the enclosing entity but will have its own BaseVisualModel so
 * it can have its own bounds and location.
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 3, 2009
 */
public class VisualEntityProxy extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -207634754268186539L;
	/** The real entity **/
	private Entity entity = new Entity();

	/** The visual properties of this entity are different from the real entity **/

	/**
	 * Creates a new visual entity proxy with a dummy entity.
	 */
	public VisualEntityProxy() {
		super("visualentityproxy");
	}

	/**
	 * Creates a new visual entity proxy and sets its proxied entity object as
	 * the parameter
	 * 
	 * @param proxied
	 *            the entity to be proxied
	 */
	public VisualEntityProxy(Entity proxied) {
		super("visualentityproxy");
		this.setEntity(proxied);
	}

	/**
	 * Sets the entity to be proxied by this visual entity proxy. If the entity
	 * is not added to the model layer already it will be added.
	 * 
	 * @param entity
	 *            the entity to set
	 */
	public void setEntity(Entity entity) {
		ModelLayer modelLayer = this.getModelLayer();
		if (!modelLayer.hasEntity(entity)) {
			modelLayer.addEntity(entity);
		}
		this.entity = entity;
	}

	/**
	 * Returns the proxied entity. If it doesn't exists a new one will be
	 * created and returned but not added to the model layer.
	 * 
	 * @return the entity proxied or a dummy entity.
	 */
	public Entity getEntity() {
		if (entity == null) {
			entity = new Entity();
		}
		return entity;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		this.getEntity().setPropertyValue(id, value);
	}

	@Override
	public void addModelChangeListener(ModelChangeListener listener) {
		this.getEntity().addModelChangeListener(listener);
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		return this.getEntity().getChildren();
	}

	@Override
	public List<ModelChangeListener> getModelChangeListeners() {
		return this.getEntity().getModelChangeListeners();
	}

	@Override
	public String getName() {
		return this.getEntity().getName();
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		return this.getEntity().getPropertyDescriptors();
	}

	@Override
	public Object getPropertyValue(Object id) {
		return this.getEntity().getPropertyValue(id);
	}

	@Override
	public String getStereotypeName() {
		return this.getEntity().getStereotypeName();
	}

	@Override
	public String getTemplatePath() {
		return this.getEntity().getTemplatePath();
	}

	@Override
	public boolean isPropertySet(Object id) {
		return this.getEntity().isPropertySet(id);
	}

	@Override
	protected synchronized void notifyModelChangeListeners() {
		this.getEntity().notifyModelChangeListeners();
	}

	@Override
	public void removeModelChangeListener(ModelChangeListener listener) {
		this.getEntity().removeModelChangeListener(listener);
	}

	@Override
	public void resetPropertyValue(Object id) {
		this.getEntity().resetPropertyValue(id);
	}

	@Override
	public void setName(String name) {
		this.getEntity().setName(name);
	}

	@Override
	public void setTemplatePath(String templatePath) {
		this.getEntity().setTemplatePath(templatePath);
	}

	@Override
	public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
		this.getEntity().addPropertyChangeListener(l);
	}

	@Override
	protected void firePropertyChange(String property, Object oldValue,
			Object newValue) {
		this.getEntity().firePropertyChange(property, oldValue, newValue);
	}

	@Override
	public synchronized void removePropertyChangeListener(
			PropertyChangeListener l) {
		this.getEntity().removePropertyChangeListener(l);
	}

	/**
	 * Adds an attribute in the enclosing entity.
	 * 
	 * @param att
	 */
	public void addAttribute(Attribute att) {
		this.getEntity().addAttribute(att);
	}

	/**
	 * @return
	 */
	public Map<String, Attribute> getRelationAttributes() {
		return this.getEntity().getRelationAttributes();
	}

	/**
	 * Returns the model layer in the project.
	 * 
	 * @return
	 */
	public ModelLayer getModelLayer() {
		return this.getEnclosingProject().getModelLayer();
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		return this.getEntity().addChild(child);
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		return this.getEntity().removeChild(child);
	}

}