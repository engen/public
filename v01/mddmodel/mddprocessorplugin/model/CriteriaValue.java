package mddprocessorplugin.model;

public class CriteriaValue implements Translatable {

	private String criteriaValue;

	public CriteriaValue(String criteriaValue) {
		this.criteriaValue = criteriaValue;
	}

	@Override
	public String internalValue() {
		return criteriaValue;
	}

}
