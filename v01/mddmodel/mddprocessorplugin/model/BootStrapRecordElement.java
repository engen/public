package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;


public class BootStrapRecordElement {

	private String name;
	private List<BootStrapRecordData> data = new ArrayList<BootStrapRecordData>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<BootStrapRecordData> getData() {
		return data;
	}
	public void setData(List<BootStrapRecordData> data) {
		this.data = data;
	}
	
}
