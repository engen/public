/**
 * 
 */
package mddprocessorplugin.model;

import org.apache.commons.lang.StringUtils;

/**
 * Logical path for a web resource. If inside a ControlModule it is relative to
 * the control module' path.
 * 
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da
 *         Purificacao</a>
 * @created 08/03/2009
 * @version 1.0
 */
public class LogicalPath extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269379080852482303L;

	private Boolean externallyVisible = Boolean.FALSE;
	/**
	 * The ultimate physical path for the resource. This must start with a
	 * forward slash!
	 **/
	private String path = StringUtils.EMPTY;
	/**
	 * This is the real name for the logical path as it will be referenced
	 * inside the application.
	 */
	private String logicalName = StringUtils.EMPTY;
	/** Not used **/
	private String basePath = StringUtils.EMPTY;
	/** The type for the target of the logical path **/
	private String type = LogicalPathType.JSP.toString();
	/** This is the correct resource name like the controller name or view **/
	private String targetResourceName;
	/**
	 * The model representing the target resource. Can only be controllers or
	 * Presentation view model elements.
	 */
	private ModelTarget target;

	public LogicalPath() {
		this.setStereotypeName("logicalpath");
	}

	/**
	 * @param externallyVisible
	 *            the externallyVisible to set
	 */
	public void setExternallyVisible(Boolean externallyVisible) {
		this.externallyVisible = externallyVisible;
	}

	/**
	 * @return the externallyVisible
	 */
	public Boolean getExternallyVisible() {
		return externallyVisible;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param logicalName
	 *            the logicalName to set
	 */
	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}

	/**
	 * @return the logicalName
	 */
	public String getLogicalName() {
		return logicalName;
	}

	/**
	 * @param basePath
	 *            the basePath to set
	 */
	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	/**
	 * @return the basePath
	 */
	public String getBasePath() {
		return basePath;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	public ModelTarget getTarget() {
		return target;
	}

	public void setTarget(ModelTarget target) {
		if (target instanceof PresentationView || target instanceof Controller) {
			this.target = target;
		} else {
			throw new IllegalArgumentException(
					"Target for Local path can be only a PresentationView or controller.");
		}
	}

	public String getTargetResourceName() {
		return targetResourceName;
	}

	public void setTargetResourceName(String targetResourceName) {
		this.targetResourceName = targetResourceName;
	}

	/**
	 * Remove the connection among controller and logical path
	 */
	public void detachSource() {
		Controller controller = (Controller) getParent();
		if (controller != null) {
			controller.removeLogicalPath(this);
		}
		this.setParent(null);
		this.notifyModelChangeListeners();
	}

	/**
	 * Creates a link in the parent controller to this Logical Path
	 */
	public void attachSource() {
		Controller controller = (Controller) getParent();
		if (controller != null) {
			controller.addLogicalPath(this);
			this.notifyModelChangeListeners();
		}
	}

	/**
	 * If the target is a PresentationView then set this LogicalPath as its
	 * parent
	 */
	public void attachTarget() {
		if (this.getTarget() instanceof PresentationView) {
			((PresentationView) this.getTarget()).setParent(null);
			this.notifyModelChangeListeners();
		}
	}

	/**
	 * Removes the target and if it is a PresentationView remove the parent
	 * setting
	 */
	public void detachTarget() {
		if (this.getTarget() instanceof PresentationView) {
			((PresentationView) this.getTarget()).setParent(null);
		}
		this.setTarget(null);
		this.notifyModelChangeListeners();
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}
}
