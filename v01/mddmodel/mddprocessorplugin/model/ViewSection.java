package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import mddprocessorplugin.model.layouts.FieldStyle;
import mddprocessorplugin.model.layouts.LayoutStyle;
import mddprocessorplugin.model.util.StringUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 * Represents a section inside a view
 * 
 * @author eugenio
 * 
 */
public class ViewSection extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 8698314310655878316L;
	private static final int FIELD_BLOCK_SIZE = 1;
	/**
	 * Sections may have sub-sections.
	 */
	private List<ViewSection> subSections = new ArrayList<ViewSection>();
	/**
	 * The section this section extends if the case
	 */
	private ViewSection superSection;
	/**
	 * The section type
	 */
	private ViewSectionType sectionType;
	/**
	 * Special attribute label for this section.
	 */
	private String label;
	/**
	 * If this section is a form this is the target
	 */
	private Controller formTarget;
	/**
	 * Free fields inside this section. They are not attached to an Entity Form.
	 */
	private List<InputField> fields = new ArrayList<InputField>();
	private boolean readOnly;
	/**
	 * Indicates whether this section has a putFields declaration
	 */
	private boolean putFields;
	/**
	 * Defines the section style class
	 */
	private String style = Defaults.DEFAULT_SECTION_STYLE;
	/**
	 * The default bean name used if no one is defined.
	 */
	private String defaultInputFieldBeanName = Defaults.DEFAULT_INPUTFIELD_BEAN_NAME;

	public ViewSection() {
		super("viewsection");
		fields = new ArrayList<InputField>();
		subSections = new ArrayList<ViewSection>();
	}

	public ViewSection(String string) {
		super(string);
	}

	/**
	 * @return the SubSections
	 */
	public List<ViewSection> getSubSections() {
		return subSections;
	}

	/**
	 * @param sections
	 *            the SubSections to set
	 */
	public void setSubSections(List<ViewSection> sections) {
		for (ViewSection section : subSections) {
			section.setParent(this);
		}
		this.subSections = sections;
		this.notifyModelChangeListeners();
	}

	public void addSubSection(ViewSection section) {
		section.setParent(this);
		this.getSubSections().add(section);
		this.notifyModelChangeListeners();
	}

	public void removeSubSection(ViewSection section) {
		this.getSubSections().remove(section);
		this.notifyModelChangeListeners();
	}

	/**
	 * Returns true if this section has a putFields declaration in it.
	 * 
	 * @return true if the section has a putFields declaration
	 */
	public boolean hasPutFields() {
		return this.putFields;
	}

	public String getNamespace() {
		String namespace = this.getName();
		if (this.getParent() instanceof ViewSection) {
			ViewSection upperSection = (ViewSection) this.getParent();
			namespace = upperSection.getNamespace() + "." + namespace;
		}
		return namespace;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof ViewSection) {
			this.addSubSection((ViewSection) child);
			return true;
		}
		if (child instanceof InputField) {
			this.addField((InputField) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof ViewSection) {
			this.removeSubSection((ViewSection) child);
			return true;
		}
		if (child instanceof InputField) {
			this.removeField((InputField) child);
			return true;
		}
		return false;
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		if (this.subSections != null && !this.subSections.isEmpty())
			children.addAll(this.subSections);
		if (this.fields != null && !this.fields.isEmpty())
			children.addAll(this.fields);
		return children;
	}

	public ViewSection getSuperSection() {
		return superSection;
	}

	public void setSuperSection(ViewSection superSection) {
		this.superSection = superSection;
	}

	public ViewSectionType getSectionType() {
		return sectionType;
	}

	public void setSectionType(ViewSectionType sectionType) {
		this.sectionType = sectionType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setFormTarget(Controller modelController) {
		this.formTarget = modelController;
	}

	public Controller getFormTarget() {
		Controller target = this.formTarget;
		if (target == null) {
			if (this.superSection != null) {
				target = this.superSection.formTarget;
			}
			if (target == null) {
				for (ViewSection sub : this.getSubSections()) {
					target = sub.formTarget;
					if (target != null) {
						break;
					}
				}
			}
		}
		return formTarget;
	}

	public List<InputField> getFields() {
		return fields;
	}

	/**
	 * Return fields that are buttons. Will not include form buttons
	 * 
	 * @return
	 */
	public List<InputField> getButtons() {
		List<InputField> buttons = new ArrayList<InputField>();
		Set<InputField> allFields = new TreeSet<InputField>(
				new Comparator<InputField>() {
					@Override
					public int compare(InputField o1, InputField o2) {
						return o1.getName().compareToIgnoreCase(o2.getName());
					}
				});
		allFields.addAll(this.getFields());
		for (InputField field : allFields) {
			if ("button".equals(field.getType())
					|| "button".equals(field.getFormFieldType())
					|| "cancel".equals(field.getType())
					|| "cancel".equals(field.getFormFieldType())) {
				buttons.add(field);
			}
		}
		return buttons;
	}

	/**
	 * Return fields that are tables
	 * 
	 * @return
	 */
	public List<InputField> getTables() {
		List<InputField> tables = new ArrayList<InputField>();
		for (InputField field : this.getFields()) {
			if ("table".equals(field.getType())
					|| "table".equals(field.getFormFieldType())) {
				tables.add(field);
			}
		}
		return tables;
	}

	public void setFields(List<InputField> fields) {
		for (InputField field : fields) {
			field.setParent(this);
		}
		this.fields = fields;
		this.notifyModelChangeListeners();
	}

	public void addField(InputField field) {
		if (this.hasField(field.getName())) {
			throw new IllegalArgumentException("The section " + this.getName()
					+ " already contains a field with this name: "
					+ field.getName());
		}
		field.setParent(this);
		this.getFields().add(field);
		this.notifyModelChangeListeners();
	}

	public void removeField(InputField field) {
		field.setParent(null);
		this.getFields().remove(field);
		this.notifyModelChangeListeners();
	}

	/**
	 * @param name
	 * @return
	 */
	public boolean hasField(String name) {
		return this.getField(name) != null;
	}

	/**
	 * Returns an InputField with the given name or null if there is no field
	 * with the given name
	 * 
	 * @param name
	 *            the name to look for
	 * @return the InputField or null
	 */
	public InputField getField(String name) {
		for (InputField field : this.getFields()) {
			if (StringUtils.equals(name, field.getName())
					|| StringUtils.equals(name, field.getFieldId())) {
				return field;
			}
		}
		return null;
	}

	public Boolean isForm() {
		return ViewSectionType.form.equals(this.sectionType);
	}

	public Boolean hasEnclosingForm() {
		return getParentForm() != null;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setPutFields(boolean value) {
		this.putFields = value;
	}

	public boolean isPutFields() {
		return putFields;
	}

	public Boolean isTab() {
		return ViewSectionType.tab.equals(this.sectionType);
	}

	public Boolean hasTabs() {
		for (ViewSection subSection : this.getSubSections()) {
			if (subSection.isTab()) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	public List<ViewSection> getTabs() {
		List<ViewSection> tabs = new ArrayList<ViewSection>();
		for (ViewSection subSection : this.getSubSections()) {
			if (subSection.isTab()) {
				tabs.add(subSection);
			}
		}
		return tabs;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getStyle() {
		return style;
	}

	/**
	 * Currently this method is only called by the GenericDSLPutFieldsSection.vm
	 * template so it can create blocks of fields two by two in the page.
	 * 
	 * @return
	 */
	public FieldBlock getMainBlock() {
		// In this test the main block has the field blocks
		// defined statically bellow
		FieldBlock mainBlock = new FieldBlock("main_block", 1);// This is the
																// main block.
																// The size is
																// fixed
		mainBlock.addAll(getFieldBlocks2());
		return mainBlock;
	}

	private List<FieldBlock> getFieldBlocks2() {
		// This will contain all rows for the View
		final List<FieldBlock> fieldBlockList = new ArrayList<FieldBlock>();
		// This is the current rowBlock
		FieldBlock currentRowBlock = null;
		final LayoutStyle layoutStyle = getLayoutStyleForProject();
		List<InputField> viewFields = new ArrayList<InputField>();
		// Adds the correct fields to the list of fields in the
		// view.
		addFields(viewFields);
		int fieldIndex = 0;
		// Can't be here, otherwise buttons will be inside the form
		// making them look and behave differently
		// viewFields.addAll(getButtons());
		for (InputField currentInputField : viewFields) {
			if (layoutStyle == null) {
				// Default blocks if no style is present
				log.warn("No style is present. No block will be created!");
				continue;
			}
			// Create a block only if necessary
			if (createBlock(layoutStyle, currentInputField, currentRowBlock)) {
				// The block is full or doesn't exists yet. Create another one
				// and append to the final block list.
				Integer blocks = layoutStyle.getBlocks();
				currentRowBlock = new FieldBlock("row_block_" + this.getName(),
						blocks);
				// Sets the correct row style for the row
				currentRowBlock.style = getRowStyle(currentInputField,
						layoutStyle);
				// Append to the list
				fieldBlockList.add(currentRowBlock);
				// We just created a new row, reset field index
				fieldIndex = 0;
			}
			// Create a FieldColumn to actually hold the InputField
			FieldColumn fieldColumn = new FieldColumn("column_forfield_"
					+ currentInputField.getName(), FIELD_BLOCK_SIZE);
			// Set the property directly
			fieldColumn.field = currentInputField;
			// The column style
			fieldColumn.style = getFieldColumStyle(currentInputField);
			// Now, create a block to hold the FieldColumn. This is necessary
			// as the FieldBlock itself has its own style and the FieldColumn
			// will only have the InputField style. TODO: check if this is true!
			FieldBlock fieldBlock = new FieldBlock("field_block_"
					+ currentInputField.getName(), FIELD_BLOCK_SIZE);
			// Set the column with the field. TODO: Check if it is necessary!
			fieldBlock.add(fieldColumn);
			// The fieldBlock style will obey the fieldBlock style for the
			// field index in the block.
			/*
			 * fieldBlock.style = layoutStyle.getBlockStyles().get(fieldIndex) +
			 * " blocStyleIndex" + fieldIndex;
			 */
			FieldStyle styleForField = layoutStyle
					.getStyleForField(currentInputField);
			if (styleForField == null) {
				// TODO: This shouldn't happen as we parsed it before
				throw new IllegalArgumentException("Field: "
						+ currentInputField
						+ " does not have a FieldStyle defined!");
			}
			List<String> blockStyles = styleForField.getStyles("block");
			String fieldBlockStyle = null;
			log.info("all block styles for field ["
					+ currentInputField.getName() + "]: " + blockStyles);
			if (fieldIndex >= blockStyles.size()) {
				log.error("The current fieldIndex [" + fieldIndex
						+ "] is more than the current blockStyles size ["
						+ blockStyles.size() + "]");
				log.error("Using the last blockStyles.get(size - 1)");
				fieldBlockStyle = blockStyles.get(blockStyles.size() - 1);
			} else {
				fieldBlockStyle = blockStyles.get(fieldIndex);
			}
			// log.info("Setting style for fieldBlock [" + fieldBlock.getId()
			// + "]: [" + fieldBlockStyle + "]");
			fieldBlock.style = fieldBlockStyle;
			fieldIndex++;
			// Add the fieldBlock to the current rowBlock
			currentRowBlock.add(fieldBlock);
		}
		return fieldBlockList;
	}

	private String getFieldColumStyle(InputField currentInputField) {
		// currentInputField.getStyle();
		return null;
	}

	/**
	 * Returns the current layout style for this project
	 * 
	 * @return
	 */
	private LayoutStyle getLayoutStyleForProject() {
		ControlLayer controlLayer = getEnclosingProject().getControlLayer();
		return controlLayer.getCurrentLayoutStyle();
	}

	/**
	 * Returns the correct row style. If the field type overrides the project
	 * rowStyle definition in the layout DSL then this value is returned,
	 * otherwise the project rowStyle is returned.
	 */
	private String getRowStyle(InputField field, LayoutStyle layoutStyle) {
		// This is the field style as defined in the DSL
		FieldStyle fieldStyle = layoutStyle.getStyleForField(field);
		// This is the default row style for this project. Some field types
		// may override this.
		String rowStyle = layoutStyle.getRowStyle();
		if (fieldStyle != null
				&& StringUtils.trimToNull(fieldStyle.getRowStyle()) != null) {
			// The field row style can override the layout row style
			// when creating new blocks!
			rowStyle = fieldStyle.getRowStyle();
		}
		return rowStyle;
	}

	/**
	 * Returns true if the field and layout style requires a new block to be
	 * created.
	 */
	private boolean createBlock(LayoutStyle layoutStyle, InputField field,
			FieldBlock block) {
		if (block == null || block.isComplete()) {
			return true;
		}
		return false;
	}

	private void debugBlock(final List<FieldBlock> fieldBlockList,
			StringBuilder sb) {
		sb.append("Final all fields in block:\n");
		for (FieldBlock fb : fieldBlockList) {
			sb.append("Block[" + fb + "] ");
			InputField field = fb.getField();
			if (field != null) {
				sb.append("FieldBlock field: " + field.getName()
						+ ", decorators: " + field.getFieldDecorators() + "\n");
			} else {
				debugBlock(fb, sb);
			}
		}
	}

	private void makeOneLineBlock(FieldBlock block) {
		// We need to make the block complete to force a new
		// block to be created just after this field has been added
		block.setBlockSize(1);
	}

	private void addFields(List<InputField> viewFields) {
		viewFields.addAll(getAllFields());
	}

	/**
	 * Returns fields that are from entities model (including relationships),
	 * hidden etc.
	 * 
	 * @return
	 */
	public List<InputField> getAllFields() {
		// TODO: Checking why buttons are not added here!?
		final List<InputField> viewFields = new ArrayList<InputField>();
		Predicate<? super InputField> predicate = new Predicate<InputField>() {
			@Override
			public boolean apply(InputField other) {
				for (InputField field : viewFields) {
					if (field.getName().equals(other.getName())
							&& field.getFormFieldType().equals(
									other.getFormFieldType())) {
						return false;
					}
				}
				return true;
			}
		};
		viewFields.addAll(Collections2.filter(getSimpleFieldList(), predicate));
		viewFields.addAll(Collections2.filter(getRelationshpFieldList(),
				predicate));
		viewFields.addAll(Collections2.filter(unboundFieldList(), predicate));
		viewFields.addAll(Collections2.filter(hiddenFieldsList(), predicate));
		// Added in 09-10-2014, check if is correct
		viewFields.addAll(Collections2.filter(getButtons(), predicate));
		// TODO: Check if tables are rendered
		return viewFields;
	}
	
	/**
	 * Returns fields that are from entities model (including relationships),
	 * hidden etc. that were not overridden by this section or by any of its children
	 */
	public List<InputField> getNonOverriddenFormFields(Form form) {
		// TODO: Checking why buttons are not added here!?
		final List<InputField> viewFields = new ArrayList<InputField>();
		// Section already defined fields
		final List<InputField> alreadyDefined = new ArrayList<InputField>();
		alreadyDefined.addAll(this.getAllFields());
		Predicate<? super InputField> predicate = new Predicate<InputField>() {
			@Override
			public boolean apply(InputField other) {
				for (InputField field : alreadyDefined) {
					if (field.getName().equals(other.getName())
							&& field.getFormFieldType().equals(
									other.getFormFieldType())) {
						return false;
					}
				}
				return true;
			}
		};
		viewFields.addAll(Collections2.filter(form.getFields(), predicate));
		return viewFields;
	}
	
	public List<InputField> filterFields(final List<InputField> toProcess, final List<InputField> processed) {
		Predicate<? super InputField> predicate = new Predicate<InputField>() {
			@Override
			public boolean apply(InputField other) {
				for (InputField field : processed) {
					if (field.getName().equals(other.getName())
							&& field.getFormFieldType().equals(
									other.getFormFieldType())) {
						return false;
					}
				}
				return true;
			}
		};
		final List<InputField> finalFields = new ArrayList<InputField>();
		finalFields.addAll(Collections2.filter(toProcess, predicate));
		return finalFields;
	}

	/**
	 * Returns a list of fields in this form that doesn't represents a model
	 * attribute relationship or primary keys. This can be used by templates to
	 * show only simple fields in the view. Note that fields that are not
	 * visible in the model are not returned by this method. Fields are ordered
	 * based on their order in the form.
	 * 
	 * @return list of non relationship or primary keys fields to be shown on
	 *         view
	 */
	public List<InputField> getSimpleFieldList() {
		List<InputField> simpleFieldList = new ArrayList<InputField>();
		for (InputField field : this.getFields()) {
			// Only visible fields are returned
			if (Boolean.TRUE.equals(field.getVisible())) {
				Attribute attribute = field.getParentAttribute();
				if (attribute == null) {
					// Try to find the attribute by the field name
					// TODO: Check this, it seams completely wrong assumption
					// there may have more than one entity with attributes with
					// the same name.
					attribute = this.getEnclosingProject().getModelLayer()
							.getAttributeByName(field.getName());
				}
				if (attribute != null && attribute.isNotRelationOrPrimaryKey()) {
					// This is a simple attribute to be shown on view
					simpleFieldList.add(field);
				}
			}
		}
		// String debug = "Computed SimpleFieldList in Section [" + getName()
		// + "]:\n";
		// for (InputField field : simpleFieldList) {
		// debug += "SimpleField: " + field.getName() + "\n";
		// }
		// log.info(debug);
		return simpleFieldList;
	}

	/**
	 * Returns a list of fields in this form that represents a model attribute
	 * relationship. This can be used by templates to show only fields in the
	 * view. Note that fields that are not visible in the model are not returned
	 * by this method. Fields are ordered based on their order in the form.
	 * 
	 * @return list of relationship fields to be shown on view
	 */
	public List<InputField> getRelationshpFieldList() {
		List<InputField> relationshpFieldList = new ArrayList<InputField>();
		for (InputField field : this.getFields()) {
			// Only visible fields are returned
			if (Boolean.TRUE.equals(field.getVisible())) {
				Attribute attribute = field.getParentAttribute();
				if (attribute != null && attribute.getFromRelation()) {
					// This is a simple attribute to be shown on view
					relationshpFieldList.add(field);
				}
			}
		}
		// String debug = "Computed RelationshipFieldList in Section ["
		// + getName() + "]:\n";
		// for (InputField field : relationshpFieldList) {
		// debug += "Computed RelationshipField. Field: " + field.getName()
		// + ", decorators: " + field.getFieldDecorators() + "\n";
		// }
		// log.info(debug);
		return relationshpFieldList;
	}

	private Collection<? extends InputField> hiddenFieldsList() {
		List<InputField> hiddenFieldList = new ArrayList<InputField>();
		for (InputField field : this.getFields()) {
			// Only hidden fields are returned
			if (Boolean.TRUE.equals(field.isHidden())) {
				hiddenFieldList.add(field);
			}
		}
		return hiddenFieldList;
	}

	private Collection<? extends InputField> unboundFieldList() {
		List<InputField> unboundFieldList = new ArrayList<InputField>();
		for (InputField field : this.getFields()) {
			// TODO: Why table was in this list?:
			if (field.isButton() || field.isCancel()) {// || field.isTable()) {
				continue;
			}
			// Only visible fields are returned
			if (Boolean.TRUE.equals(field.getVisible())) {
				Attribute attribute = field.getParentAttribute();
				if (attribute == null) {
					// This field is currently unbound to any model
					// attribute. Add it if it has a sourceProperty or
					// source bean defined.
					if (StringUtils.isNotEmpty(field.getFieldBean())) {
						unboundFieldList.add(field);
						continue;
					}
					if (StringUtils.isNotEmpty(field.getFieldSource())) {
						unboundFieldList.add(field);
						continue;
					}
				}
			}
		}
		// String debug = "Computed UnboundFieldList in Section [" + getName()
		// + "]:\n";
		// for (InputField field : unboundFieldList) {
		// debug += "UnboundField: " + field.getName() + "\n";
		// }
		// log.info(debug);
		return unboundFieldList;
	}

	/**
	 * Defines the default block size (how many elements - columns)
	 */
	final static int DEFAULT_BLOCK_SIZE = 2;

	/**
	 * FieldBlock represents the composite in the composite pattern. It holds
	 * references to other blocks (it is a List<FieldBlock> itself). This
	 * generic representation is used to create row blocks. The FieldColumn is
	 * used to create leafs in the pattern.
	 * 
	 * @author 61603945504
	 * 
	 */
	public class FieldBlock extends ArrayList<FieldBlock> {
		private static final long serialVersionUID = 1L;
		int capacity;
		String style = StringUtils.EMPTY;
		String id = "block_undefined";

		public FieldBlock(String str, int layoutBlockSize) {
			id = str;
			capacity = layoutBlockSize;
		}

		public void setBlockSize(int value) {
			capacity = value;
		}

		public InputField getField() {
			return null;
		}

		/**
		 * Returns true if the current elements of this block reached the
		 * maximum capacity
		 * 
		 * @return
		 */
		public boolean isComplete() {
			return this.size() == capacity;
		}

		public String getId() {
			return id;
		}

		public String getStyle() {
			return style;
		}

		@Override
		public String toString() {
			return "FieldBlock [blockSize=" + capacity + ", style=" + style
					+ ", id=" + id + ", elements: " + super.toString() + "]";
		}
	}

	/**
	 * Represents a leaf in the composite pattern for {@link FieldBlock} and
	 * encapsulates a {@link InputField}.
	 * 
	 * @author 61603945504
	 * 
	 */
	public class FieldColumn extends FieldBlock {
		private static final long serialVersionUID = 1L;

		public FieldColumn(String id, int size) {
			super(id, size);
		}

		InputField field;

		@Override
		public InputField getField() {
			return field;
		}

		@Override
		public String toString() {
			return "FieldColumn [field=" + field + ", decorators: "
					+ field.getFieldDecorators() + "]";
		}

	}

	/**
	 * Return a list of primary key attributes for this form
	 * 
	 * @return list of primary key attributes for this form
	 */
	public List<Attribute> getPrimaryKeyAttributeList() {
		List<Attribute> primaryKeyAttributeList = new ArrayList<Attribute>();
		for (InputField field : this.getFields()) {
			Attribute attribute = field.getParentAttribute();
			if (attribute != null
					&& Boolean.TRUE.equals(attribute.isPrimaryKey())) {
				// This is a simple attribute to be shown on view
				primaryKeyAttributeList.add(attribute);
			}
		}
		return primaryKeyAttributeList;
	}

	public Boolean hasParentForm() {
		if (this.getParent() != null) {
			StereotypedItem parent = this.getParent();
			if (parent instanceof Form) {
				return true;
			} else if (parent instanceof ViewSection) {
				if (((ViewSection) parent).isForm()) {
					return true;
				}
				// The upper component is a section. Ask
				// it recursively if it has a form
				return ((ViewSection) parent).hasParentForm();
			}
		}
		return false;
	}

	public PresentationView getParentView() {
		if (this.getParent() != null) {
			StereotypedItem parent = this.getParent();
			if (parent instanceof PresentationView) {
				return (PresentationView) parent;
			} else if (parent instanceof ViewSection) {
				// The upper component is a section. Ask
				// it recursively to find the view this section
				// belongs to.
				return ((ViewSection) parent).getParentView();
			}
		}
		return null;
	}

	public ViewSection getParentForm() {
		if (this.getParent() != null) {
			StereotypedItem parent = this.getParent();
			if (parent instanceof ViewSection) {
				ViewSection parentSection = (ViewSection) parent;
				if (parentSection.isForm()) {
					return parentSection;
				} else {
					// The upper component is a section but not a form. Ask
					// it recursively to find the view this section belongs to.
					return parentSection.getParentForm();
				}
			} else if (parent instanceof PresentationView) {
				// The parent of this section is a view. It can't contain upper
				// sections
				return null;
			}
		}
		return null;
	}

	public void setDefaultInputFieldBeanName(String defaultInputFieldBeanName) {
		this.defaultInputFieldBeanName = defaultInputFieldBeanName;
	}

	public String getDefaultInputFieldBeanName() {
		return defaultInputFieldBeanName;
	}

}
