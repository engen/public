package mddprocessorplugin.model;

public class LogicalPathFactory {

	public static LogicalPath creeate(ModelTarget target,
			LogicalPathType logicalPathType, String name, String logicalName) {
		LogicalPath logicalPath = new LogicalPath();
		logicalPath.setParent(target);
		logicalPath.setType(logicalPathType.getExtension());
		logicalPath.setName(name);
		logicalPath.setLogicalName(logicalName);
		return logicalPath;
	}

}
