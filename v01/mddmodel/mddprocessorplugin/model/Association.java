/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/

package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.model.util.StringUtils;

/**
 * Represents an association between to entities in the model
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 1, 2009
 */
public class Association extends BaseModel {

	/**
	 * Generated serial version id
	 */
	private static final long serialVersionUID = -8641620853962064553L;
	/** The relation model representing this association if it comes from database model **/
	private Relation relation;
	/** The source of this connection **/
	private AssociationEnd source;
	private AssociationEnd target;

	public Association() {
		this.setStereotypeName("association");
	}
	
	/**
	 * Creates an association representing a relation in the model
	 * @param source the source entity for this association
	 * @param relation the relation
	 */
	public Association(Entity source, Relation relation) {
		this();
		if (source == null) {
			throw new NullPointerException("The source entity can't be null");
		}
		this.setParent(source);
		this.setRelation(relation);
		// The role this attribute plays in the relation is the left role.
		// If the left role (this attribute's role) is left and the target
		// for this role is not many, this is a many-to-one relation, where
		// this attribute is a single reference to the other entity.
		// TODO: Rename these names after understood.
		// TODO: Why this is only stopping in debug when import database action
		// is
		// called?
		if (relation != null) {
			RelationshipRole leftRole = relation.getLeftRole();
			if (leftRole.isTargetMany()) {
				createOneToManyAttribute(leftRole);
			} else {
				//The target of the left role is one. This means the other
				//end is the one side
				createManyToOneAttribute(leftRole);
			}
		}
		
	}
	
	/**
	 * Creates an one-to-many attribute attribute. 
	 * @param role
	 */
	private void createOneToManyAttribute(RelationshipRole role) {
		if (this.getParent() == null) {
			throw new ModelException("Please, run database import before.");
		}
		/*
		ForeignKey fk = role.getTarget().getForeignKeyForRole(role);
		if (fk != null) {
			this.setForeignKeyColumn(fk.getFkColumn());
			String fkColumnCamelCase = StringUtils
					.toCamelCase(fk.getFkColumn());
			this.setReferencedColumnName(fk.getFkColumn());
			this.setBackedRelationAttribute(this
					.getAttributeNameForRelationShip(role.getTargetRole(),
							fkColumnCamelCase));
		}
		// The name of this attribute is the
		// target table foreign key java normalized name plus the target table
		// multiplicity (eg.: idFkTables)
		String fkNormalizedJavaName = StringUtils.toCamelCase(this
				.getForeignKeyColumn());
		String entityReferenceTypeName = StringUtils
				.toUppercaseFirstLetter(StringUtils.toCamelCase(role
						.getTarget().getName()));
		String pluralReferenceTypeName = StringUtils
				.toPlural(entityReferenceTypeName);
		this.setName(fkNormalizedJavaName + pluralReferenceTypeName);
		// there is no default value for references.
		// TODO: Check if this is true.
		this.setDefaultValue(StringUtils.EMPTY);
		// This is the many-to-one side of the relation.
		this.setCollection(false);
		// Sure this is from relation.
		this.setFromRelation(true);
		// For relations many-to-one always attributes are treated as
		// references.
		this.setReference(true);
		// This is a collection
		this.setCollection(true);
		// TODO: Use the project default type for collections.
		String uType = "java.util.Set";
		// Sets the type accordingly.
		this.setType(uType);
		// Type implementation for collections
		this.setTypeImpl("java.util.HashSet");
		// The other end of this relation.
		this.setReferenceType(entityReferenceTypeName);
		// The reference type implementation depends on project
		// configuration
		// In this case we consult the project to see if it has a suffix
		// configured
		// to it. If it has use it, otherwise use the default.TODO: verify
		// if this is
		// the correct behavior.
		boolean allEntitiesAreAbstract = false;// TODO: this is not working.
		// this.getEnclosingProject().getProjectConfig().getIsEntityAbstract();
		if (allEntitiesAreAbstract) {
			this.setReferenceTypeImpl(this.getReferenceType()
					+ this.getEnclosingProject().getProjectConfig()
							.getEntityImplementationSuffix());
		} else {
			// If not defined as abstract entities will have the same
			// reference type implementation
			// as their reference type.
			this.setReferenceTypeImpl(this.getReferenceType());
		}
		// Value to be shown in pages or other places where
		// a default value is required. By default the primary key.
		List<Column> primaryKeyColumns = role.getOrigin()
				.getPrimaryKeyColumns();
		if (primaryKeyColumns != null && primaryKeyColumns.size() > 0) {
			String primaryKeyColumnName = primaryKeyColumns.get(0).getName();
			String primaryKeyAttributeName = StringUtils
					.toCamelCase(primaryKeyColumnName);
			// The default value will be the first primary key column
			this.setDefaultValue(primaryKeyAttributeName);
			this.setReferenceTypePrimaryKey(primaryKeyAttributeName);
		}
		// For now always false. If this is really required it should be
		// treated as so in the business layer.
		// TODO: verify if this requirement still holds true.
		this.setRequired(false);
		this.setCardinality(RelationCardinality.ONE_TO_MANY);
		this.setCommonAttributesAfterNameHasBeenSet();
		*/
	}

	private void createManyToOneAttribute(RelationshipRole role) {
		/*
		// Single reference to the other entity.
		// Setting the foreign key that controls this relation ship.
		ForeignKey fk = role.getOrigin().getForeignKeyForRole(role);
		String fkColumnCamelCase = StringUtils.toCamelCase(fk
				.getFkColumn()); 
		// TODO: Rename these methods when understood.
		if (fk != null) {
			this.setBackedRelationAttribute(fkColumnCamelCase);
			// The foreign key column name.
			this.setForeignKeyColumn(fk.getFkColumn());
			// The primary key on the referenced table
			this.setReferencedColumnName(fk.getPkColumn());
		}
		// The name of this attribute is the Foreign key in this table
		// referencing the id in the target table plus the target table
		// name in singular.
		this.setName(getAttributeNameForRelationShip(role, fkColumnCamelCase));
		// there is no default value for references. TODO: Check if this is
		// true.
		this.setDefaultValue(StringUtils.EMPTY);
		// This is the many-to-one side of the relation.
		this.setCollection(false);
		// Sure this is from relation.
		this.setFromRelation(true);
		// For relations many-to-one always attributes are treated as
		// references.
		this.setReference(true);
		// The type of this attribute is the type of his
		// mother file.
		String uType = this.getRelationShipReferenceTypeName(role);
		if (this.isCollection()) {
			// TODO: Use the project default type for collections.
			uType = "java.util.Set";
		}
		// Sets the type accordingly.
		// TODO: Update the type name space.
		this.setType(uType);
		// TODO: The reference type is the same for now but has to reflect the
		// correct type.
		this.setReferenceType(this.getType());
		// The reference type implementation depends on project
		// configuration
		// In this case we consult the project to see if it has a suffix
		// configured
		// to it. If it has use it, otherwise use the default.TODO: verify
		// if this is
		// the correct behavior.
		boolean allEntitiesAreAbstract = false;// TODO: should get from project.
		// this is not working.
		// this.getEnclosingProject().getProjectConfig().getIsEntityAbstract();
		if (allEntitiesAreAbstract) {
			this.setReferenceTypeImpl(this.getReferenceType()
					+ this.getEnclosingProject().getProjectConfig()
							.getEntityImplementationSuffix());
		} else {
			// If not defined as abstract entities will have the same
			// reference type implementation
			// as their reference type.
			this.setReferenceTypeImpl(this.getReferenceType());
		}
		// Value to be shown in pages or other places where
		// a default value is required. By default the primary key.
		List<Column> primaryKeyColumns = role.getOrigin()
				.getPrimaryKeyColumns();
		if (primaryKeyColumns != null && primaryKeyColumns.size() > 0) {
			String primaryKeyColumnName = primaryKeyColumns.get(0).getName();
			String primaryKeyAttributeName = StringUtils
					.toCamelCase(primaryKeyColumnName);
			// The default value will be the first primary key column
			this.setDefaultValue(primaryKeyAttributeName);
			this.setReferenceTypePrimaryKey(primaryKeyAttributeName);
		}
		String properAttributeName = StringUtils.toCamelCase(this
				.getBackedRelationAttribute());
		Attribute fkAttribute = ((Entity) this.getParent())
				.getAttribute(properAttributeName);
		if (fkAttribute != null) {
			this.setRequired(fkAttribute.isRequired());
		} else {
			// Set to required is false when the attribute is not found.
			this.setRequired(Boolean.FALSE);
		}
		// TODO: Make it a enumeration.
		this.setCardinality(RelationCardinality.MANY_TO_ONE);
		this.setCommonAttributesAfterNameHasBeenSet();
		*/
	}
	
	@Override
	public List<? extends StereotypedItem> getChildren() {
		List<AssociationEnd> children = new ArrayList<AssociationEnd>();
		children.add(source);
		children.add(target);
		return super.getChildren();
	}

	public AssociationEnd getSource() {
		return source;
	}

	public void setSource(AssociationEnd source) {
		this.source = source;
		this.notifyModelChangeListeners();
	}

	public AssociationEnd getTarget() {
		return target;
	}

	public void setTarget(AssociationEnd target) {
		this.target = target;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the relation
	 */
	public Relation getRelation() {
		return relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}

}
