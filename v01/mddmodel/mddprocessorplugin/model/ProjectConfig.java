package mddprocessorplugin.model;

import org.apache.commons.lang.StringUtils;

public class ProjectConfig extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5642946829830743599L;
	/* The project' author. Usually a company name or an individual. */
	private String author = Defaults.AUTHOR;
	/* The project' description. */
	private String description = Defaults.PROJECT_DESCRIPTION;
	/*
	 * NameSpace for this project. It comes before any entity nameSpace or
	 * package.
	 */
	private String namespace = Defaults.PROJECT_NAMESPACE;
	/* If true all paths will be computed relative to project path */
	private String relativePath = Defaults.RELATIVE_PATH;
	/* The path where files will be generated to. */
	private String outputPath = Defaults.OUTPUT_PATH;
	/* Root source directory for source code files */
	private String srcDir = Defaults.SRC_DIR;
	/* Root source directory for generated source code files */
	private String genSrcDir = Defaults.GEN_SRC_DIR;
	/* Path to the templates in the project. */
	private String templatesPath = Defaults.TEMPLATE_PATH;
	/* Path to the specific product images. Will be merged to the images dir */
	private String productImagesPath = Defaults.PRODUCT_IMAGES_PATH;
	/* Project or product specific logo file name */
	private String productLogoFileName = Defaults.PRODUCT_LOGO_FILE_NAME;
	/* Company specific logo file name */
	private String companyLogoFileName = Defaults.COMPANY_LOGO_FILE_NAME;

	/* Source files build directory */
	private String buildDir = Defaults.BUILD_DIR;
	/* Tests sources build directory */
	private String buildTestsDir = Defaults.BUILD_TESTS_DIR;
	/* The library (jar files) directory */
	private String libDir = Defaults.LIB_DIR;
	/* Temporary Libraries file location */
	private String tmpLibDir = Defaults.TEMP_LIB_DIR;
	/* The final distribution directory for the project files */
	private String distDir = Defaults.DIST_DIR;
	/* The directory for the tests result report */
	private String testReportDir = Defaults.TEST_REPORT_DIR;
	/* The directory where we can find the required jar files for this project */
	private String requiredLibDir = Defaults.REQUIRED_LIB_DIR;
	/* The path of this project. Required. Must be the same as the .mdd file */
	private String projectPath = Defaults.PROJECT_PATH;
	/* If defined, all entities should be generated as interfaces. */
	private boolean entityAbstract = Defaults.IS_ENTITY_ABSTRACT;
	/* If the entityAbstract is defined, this is the implementation suffix */
	private String entityImplementationSuffix = Defaults.ENTITY_IMPLEMENTATION_SUFFIX;
	/* The server location */
	private String serverDir = Defaults.SERVER_DIR;
	/* The web application description */
	private String webAppDescription = Defaults.WEB_APP_DESCRIPTION;
	/* The web application display name */
	private String webAppDisplayName = Defaults.WEB_APP_DISPLAY_NAME;
	/* The target web files location */
	private String webDir = Defaults.WEB_DIR;
	/* The target for generated web files */
	private String genWebDir = Defaults.GEN_WEB_DIR;
	/* The target for web files for CRUD operations */
	private String jspDir = Defaults.JSP_DIR;
	/* The application server this application will be target to */
	private String applicationServer = Defaults.APPLICATION_SERVER;
	/* Context for the web application */
	private String webContext = Defaults.WEB_CONTEXT;
	/* The control template for this project. */
	private String controlTemplate = Defaults.CONTROL_TEMPLATE;
	/* Where the JavaDoc files for this project will be generated. */
	private String javaDocDir = Defaults.JAVA_DOC_DIR;
	/* The tests source files directory */
	private String testDir = Defaults.TEST_DIR;
	/* The suffix every action file will have. */
	private String actionSuffix = Defaults.ACTION_SUFFIX;
	/**
	 * Default suffix for the controller URI.
	 */
	private String controllerUriSuffix = Defaults.CONTROLLER_URI_SUFFIX;
	/* The physical project file location. */
	private String projectFileLocation = StringUtils.EMPTY;
	/* The default page size for listings */
	private String defaultPageSize = Defaults.DEFAULT_PAGE_SIZE;
	/* The suffix every DAO will have */
	private String daoSuffix = Defaults.DAO_SUFFIX;
	/* The sequence name pattern - used for Oracle databases */
	private String idGenerator = Defaults.ID_GENERATOR;
	/**
	 * The inheritance strategy mapping for the project. Can be overriden by
	 * individual table strategy.
	 */
	private String inheritanceStrategy = Defaults.DEFAULT_INHERITANCE_STRATEGY
			.toString();
	/** The source files extensions. This is determined by the language. **/
	private String sourceFielExtension = Defaults.SOURCE_FILE_EXTENSION;
	/** The extension for view files **/
	private String viewFileExtension = Defaults.VIEW_FILE_EXTENSION;
	/**
	 * Should security files be generated.
	 */
	private boolean addSecurity = Boolean.TRUE;
	/**
	 * The default user session timeout
	 */
	private String userSessionTimeout = Defaults.USER_SESSION_TIMEOUT;

	private boolean loadBootstrap = Boolean.TRUE;
	/**
	 * The architecture template path defines the root for templates for the
	 * view, controllers and model templates. All other paths are relative to
	 * this.
	 */
	private String architectureTemplatePath = Defaults.DEFAULT_ROOT_ARCHITECUTRE;

	/**
	 * The current layout name for this project control layer for code
	 * generation. This name will be used to append the "Config" suffix for
	 * configure the layout for presentation
	 */
	private String currentLayoutName = Defaults.DEFAULT_LAYOUT_NAME;

	/**
	 * Constructor for ProjectConfig.
	 */
	public ProjectConfig() {
		this.setStereotypeName("projectconfig");
	}

	public ProjectConfig(StereotypedItem parent) {
		super(parent);
		this.setStereotypeName("projectconfig");
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the relativePath
	 */
	public String getRelativePath() {
		return relativePath;
	}

	/**
	 * @param relativePath
	 *            the relativePath to set
	 */
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	/**
	 * @return the outputPath
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * @param outputPath
	 *            the outputPath to set
	 */
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	/**
	 * @return the srcDir
	 */
	public String getSrcDir() {
		return srcDir;
	}

	/**
	 * @param srcDir
	 *            the srcDir to set
	 */
	public void setSrcDir(String srcDir) {
		this.srcDir = srcDir;
	}

	/**
	 * @return the genSrcDir
	 */
	public String getGenSrcDir() {
		return genSrcDir;
	}

	/**
	 * @param genSrcDir
	 *            the genSrcDir to set
	 */
	public void setGenSrcDir(String genSrcDir) {
		this.genSrcDir = genSrcDir;
	}

	/**
	 * @return the templatesPath
	 */
	public String getTemplatesPath() {
		return templatesPath;
	}

	/**
	 * @param templatesPath
	 *            the templatesPath to set
	 */
	public void setTemplatesPath(String templatesPath) {
		this.templatesPath = templatesPath;
	}

	/**
	 * @return the builDir
	 */
	public String getBuildDir() {
		return buildDir;
	}

	/**
	 * @param builDir
	 *            the builDir to set
	 */
	public void setBuildDir(String builDir) {
		this.buildDir = builDir;
	}

	/**
	 * @return the buildTestsDir
	 */
	public String getBuildTestsDir() {
		return buildTestsDir;
	}

	/**
	 * @param buildTestsDir
	 *            the buildTestsDir to set
	 */
	public void setBuildTestsDir(String buildTestsDir) {
		this.buildTestsDir = buildTestsDir;
	}

	/**
	 * @return the libDir
	 */
	public String getLibDir() {
		return libDir;
	}

	/**
	 * @param libDir
	 *            the libDir to set
	 */
	public void setLibDir(String libDir) {
		this.libDir = libDir;
	}

	/**
	 * @param tmpLibDir
	 *            the tmpLibDir to set
	 */
	public void setTmpLibDir(String tmpLibDir) {
		this.tmpLibDir = tmpLibDir;
	}

	/**
	 * @return the tmpLibDir
	 */
	public String getTmpLibDir() {
		return tmpLibDir;
	}

	/**
	 * @return the distDir
	 */
	public String getDistDir() {
		return distDir;
	}

	/**
	 * @param distDir
	 *            the distDir to set
	 */
	public void setDistDir(String distDir) {
		this.distDir = distDir;
	}

	/**
	 * @return the testReportDir
	 */
	public String getTestReportDir() {
		return testReportDir;
	}

	/**
	 * @param testReportDir
	 *            the testReportDir to set
	 */
	public void setTestReportDir(String testReportDir) {
		this.testReportDir = testReportDir;
	}

	/**
	 * @return the testDir
	 */
	public String getTestDir() {
		return testDir;
	}

	/**
	 * @param testDir
	 *            the testDir to set
	 */
	public void setTestDir(String testDir) {
		this.testDir = testDir;
	}

	/**
	 * @return the requiredLibDir
	 */
	public String getRequiredLibDir() {
		return requiredLibDir;
	}

	/**
	 * @param requiredLibDir
	 *            the requiredLibDir to set
	 */
	public void setRequiredLibDir(String requiredLibDir) {
		this.requiredLibDir = requiredLibDir;
	}

	/**
	 * @return the projectPath
	 */
	public String getProjectPath() {
		return projectPath;
	}

	/**
	 * @param projectPath
	 *            the projectPath to set
	 */
	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}

	/**
	 * @return the isEntityAbstract
	 */
	public Boolean getIsEntityAbstract() {
		return entityAbstract;
	}

	/**
	 * @param isEntityAbstract
	 *            the isEntityAbstract to set
	 */
	public void setIsEntityAbstract(Boolean isEntityAbstract) {
		this.entityAbstract = isEntityAbstract;
	}

	/**
	 * @return the entityImplementationSuffix
	 */
	public String getEntityImplementationSuffix() {
		return entityImplementationSuffix;
	}

	/**
	 * @param entityImplementationSuffix
	 *            the entityImplementationSuffix to set
	 */
	public void setEntityImplementationSuffix(String entityImplementationSuffix) {
		this.entityImplementationSuffix = entityImplementationSuffix;
	}

	/**
	 * @return the serverDir
	 */
	public String getServerDir() {
		return serverDir;
	}

	/**
	 * @param serverDir
	 *            the serverDir to set
	 */
	public void setServerDir(String serverDir) {
		this.serverDir = serverDir;
	}

	/**
	 * @return the webAppDescription
	 */
	public String getWebAppDescription() {
		return webAppDescription;
	}

	/**
	 * @param webAppDescription
	 *            the webAppDescription to set
	 */
	public void setWebAppDescription(String webAppDescription) {
		this.webAppDescription = webAppDescription;
	}

	/**
	 * @return the webAppDisplayName
	 */
	public String getWebAppDisplayName() {
		return webAppDisplayName;
	}

	/**
	 * @param webAppDisplayName
	 *            the webAppDisplayName to set
	 */
	public void setWebAppDisplayName(String webAppDisplayName) {
		this.webAppDisplayName = webAppDisplayName;
	}

	/**
	 * @return the webDir
	 */
	public String getWebDir() {
		return webDir;
	}

	/**
	 * @param webDir
	 *            the webDir to set
	 */
	public void setWebDir(String webDir) {
		this.webDir = webDir;
	}

	/**
	 * @return the genWebDir
	 */
	public String getGenWebDir() {
		return genWebDir;
	}

	/**
	 * @param genWebDir
	 *            the genWebDir to set
	 */
	public void setGenWebDir(String genWebDir) {
		this.genWebDir = genWebDir;
	}

	/**
	 * @return the jspDir
	 */
	public String getJspDir() {
		return jspDir;
	}

	/**
	 * @param jspDir
	 *            the jspDir to set
	 */
	public void setJspDir(String jspDir) {
		this.jspDir = jspDir;
	}

	/**
	 * @return the applicationServer
	 */
	public String getApplicationServer() {
		return applicationServer;
	}

	/**
	 * @param applicationServer
	 *            the applicationServer to set
	 */
	public void setApplicationServer(String applicationServer) {
		this.applicationServer = applicationServer;
	}

	/**
	 * @return the webContext
	 */
	public String getWebContext() {
		return webContext;
	}

	/**
	 * @param webContext
	 *            the webContext to set
	 */
	public void setWebContext(String webContext) {
		this.webContext = webContext;
	}

	/**
	 * @return the controlTemplate
	 */
	public String getControlTemplate() {
		return controlTemplate;
	}

	/**
	 * @param controlTemplate
	 *            the controlTemplate to set
	 */
	public void setControlTemplate(String controlTemplate) {
		this.controlTemplate = controlTemplate;
	}

	/**
	 * @return the javaDocDir
	 */
	public String getJavaDocDir() {
		return javaDocDir;
	}

	/**
	 * @param javaDocDir
	 *            the javaDocDir to set
	 */
	public void setJavaDocDir(String javaDocDir) {
		this.javaDocDir = javaDocDir;
	}

	public void setProjectFileLocation(String projectFileLocation) {
		this.projectFileLocation = projectFileLocation;
	}

	public String getProjectFileLocation() {
		return this.projectFileLocation;
	}

	/**
	 * @param actionSuffix
	 *            the actionSuffix to set
	 */
	public void setActionSuffix(String actionSuffix) {
		this.actionSuffix = actionSuffix;
	}

	/**
	 * @return the actionSuffix
	 */
	public String getActionSuffix() {
		return actionSuffix;
	}

	/**
	 * @param defaultPageSize
	 *            the defaultPageSize to set
	 */
	public void setDefaultPageSize(String defaultPageSize) {
		this.defaultPageSize = defaultPageSize;
	}

	/**
	 * @return the defaultPageSize
	 */
	public String getDefaultPageSize() {
		return defaultPageSize;
	}

	/**
	 * @param daoSuffix
	 *            the daoSuffix to set
	 */
	public void setDaoSuffix(String daoSuffix) {
		this.daoSuffix = daoSuffix;
	}

	/**
	 * @return the daoSuffix
	 */
	public String getDaoSuffix() {
		return daoSuffix;
	}

	/**
	 * @param idGenerator
	 *            the idGenerator to set
	 */
	public void setIdGenerator(String idGenerator) {
		this.idGenerator = idGenerator;
	}

	/**
	 * @return the idGenerator
	 */
	public String getIdGenerator() {
		return idGenerator;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}

	/**
	 * @param inheritanceStrategy
	 *            the inheritanceStrategy to set
	 */
	public void setInheritanceStrategy(String inheritanceStrategy) {
		this.inheritanceStrategy = inheritanceStrategy;
	}

	/**
	 * @return the inheritanceStrategy
	 */
	public String getInheritanceStrategy() {
		return inheritanceStrategy;
	}

	/**
	 * @return
	 */
	public String getSourceFileExtension() {
		return this.sourceFielExtension;
	}

	public void setViewFileExtension(String viewFileExtension) {
		this.viewFileExtension = viewFileExtension;
	}

	public String getViewFileExtension() {
		return viewFileExtension;
	}

	public void setControllerUriSuffix(String controllerUriSuffix) {
		this.controllerUriSuffix = controllerUriSuffix;
	}

	public String getControllerUriSuffix() {
		return controllerUriSuffix;
	}

	public void setProductImagesPath(String productImagesPath) {
		this.productImagesPath = productImagesPath;
	}

	public String getProductImagesPath() {
		return productImagesPath;
	}

	public void setProductLogoFileName(String productLogoFileName) {
		this.productLogoFileName = productLogoFileName;
	}

	public String getProductLogoFileName() {
		return productLogoFileName;
	}

	public void setCompanyLogoFileName(String companyLogoFileName) {
		this.companyLogoFileName = companyLogoFileName;
	}

	public String getCompanyLogoFileName() {
		return companyLogoFileName;
	}

	public void setAddSecurity(Boolean addSecurity) {
		this.addSecurity = addSecurity;
	}

	public Boolean getAddSecurity() {
		return addSecurity;
	}

	public void setAddSecurity(boolean addSecurity) {
		this.addSecurity = addSecurity;
	}

	public String getUserSessionTimeout() {
		return userSessionTimeout;
	}

	public void setUserSessionTimeout(String userSessionTimeout) {
		this.userSessionTimeout = userSessionTimeout;
	}

	public boolean isLoadBootstrap() {
		return loadBootstrap;
	}

	public void setLoadBootstrap(boolean loadBootstrap) {
		this.loadBootstrap = loadBootstrap;
	}

	public String getArchitectureTemplatePath() {
		return architectureTemplatePath;
	}

	public void setArchitectureTemplatePath(String architectureTemplatePath) {
		this.architectureTemplatePath = architectureTemplatePath;
	}

	public String getCurrentLayoutName() {
		return currentLayoutName;
	}

	public void setCurrentLayoutName(String currentLayoutName) {
		this.currentLayoutName = currentLayoutName;
	}

}
