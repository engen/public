package mddprocessorplugin.model;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a relation between a target table and another table in the model.
 * @author Carlos Eugenio P. da Purificacao
 * @created 15/03/2009
 * @version
 */
public class Relation extends BaseModel implements Ordered {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5885652121413781267L;

	private final String namePattern;
	/** source **/
	private Table sourceTable;
	/** target **/
	private Table targetTable;
	private Table joinTable;
	private RelationshipRole leftRole;
	private RelationshipRole rightRole;
	private List<ColumnMap> leftColumnsMap;
	private List<ColumnMap> rightColumnsMap;
	private String relationSuffix;
	private String fkRoleSuffix;

	public Relation(Table leftTable, Table rightTable, Table joinTable,
			List<ColumnMap> leftColumnsMap, List<ColumnMap> rightColumnsMap,
			String relationSuffix, String fkRoleSuffix) {
		super("relation");
		if (leftTable == null) {
			throw new IllegalArgumentException("leftTable cant be null!");
		}
		if (rightTable == null) {
			throw new IllegalArgumentException("rightTable cant be null!");
		}
		this.sourceTable = leftTable;
		this.targetTable = rightTable;
		this.joinTable = joinTable;
		this.setLeftColumnsMap(leftColumnsMap);
		this.setRightColumnsMap(rightColumnsMap);
		this.relationSuffix = relationSuffix;
		this.fkRoleSuffix = fkRoleSuffix;
		namePattern = "{0}-{1}" + relationSuffix;

		
		// Cria a role direita e a role esquerda.
		// We use the {0} so this String can be replaced with something else if
		// desired
		// String leftRoleName = (leftTable.getSqlName() + "{0}-has-" +
		// rightTable.getSqlName() + "{1}").toLowerCase();
		// String rightRoleName = (rightTable.getSqlName() + "{0}-has-" +
		// leftTable.getSqlName() + "{0}").toLowerCase();

		// if(leftTable.getCountReferencesForTable(rightTable) > 0)
		this.leftRole = new RelationshipRole(leftTable, rightTable,
				leftColumnsMap, this, fkRoleSuffix);
		// if(rightTable.getCountReferencesForTable(leftTable) > 0)
		this.rightRole = new RelationshipRole(rightTable, leftTable,
				rightColumnsMap, this, fkRoleSuffix);
		// if(this._leftRole != null)
		this.leftRole.setTargetRole(this.rightRole);
		// if(this._rightRole != null)
		this.rightRole.setTargetRole(this.leftRole);

		// Adiciona as duas roles para cada uma das tabelas da rela��o.
		// if(this._leftRole != null)
		this.sourceTable.addRelationshipRole(this.leftRole);
		// if(this._rightRole != null)
		this.targetTable.addRelationshipRole(this.rightRole);
		// if(this._leftRole != null)
		this.leftRole.print();
		// if(this._rightRole != null)
		this.rightRole.print();
		
		this.setName(this.getLeftRole().getName() + "--" + this.getRightRole().getName());
	}


	public String getName() {
		String name = MessageFormat.format(namePattern, new Object[] {
				this.sourceTable.getName(), this.targetTable.getName() });
		return name;
	}


	public boolean isMany2Many() {
		return getJoinTable() != null;
	}

	public boolean isOne2One() {
		return !this.leftRole.isTargetMany() && !this.rightRole.isTargetMany();
	}


	/**
	 * @return the sourceTable
	 */
	public Table getSourceTable() {
		return sourceTable;
	}


	/**
	 * @param sourceTable the sourceTable to set
	 */
	public void setSourceTable(Table sourceTable) {
		this.sourceTable = sourceTable;
	}


	/**
	 * @return the targetTable
	 */
	public Table getTargetTable() {
		return targetTable;
	}


	/**
	 * @param targetTable the targetTable to set
	 */
	public void setTargetTable(Table targetTable) {
		this.targetTable = targetTable;
	}


	/**
	 * @return the joinTable
	 */
	public Table getJoinTable() {
		return joinTable;
	}


	/**
	 * @param joinTable the joinTable to set
	 */
	public void setJoinTable(Table joinTable) {
		this.joinTable = joinTable;
	}


	/**
	 * @return the leftRole
	 */
	public RelationshipRole getLeftRole() {
		return leftRole;
	}


	/**
	 * @param leftRole the leftRole to set
	 */
	public void setLeftRole(RelationshipRole leftRole) {
		this.leftRole = leftRole;
	}


	/**
	 * @return the rightRole
	 */
	public RelationshipRole getRightRole() {
		return rightRole;
	}


	/**
	 * @param rightRole the rightRole to set
	 */
	public void setRightRole(RelationshipRole rightRole) {
		this.rightRole = rightRole;
	}


	/**
	 * @return the leftColumnsMap
	 */
	public List<ColumnMap> getLeftColumnsMap() {
		return leftColumnsMap;
	}


	/**
	 * @param leftColumnsMap the leftColumnsMap to set
	 */
	public void setLeftColumnsMap(List<ColumnMap> leftColumnsMap) {
		this.leftColumnsMap = leftColumnsMap;
	}


	/**
	 * @return the rightColumnsMap
	 */
	public List<ColumnMap> getRightColumnsMap() {
		return rightColumnsMap;
	}


	/**
	 * @param rightColumnsMap the rightColumnsMap to set
	 */
	public void setRightColumnsMap(List<ColumnMap> rightColumnsMap) {
		this.rightColumnsMap = rightColumnsMap;
	}


	/**
	 * @return the relationSuffix
	 */
	public String getRelationSuffix() {
		return relationSuffix;
	}


	/**
	 * @param relationSuffix the relationSuffix to set
	 */
	public void setRelationSuffix(String relationSuffix) {
		this.relationSuffix = relationSuffix;
	}


	/**
	 * @return the fkRoleSuffix
	 */
	public String getFkRoleSuffix() {
		return fkRoleSuffix;
	}


	/**
	 * @param fkRoleSuffix the fkRoleSuffix to set
	 */
	public void setFkRoleSuffix(String fkRoleSuffix) {
		this.fkRoleSuffix = fkRoleSuffix;
	}


	/**
	 * @return the namePattern
	 */
	public String getNamePattern() {
		return namePattern;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Comparator getComparator() {
		return new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				// The relations are always bellow of any column.
				// It always will be the last item inside a table.
				return 1;
			}			
		};
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}	

}
