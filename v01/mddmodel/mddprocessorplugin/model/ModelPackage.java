package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ModelPackage extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 8698314310655878316L;
	private List<ModelPackage> packages = new ArrayList<ModelPackage>();
	private List<Entity> entities = new ArrayList<Entity>();

	public ModelPackage() {
		super("modelpackage");
	}
	
	/**
	 * @return the subPackages
	 */
	public List<ModelPackage> getPackages() {
		return packages;
	}
	/**
	 * @param subPackages the subPackages to set
	 */
	public void setPackages(List<ModelPackage> packages) {
		for(ModelPackage pack : packages) {
			pack.setParent(this);
		}
		this.packages = packages;
		this.notifyModelChangeListeners();
	}
	
	public void addPackage(ModelPackage pack) {
		pack.setParent(this);
		this.getPackages().add(pack);
		this.notifyModelChangeListeners();
	}
 
	public void removePackage(ModelPackage pack) {
		this.getPackages().remove(pack);
		this.notifyModelChangeListeners();
	}
	
	/**
	 * @return the entities
	 */
	public List<Entity> getEntities() {
		return entities;
	}
	/**
	 * @param entities the entities to set
	 */
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	
	public void addEntity(Entity entity) {
		entity.setParent(this);
		this.getEntities().add(entity);
		this.notifyModelChangeListeners();
	}
	
	public boolean hasEntity(Entity entity) {
		return this.getEntities().contains(entity);
	}
	
	public void removeEntity(Entity entity) {
		this.getEntities().remove(entity);
		this.notifyModelChangeListeners();
	}	
	
	public String getNamespace() {
		String namespace = this.getName();
		if (this.getParent() instanceof ModelPackage) {
			ModelPackage upperPackage = (ModelPackage)this.getParent();
			namespace = upperPackage.getNamespace() + "." + namespace;
		}
		return namespace;
	}
	public Collection<? extends Entity> getAllEntities() {
		List<Entity> allEntities = new ArrayList<Entity>();
		allEntities.addAll(this.getEntities());
		for(ModelPackage pack : this.getPackages()) {
			allEntities.addAll(pack.getAllEntities());
		}
		return allEntities;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Entity) {
			this.addEntity((Entity) child);
			return true;
		}
		if (child instanceof ModelPackage) {
			this.addPackage((ModelPackage) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Entity) {
			this.removeEntity((Entity) child);
			return true;
		}
		if (child instanceof ModelPackage) {
			this.removePackage((ModelPackage) child);
			return true;
		}
		return false;
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		children.addAll(this.entities);
		children.addAll(this.packages);
		return children;
	}
	
	
}
