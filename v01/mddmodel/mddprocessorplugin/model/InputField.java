package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mddprocessorplugin.model.layouts.LayoutStyle;
import mddprocessorplugin.model.util.AttributeUtil;
import mddprocessorplugin.model.util.StringUtils;
import mddprocessorplugin.vdsl.InputFieldTypes;

import org.apache.commons.lang.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.engeny.dsl.engen.FieldDecoratorType;

public class InputField extends BaseModel {

	protected transient static Logger log = LoggerFactory
			.getLogger(InputField.class);

	/**
	 * Represents an event attached to the InputField. Events can be click,
	 * mouseover, mouseout, etc.
	 * 
	 * @author eugenio
	 * 
	 */
	public static class InputFieldEvent {
		private String eventType;
		private String eventValue;
		private ModelTarget successTarget;
		private ModelTarget failureTarget;
		private String eventComponent;

		public InputFieldEvent() {

		}

		public InputFieldEvent(String eventType, String eventValue) {
			this(eventType);
			this.setEventValue(eventValue);
		}

		public InputFieldEvent(String eventType) {
			this.eventType = eventType;
		}

		public void setEventType(String eventType) {
			this.eventType = eventType;
		}

		public String getEventType() {
			return eventType;
		}

		public void setEventValue(String eventValue) {
			this.eventValue = eventValue;
		}

		public String getEventValue() {
			return eventValue;
		}

		public void setSuccessTarget(ModelTarget target) {
			this.successTarget = target;
		}

		public ModelTarget getSuccessPath() {
			return this.successTarget;
		}

		public void setFailureTarget(ModelTarget failureTarget) {
			this.failureTarget = failureTarget;
		}

		public ModelTarget getFailureTarget() {
			return failureTarget;
		}

		public void setEventComponent(String eventComponent) {
			this.eventComponent = eventComponent;
		}

		public String getEventComponent() {
			return eventComponent;
		}

		@Override
		public String toString() {
			return "InputFieldEvent [eventType=" + eventType + ", eventValue="
					+ eventValue + ", successTarget=" + successTarget
					+ ", failureTarget=" + failureTarget + ", eventComponent="
					+ eventComponent + "]";
		}

	}

	/**
	 * This class represents a DisplayField in a particular Field in a view.
	 * This is primarily used by table fields where each field in a table may
	 * have a property defined and also a label to display the table header
	 * column.
	 * 
	 * @author eugenio
	 * 
	 */
	public static class DispalyField {
		private String property;
		private String label;
		private String displayProperty;

		public DispalyField() {
		}

		/**
		 * Creates the display field
		 * 
		 * @param prop
		 *            the property for this display field
		 * @param lab
		 *            the label for this display field
		 * @param displayProperty
		 *            property used to display the field on screen
		 */
		public DispalyField(String prop, String lab, String displayProperty) {
			setProperty(prop);
			// If the label is not defined make it a phrase
			// from the property itself
			if (StringUtils.trimToNull(lab) == null) {
				setLabel(StringUtils.toPhrase(getProperty()));
			} else {
				setLabel(lab);
			}
			// If the display property is not defined make the same as
			// the field property itself
			if (StringUtils.trimToNull(displayProperty) == null) {
				setDisplayProperty(getProperty());
			} else {
				setDisplayProperty(displayProperty);
			}
		}

		public void setDisplayProperty(String displayProperty) {
			this.displayProperty = displayProperty;
		}

		public String getDisplayProperty() {
			return this.displayProperty;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public void setProperty(String property) {
			this.property = property;
		}

		public String getProperty() {
			return property;
		}
	}

	/**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;
	/** The validations for the controller. Can be different from attributes. **/
	private List<AttributeValidator> validators = new ArrayList<AttributeValidator>();
	private int ordinalPosition;
	private String size;
	private String maxDisplaySize;
	private String description;
	private String label;
	/**
	 * Usually this is required as the property itself can be an implementation
	 * dependent property, such as an ID, and the property used to display the
	 * UI to the user is another, such as NAME.
	 */
	private String displayProperty;

	/**
	 * Defines the CSS style for the field
	 */
	private String style = StringUtils.EMPTY;
	/**
	 * Defines the CSS style for the field label when applicable
	 */
	private String labelStyle;

	/** How many columns this field will occupy **/
	private String colSpan = Defaults.DEFAULT_FIELD_COLSPAN;
	/** Type for this field to appear on the view. **/
	private String formFieldType;
	/** The id attribute for this field on the view. **/
	private String fieldId;
	/** Same as the attribute it stands for **/
	private String type;
	/** The initial value for this field **/
	private String initialValue;
	/** Required fields will have validation. **/
	private Boolean required = Boolean.FALSE;
	/** The model attribute this input field models and represents **/
	private Attribute parentAttribute;
	/** If the attribute was already defined in the base form **/
	private Boolean existisOnBaseForm = Boolean.FALSE;
	/** If the attribute should be visible on views. Default is true **/
	private Boolean visible = Boolean.TRUE;
	/**
	 * Target for this field, if it is a button for example.
	 **/
	// private Controller controllerTarget;
	// private PresentationView viewTarget;
	private ModelTarget target;
	/**
	 * Fields to display name in the simplest case or multiple like in a table:
	 * name, age, gender, etc.
	 */
	private List<DispalyField> displayFields = new ArrayList<DispalyField>();
	/**
	 * This is fieldSource for this field. Perhaps like an entity or collection
	 * that the view should use to lookup in the context to retrieve its value
	 * to display.
	 */
	private String fieldSource;
	/**
	 * This is the controller source for this field. Probably the controller
	 * that gets called on the presence of some event
	 */
	private Controller fieldSourceController;
	/**
	 * This will indicate which field in the displayFields collection should be
	 * used as the key for tables for example.
	 */
	private String key;

	/**
	 * Used primarily to buttons to indicate that one is the default button for
	 * the form.
	 */
	private Boolean isDefault = Boolean.FALSE;
	/**
	 * Events that can be attached to a field
	 */
	private List<InputFieldEvent> events = new ArrayList<InputFieldEvent>();
	/**
	 * The type of initializer this field has, if any
	 */
	private InitializerType initializerType;
	/**
	 * Indicates whether this field is disabled. Default is FALSE
	 */
	private Boolean disabled = Boolean.FALSE;
	/**
	 * Defines the model type as in DSL.
	 */
	private ModelType modelType;
	/**
	 * Pattern for displaying fields like dates (eg: dd/MM/yyyy)
	 */
	private String displayPattern;
	/**
	 * The property this field references. Can be a direct property reference to
	 * a form property or a property (or nested property) of a bean defined by
	 * the fieldSource attribute.
	 */
	private String fieldProperty;
	/**
	 * The bean (object) that holds the value for this field
	 */
	private String fieldBean;
	/**
	 * The control this field observes for changes
	 */
	private String sourceControl;
	/**
	 * The type of decorator this field uses. Usually a dialog for search or
	 * something similar.
	 * 
	 */
	private List<String> fieldDecorators = new ArrayList<String>();
	/**
	 * If the attribute is hidden
	 */
	private boolean hidden;
	private List<InputFieldEvent> updateEvents = new ArrayList<InputFieldEvent>();

	/**
	 * This is a proxy for a target. If the real target wasn't resolved during
	 * first parse, then it will be resolved using the proxy after all elements
	 * have been resolved.
	 */
	// private ModelProxy<ModelTarget> targetProxy;

	public InputField() {
		super("inputfield");
		initCollections();
	}

	public InputField(StereotypedItem parent) {
		super(parent);
	}

	public InputField(StereotypedItem parent, Attribute attribute) {
		this(parent);
		initInputField(attribute);
	}

	public InputField(StereotypedItem parent, String name, String type) {
		this(parent);
		this.setName(name);
		this.setType(type);
		this.setFormFieldType(type);
		this.setCommonAttributesAfterNameHasBeenSet();
	}

	public void initInputField(Attribute attribute) {
		initCollections();
		super.setStereotypeName("inputfield");
		this.setParentAttribute(attribute);
		this.setName(attribute.getName());
		// Label to show in the view.
		this.setLabel(attribute.getLabel());
		// Sets the position for this field in views or plug-in.
		this.setOrdinalPosition(attribute.getOrdinalPosition());
		this.setSize(attribute.getSize());
		// TODO: Verify this setting for a reasonable setting for tables
		this.setMaxDisplaySize(attribute.getMaxDisplaySize());
		this.setType(attribute.getType());
		this.setCommonAttributesAfterNameHasBeenSet();
		// Do not add this here. Not all forms will require this validator
		this.addRequiredValidatorIfApplicable();
	}

	public void initCollections() {
		updateEvents = new ArrayList<InputFieldEvent>();
		events = new ArrayList<InputFieldEvent>();
		displayFields = new ArrayList<DispalyField>();
		// fieldDecorators = new ArrayList<String>();
	}

	public void defineVisibleState(Boolean showOnView) {
		this.setVisible(showOnView);
	}

	/**
	 * Compute the enabled state
	 */
	private void setDisabledState() {
		// If there is a backing attribute set enabled state
		// as it was defined in the attribute
		if (this.getParentAttribute() != null) {
			this.setDisabled(this.getParentAttribute().getDisabled());
		}
		if (this.getEnclosingProject() != null) {
			ViewSection section = ModelUtils.getEnclosingSectionFor(this);
			// If the section is disabled, the fields inside it should also be
			// disabled. Buttons should not respect this
			if (section != null && section.isReadOnly() && !this.isButton()) {
				this.setDisabled(Boolean.TRUE);
			}
		} else {
			// This means we probably don't have a model backing up
			// the InputField creation. This can happen on DSL file
			// creation where there isn't a model backing up the
			// field yet. (TODO: Why there isn't a backing attribute?)
			if (this.isCounter()) {
				// When creating DSL files the counter fields will
				// be disabled and get their values automatically
				// by counter algorithm.
				this.setDisabled(Boolean.TRUE);
			}
		}
	}

	/**
	 * If the parent attribute is set and it has an initializer defined the same
	 * initializer type will be applied to the field so templates can easily
	 * query this property
	 */
	public void addInitializersIfApplicable() {
		if (this.getParentAttribute() != null) {
			InitializerType initializerType = this.getParentAttribute()
					.getInitializerType();
			if (initializerType != null) {
				this.setInitializerType(initializerType);
			}
		}
	}

	public void setInitializerType(InitializerType initializerType) {
		this.initializerType = initializerType;
	}

	public InitializerType getInitializerType() {
		return initializerType;
	}

	private void setCommonAttributesAfterNameHasBeenSet() {
		// The fieldProperty and fieldId properties are initialized with
		// the field's name. This is the default behavior,
		// the property this field references is a property
		// with the field's name.
		this.setFieldProperty(this.getName());
		this.setFieldId(this.getName());
		// The label by default is the field name as a label
		this.setLabel(AttributeUtil.createLabelFor(this.getName()));
		// The description for this field
		this.setDescription("Field " + this.getName());
		// The default size applied according to field's type
		this.setSize(AttributeUtil.getDisplaySizeFor(this.getType()));
		// Sets the property used to display values to user UI
		this.setDisplayProperty();
		// Add initializers if applicable
		this.addInitializersIfApplicable();
		// By default fields are to be show on view
		// this can be changed later by defineFormFieldType method
		this.defineVisibleState(Boolean.TRUE);
		// TODO: Use this in the forms.
		this.defineFormFieldType();
		// Defines the fieldSource property based on
		// its parent attribute type.
		this.defineFieldSource();
		// Defines the fieldBean property based on
		// its target controller form
		this.defineFieldBean();
		// ///////////////////////////////////////////////
		// Defines the class used to display the field
		// TODO: Remove after layouts finalization
		// this.defineFormFieldDisplayClass();
		// ///////////////////////////////////////////////
		// Will compute the disabled state based on
		// backing attribute state
		this.setDisabledState();
		// Define the model type
		this.setModelType();
		// Define the decorators if applicable
		this.defineFieldDecorators();
		// We need to immediately define the the field and label style classes
		// as defined in style configuration before overriding it at field
		// configuration
		this.defineFieldStyle();
	}

	/**
	 * Define field styles according to DSL field_style and layout_style tags
	 */
	private void defineFieldStyle() {
		if (this.getEnclosingProject() == null) {
			log.warn("At this point we can't define the style "
					+ "since no parent project was defined.");
		} else {
			LayoutStyle layoutStyle = getEnclosingProject().getControlLayer()
					.getCurrentLayoutStyle();
			if (layoutStyle != null) {
				layoutStyle.defineFieldStyle(this);
			} else {
				log.warn("The layout style was not defined for this project!!");
			}
		}
	}

	/**
	 * Define default decorators for fields
	 */
	public void defineFieldDecorators() {
		// TODO: Make this configurable
		if (this.isTable()
				|| this.isRelationAttributeTable()
				&& !StringUtils.contains(this.getFieldDecorators(),
						FieldDecoratorType.ADD_RELATION_DIALOG.getName())) {
			// Add the default table field decorator, a add relation dialog
			this.addFieldDecorator(FieldDecoratorType.ADD_RELATION_DIALOG
					.getName());
		} else if (this.isSelect()
				&& !StringUtils.contains(this.getFieldDecorators(),
						FieldDecoratorType.DETAIL_DIALOG.getName())) {
			// TODO: Use this value in FieldRender-select.vm template
			this.addFieldDecorator(FieldDecoratorType.DETAIL_DIALOG.getName());
		} else if (StringUtils.isEmpty(this.getFormFieldType())) {
			this.addFieldDecorator("NullFormFieldTypeDecorator");
		}
	}

	/**
	 * Defines this field bean as the form this field is inserted in.
	 */
	private void defineFieldBean() {
		if (this.getParent() != null) {
			StereotypedItem parentSection = this.getParent();
			if (parentSection instanceof ViewSection) {
				ViewSection section = (ViewSection) parentSection;
				Controller formTarget = section.getFormTarget();
				if (formTarget != null && formTarget.getForm() != null) {
					// this.setFieldBean(formTarget.getForm().getName());
					// TODO: This is specific to SIF, generalize o get better
					// bean definition
					this.setFieldBean("form");
					return;
				} else {
					// If there is no target, use the default field bean name
					// a defined by the configuration
					this.setFieldBean(section.getDefaultInputFieldBeanName());
					return;
				}
			}
			// If there is no target, use the default field bean name
			// a defined by the property itself
			this.setFieldBean(StringUtils.EMPTY);
		}
	}

	/**
	 * Defines the fieldSource property for this field. If it is a reference to
	 * a single instance, verify if the collection for this instance is loaded.
	 * The pattern is a request attribute that will have all objects of
	 * referenced property, which the name will be the relation name appended
	 * with an "s" like "project"s.
	 */
	public void defineFieldSource() {
		// If not defined it will be the field property
		Attribute att = this.getParentAttribute();
		if (att != null) {
			// If this field is a derived field from a relation
			if (att.getFromRelation()) {
				// If the parent attribute is a collection the it should
				// be shown in a table with its elements
				if (att.isCollection()) {
					// The default behavior will be that the field
					// will show the collection defined by the controller
					// this.setFieldSource(this.getControllerTarget().getTargetName());
					this.setFieldSource("returnCollection");
					// The type for this table is also different as the template
					// can't be the same as the regular table template
					// TODO: This is too specific, create another table type in
					// DSL
					this.setFormFieldType(FieldType.RELATION_ATTRIBUTE_TABLE
							.toString());
				} else {
					// If not a collection a select should be shown
					this.setFieldSource(att.getName() + "s");
				}
			}
		}
	}

	public void setDisplayProperty() {
		/*
		 * the attribute, which the value will be looked up on the bean to be
		 * used to show in selects or lists, and recently on labels. Note that
		 * this will be overridden by EntitySecondPassStrategyForLabels, so why
		 * this is still here?
		 */
		if (this.getParentAttribute() != null) {
			this.setDisplayProperty(this.getParentAttribute()
					.getRelationAttributeLabel());
		} else {
			this.setDisplayProperty(Defaults.DEFAULT_RELATION_ATTRIBUTE_LABEL);
		}
	}

	/**
	 * Computes the correct path used to update the field value when the source
	 * control for this field is updated. It will use the property, then bean
	 * and the fieldSource to compute the value.
	 * 
	 * @return the correct path to use in SourceControlRender template
	 */
	public String computeSource() {
		StringBuilder sb = new StringBuilder();
		// If the field source was defined use it
		if (StringUtils.isNotEmpty(this.getFieldSource())) {
			sb.append(this.getFieldSource());
		}
		// if the field source was not defined then use
		// the property. I'm not sure if this can even happen
		if (StringUtils.isEmpty(this.getFieldSource())) {
			sb.append(".").append(this.getFieldProperty());
		}
		return sb.toString();
	}

	/**
	 * Computes the correct value for display property. We are using a
	 * convention that if the value starts with a exclamation mark it shouldn't
	 * be considered.
	 */
	public String computeDisplayProperty() {
		if (StringUtils.startsWith(this.getDisplayProperty(), "!")) {
			return StringUtils.EMPTY;
		}
		return this.getDisplayProperty();
	}

	public void setModelType() {
		Attribute attribute = this.getParentAttribute();
		if (attribute != null) {
			// Set the DSL model type so we can query it later
			this.setModelType(attribute.getModelType());
			this.setDisplayPattern(DisplayPattern.getPatternFor(this
					.getModelType()));
		}
	}

	public void defineFormFieldType() {
		// If the form field type has been already defined by
		// the user, don't override it.
		if (StringUtils.isNotEmpty(this.getFormFieldType())) {
			return;
		}
		@SuppressWarnings("rawtypes")
		Class type = this.getFieldTypeClass();
		ControlLayer controlLayer = getControlLayer();
		// This is the default. Can be overridden later
		this.setFormFieldType(FieldType.TEXT.toString());
		if (this.isDateType(type)) {
			this.setFormFieldType(FieldType.DATE.toString());
		}
		if (this.isTimeType(type)) {
			this.setFormFieldType(FieldType.TIME.toString());
		}
		if (this.isNumber(this.getFieldTypeClass())) {
			this.setFormFieldType(FieldType.NUMBER.toString());
		}
		if (this.isBoolean(this.getFieldTypeClass())) {
			this.setFormFieldType(FieldType.CHECK.toString());
		}
		Attribute att = this.getParentAttribute();
		if (att != null) {
			if (att.getFromRelation()) {
				// If the parent attribute is a collection the it should
				// be shown in a table with its elements
				if (att.isCollection()) {
					this.setFormFieldType(FieldType.TABLE.toString());
				} else {
					// If not a collection a select should be shown
					this.setFormFieldType(FieldType.SELECT.toString());
				}
			} else if (att.isCollection() && !att.getFromRelation()) {
				// This field is not from a relation but it is still a
				// collection. This may happen when the collection this
				// attribute refers to is from a base type like String (e.g.:
				// Set<String> collection)
				this.setFormFieldType(FieldType.TABLE.toString());
			} else {
				if (att.isHashed()) {
					// In this case, the user intend to use a hash
					// or sometimes hiding the field in database or
					// view. Use a password field type to not show characters
					// in the view. TODO: Maybe we can have a hashed property
					// in the field and let the template decide it.
					this.setFormFieldType(InputFieldTypes.PASSWORD.toString());
				}
				// As we can see, hidden fields take precedence on password
				// fields
				// if both properties (hashed and hidden) are set to true
				if (att.isHidden()) {
					this.setFormFieldType(InputFieldTypes.HIDDEN.toString());
					this.setHidden(true);
					// Hidden fields are not visible
					this.defineVisibleState(false);
				}
			}
			if (InitializerType.counter.equals(att.getInitializerType())) {
				this.setFormFieldType(FieldType.COUNTER.toString());
			}
		}
		defineTextAreaIfAttributeSizeRequires(controlLayer);
	}

	private void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	private ControlLayer getControlLayer() {
		Project project = this.getEnclosingProject();
		ControlLayer controlLayer = null;
		if (project != null) {
			controlLayer = project.getControlLayer();
		}
		return controlLayer;
	}

	/**
	 * @param controlLayer
	 */
	private void defineTextAreaIfAttributeSizeRequires(ControlLayer controlLayer) {
		try {
			Attribute parentAttribute2 = this.getParentAttribute();
			if (parentAttribute2 != null
					&& StringUtils.trimToNull(parentAttribute2.getSize()) != null) {
				String size2 = parentAttribute2.getSize();
				// Verify the length. If more than MAX_TEXT_BOX_SIZE than it
				// needs to be a TEXT_AREA
				String maxSize = Defaults.MAX_TEXT_BOX_SIZE;
				if (controlLayer != null) {
					if (StringUtils.isNull(controlLayer.getMaxTextBoxSize())) {
						controlLayer.setMaxTextBoxSize(maxSize);
					} else {
						maxSize = controlLayer.getMaxTextBoxSize();
					}
				}
				Integer intSize2 = Integer.valueOf(size2);
				Integer textBoxMaxSize = Integer.valueOf(maxSize);
				if (intSize2 > textBoxMaxSize) {
					this.setFormFieldType(FieldType.TEXTAREA.toString());
				}
			}
		} catch (Exception ex) {
			log.debug("ParentAttribute: " + this.getParentAttribute());
			if (this.getParentAttribute() != null) {
				log.debug("Size: " + this.getParentAttribute().getSize());
			}
			log.error(ex.toString(), ex);
		}

	}

	public boolean isTextArea() {
		return FieldType.TEXTAREA.toString().equals(this.getFormFieldType());
	}

	public boolean isTextField() {
		return FieldType.TEXT.toString().equals(this.getFormFieldType());
	}

	public boolean isSelect() {
		return FieldType.SELECT.toString().equals(this.getFormFieldType());
	}

	public boolean isTable() {
		return FieldType.TABLE.toString().equals(this.getFormFieldType());
	}

	public boolean isList() {
		return FieldType.LIST.toString().equals(this.getFormFieldType());
	}

	public boolean isRelationAttributeTable() {
		return FieldType.RELATION_ATTRIBUTE_TABLE.toString().equals(
				this.getFormFieldType());
	}

	public boolean isCounter() {
		return FieldType.COUNTER.toString().equals(this.getFormFieldType());
	}

	public boolean isButton() {
		return FieldType.BUTTON.toString().equals(this.getFormFieldType());
	}

	public boolean isCancel() {
		return FieldType.CANCEL.toString().equals(this.getFormFieldType());
	}

	public boolean isReset() {
		return FieldType.RESET.toString().equals(this.getFormFieldType());
	}

	public boolean isRadio() {
		return FieldType.RADIO.toString().equals(this.getFormFieldType());
	}

	public boolean isCheck() {
		return FieldType.CHECK.toString().equals(this.getFormFieldType());
	}

	/**
	 * Returns if this field uses a control label like text boxes. This is
	 * usually false for buttons as its label is inside the button itself.
	 * 
	 * @return true if this field uses a control label
	 */
	public boolean useControlLabel() {
		return !(isCheck() || isButton() || isCancel() || isReset());
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		children.addAll(this.getValidators());
		return children;
	}

	/**
	 * @return the validators
	 */
	public List<AttributeValidator> getValidators() {
		return validators;
	}

	/**
	 * @param validators
	 *            the validators to set
	 */
	public void setValidators(List<AttributeValidator> validators) {
		for (AttributeValidator validator : validators) {
			validator.setParent(this);
		}
		this.validators = validators;
		this.notifyModelChangeListeners();
	}

	public void addValidator(AttributeValidator validator) {
		validator.setParent(this);
		this.getValidators().add(validator);
		this.notifyModelChangeListeners();
	}

	public void removeValidator(AttributeValidator validator) {
		this.getValidators().remove(validator);
		this.notifyModelChangeListeners();
	}

	public int getOrdinalPosition() {
		return ordinalPosition;
	}

	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getMaxDisplaySize() {
		return maxDisplaySize;
	}

	public void setMaxDisplaySize(String maxDisplaySize) {
		this.maxDisplaySize = maxDisplaySize;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDisplayProperty() {
		return displayProperty;
	}

	public void setDisplayProperty(String relationAttributeLabel) {
		this.displayProperty = relationAttributeLabel;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param required
	 *            the required to set
	 */
	public void setRequired(Boolean required) {
		this.required = required;
	}

	/**
	 * @return the required
	 */
	public Boolean getRequired() {
		return required;
	}

	/**
	 * Informs if a validation is applicable for this attribute. If no
	 * validation is applicable for this attribute, no validation is added at
	 * all.
	 * 
	 * @return
	 */
	private boolean validationApplicable() {
		boolean applicable = this.getParentAttribute() != null;
		applicable = applicable && !this.getParentAttribute().isPrimaryKey();
		return applicable;
	}

	/**
	 * Adds a required validation to this form field if the base attribute is
	 * required.
	 */
	public void addRequiredValidatorIfApplicable() {
		// If we don't have an parentAttribute or it is a primary key
		// we won't add a required validation.
		if (!this.validationApplicable()) {
			return;
		}
		if (getParentAttribute().isRequired() && !this.hasRequiredValidator()) {
			addRequiredValidator();
		}
	}

	/**
	 * Adds a required validator to the field.
	 */
	public void addRequiredValidator() {
		AttributeValidator validator = new AttributeValidator();
		validator.setRule(ValidatorRule.REQUIRED.toString());
		this.addValidator(validator);
	}

	/**
	 * Removes all required validation to this form field
	 */
	public void removeRequiredValidators() {
		for (Iterator<AttributeValidator> it = this.getValidators().iterator(); it
				.hasNext();) {
			if (it.next().getRule()
					.equalsIgnoreCase(ValidatorRule.REQUIRED.toString())) {
				it.remove();
				return;
			}
		}
	}

	public boolean hasRequiredValidator() {
		for (AttributeValidator av : this.getValidators()) {
			if (av.getRule()
					.equalsIgnoreCase(ValidatorRule.REQUIRED.toString())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Add validation to the attribute type, like check for integers, bytes and
	 * dates.
	 */
	public void addTypeSafetyValidators() {
		if (!this.validationApplicable()) {
			return;
		}
		@SuppressWarnings("rawtypes")
		Class type = getFieldTypeClass();
		if (type == null) {
			return;
		}
		AttributeValidator validator = new AttributeValidator();
		validator.setAttributeName(this.getName());
		if (this.isNumber(type)) {
			if (Integer.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.INTEGER.toString());
			} else if (Byte.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.BYTE.toString());
			} else if (Short.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.SHORT.toString());
			} else if (Long.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.LONG.toString());
			} else if (Float.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.FLOAT.toString());
			} else if (Double.class.isAssignableFrom(type)) {
				validator.setRule(ValidatorRule.DOUBLE.toString());
			}
		}
		if (this.isDateType(type) || this.isTimeType(type)) {
			validator.setRule(ValidatorRule.DATE.toString());
			validator.setDatePattern(this.getDisplayPattern());
		}
		// Override the pattern for time type
		if (this.isTimeType(type)) {
			validator.setDatePattern(this.getDisplayPattern());
		}
		// If a rule has been defined add the validator to the collection.
		if (StringUtils.trimToNull(validator.getRule()) != null) {
			this.addValidator(validator);
		}
	}

	private boolean isBoolean(@SuppressWarnings("rawtypes") Class type) {
		boolean isBoolean = (type != null);
		isBoolean = isBoolean
				&& ("java.lang.Boolean".equals(type.getName()) || "boolean"
						.equals(type.getName()));
		return isBoolean;
	}

	private boolean isDateType(@SuppressWarnings("rawtypes") Class type) {
		boolean isDate = (type != null);
		isDate = isDate
				&& ("java.util.Date".equals(type.getName()) || "java.sql.Date"
						.equals(type.getName()));
		return isDate;
	}

	private boolean isTimeType(@SuppressWarnings("rawtypes") Class type) {
		boolean isTime = (type != null);
		isTime = isTime
				&& ("java.sql.Timestamp".equals(type.getName()) || "java.sql.Time"
						.equals(type.getName()));
		return isTime;
	}

	private boolean isNumber(@SuppressWarnings("rawtypes") Class type) {
		return type != null
				&& (Number.class.isAssignableFrom(type) || type.isPrimitive());
	}

	/**
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Class getFieldTypeClass() {
		Class type = null;
		try {
			if (getParentAttribute() != null) {
				type = ClassUtils.getClass(getParentAttribute().getType());
			}
		} catch (Exception ex) {
			return null;
		}
		return type;
	}

	public boolean isDate() {
		// If there is a back end model attribute check it
		if (getParentAttribute() != null) {
			return getParentAttribute().isDate();
		}
		// The field does not refer to a model attribute
		// check its type class and type
		if (this.isDateType(this.getFieldTypeClass())) {
			return true;
		}
		if (AttributeUtil.isJavaDateType(this.getType())) {
			return true;
		}
		// Checks failed
		return false;
	}

	public boolean isTime() {
		// If there is a back end model attribute check it
		if (getParentAttribute() != null) {
			return getParentAttribute().isTime();
		}
		// The field does not refer to a model attribute
		// check its type class and type
		if (this.isTimeType(this.getFieldTypeClass())) {
			return true;
		}
		if (AttributeUtil.isJavaTimeType(this.getType())) {
			return true;
		}
		// Checks failed
		return false;
	}

	/**
	 * This is not implemented correctly yet
	 */
	public void addMaxAndMinValidators() {
		/*
		 * if (!this.validationApplicable()) { return; } AttributeValidator
		 * validator = new AttributeValidator();
		 * validator.setRule(ValidatorRule.MAXLENGTH.toString());
		 * getParentAttribute().addValidator(validator);
		 */
	}

	/**
	 * @param formFieldType
	 *            the formFieldType to set
	 */
	public void setFormFieldType(String formFieldType) {
		this.formFieldType = formFieldType;
	}

	/**
	 * @return the formFieldType
	 */
	public String getFormFieldType() {
		return formFieldType;
	}

	/**
	 * @param initialValue
	 *            the initialValue to set
	 */
	public void setInitialValue(String initialValue) {
		this.initialValue = initialValue;
	}

	/**
	 * @return the initialValue
	 */
	public String getInitialValue() {
		return initialValue;
	}

	/**
	 * @param fieldId
	 *            the fieldId to set
	 */
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	/**
	 * @return the fieldId
	 */
	public String getFieldId() {
		return fieldId;
	}

	/**
	 * @param existisOnBaseForm
	 *            the existisOnBaseForm to set
	 */
	public void setExistisOnBaseForm(Boolean existisOnBaseForm) {
		this.existisOnBaseForm = existisOnBaseForm;
	}

	/**
	 * @return the existisOnBaseForm
	 */
	public Boolean getExistisOnBaseForm() {
		return existisOnBaseForm;
	}

	/**
	 * Returns the model attribute this input field models and represents
	 * 
	 * @return the parent attribute for this field
	 */
	public Attribute getParentAttribute() {
		return parentAttribute;
	}

	/**
	 * Changes the model attribute this input field models and represents
	 * 
	 * @param parentAttribute
	 *            the model attribute this field models and represents
	 */
	public void setParentAttribute(Attribute parentAttribute) {
		this.parentAttribute = parentAttribute;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the visible
	 */
	public Boolean getVisible() {
		return visible;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof AttributeValidator) {
			this.addValidator((AttributeValidator) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof AttributeValidator) {
			this.removeValidator((AttributeValidator) child);
			return true;
		}
		return false;
	}

	public void setTarget(ModelTarget target) {
		if (target == null) {
			this.target = null;
			return;
		}
		if (target.getParent() == null) {
			throw new IllegalModelElementException(target);
		}
		log.debug("Setting target for field [" + this.getName() + "] :> ["
				+ target.getName() + ", parent: "
				+ target.getParent().getName() + "]");
		this.target = target;
	}

	public ModelTarget getTarget() {
		log.debug("Getting target for field [" + this.getName()
				+ "]. Field.target: " + target);
		if (target != null) {
			if (target.getParent() == null) {
				throw new IllegalModelElementException(target);
			}
			return target;
		} else {
			return null;
		}
	}

	public void addDisplayField(DispalyField field) {
		this.getDisplayFields().add(field);
	}

	public List<DispalyField> getDisplayFields() {
		return this.displayFields;
	}

	public void setDisplayFields(List<DispalyField> displayFields) {
		this.displayFields = displayFields;
	}

	public void removeDisplayField(String field) {
		this.getDisplayFields().remove(field);
	}

	public void setFieldSource(String source) {
		this.fieldSource = source;
	}

	public String getFieldSource() {
		return fieldSource;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setColSpan(String colSpan) {
		this.colSpan = colSpan;
	}

	/**
	 * How many columns this field will occupy
	 * 
	 * @return
	 */
	public String getColSpan() {
		return colSpan;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	/*
	 * public void setControllerTarget(Controller controllerTarget) {
	 * this.controllerTarget = controllerTarget; }
	 * 
	 * public Controller getControllerTarget() { return controllerTarget; }
	 * 
	 * public void setViewTarget(PresentationView viewTarget) { this.viewTarget
	 * = viewTarget; }
	 * 
	 * public PresentationView getViewTarget() { return viewTarget; }
	 */

	public void addEvent(InputFieldEvent event) {
		this.events.add(event);
	}

	public List<InputFieldEvent> getEvents() {
		return this.events;
	}

	/**
	 * Returns whether this field has an event that matches the event type
	 * passed in
	 * 
	 * @param eventType
	 * @return true if the field has an event of this type
	 */
	public boolean hasEvent(String eventType) {
		if (StringUtils.trimToNull(eventType) == null) {
			return false;
		}
		for (InputFieldEvent event : this.events) {
			if (eventType.equals(event.getEventType())) {
				return true;
			}
		}
		return false;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setModelType(ModelType modelType) {
		this.modelType = modelType;
	}

	public ModelType getModelType() {
		return modelType;
	}

	public void setDisplayPattern(String displayPattern) {
		this.displayPattern = displayPattern;
	}

	public String getDisplayPattern() {
		return displayPattern;
	}

	public void setFieldProperty(String value) {
		this.fieldProperty = value;
	}

	public String getFieldProperty() {
		return fieldProperty;
	}

	public void setFieldBean(String fieldBean) {
		this.fieldBean = fieldBean;
	}

	public String getFieldBean() {
		return fieldBean;
	}

	public void setSourceControl(String sourceControl) {
		this.sourceControl = sourceControl;
	}

	public String getSourceControl() {
		return sourceControl;
	}

	public Entity getTargetControllerReferencedEntity() {
		// Must get the target controller entity
		// Controller controller = this.getControllerTarget();
		Controller controller = (Controller) this.getTarget();
		if (controller != null) {
			return controller.getEntity();
		}
		return null;
	}

	public Entity getSourceControlReferencedEntity() {
		// We must find the source control in the presentation view
		// of this field.
		InputField sourceControlField = getSourceControlField();
		if (sourceControlField != null) {
			return sourceControlField.getReferencedEntity();
		}
		return null;
	}

	private Entity getReferencedEntity() {
		Entity referencedEntity = null;
		// TODO: First try to get it from the source attribute
		// If there is a scan, the source entity is the scanned entity
		if (getParentViewScannedEntity() != null) {
			Entity sourceEntity = getParentViewScannedEntity();
			// Now we must lookup the property this field references to
			Attribute fieldReferencedAttribute = sourceEntity.getAttribute(this
					.getFieldProperty());
			if (fieldReferencedAttribute != null) {
				String referencedEntityName = fieldReferencedAttribute
						.getType();
				referencedEntity = fieldReferencedAttribute
						.getEnclosingProject().getModelLayer()
						.getEntityByName(referencedEntityName);
			}
		}
		return referencedEntity;
	}

	private Entity getParentViewScannedEntity() {
		ViewSection parentSection = getParentSection();
		if (parentSection != null) {
			List<ViewScan> scans = parentSection.getParentView().getScans();
			for (ViewScan scan : scans) {
				// TODO: Which scanned entity we are looking
				// for, just the first one? This is wrong.
				return (Entity) scan.getTarget();
			}
		}
		return null;
	}

	private ViewSection getParentSection() {
		StereotypedItem parent = this.getParent();
		ViewSection parentSection = null;
		if (ViewSection.class.isAssignableFrom(parent.getClass())) {
			parentSection = (ViewSection) parent;
		}
		return parentSection;
	}

	/**
	 * Returns the field the sourceControl property points to.
	 */
	private InputField getSourceControlField() {
		ViewSection parentSection = getParentSection();
		if (parentSection != null) {
			InputField sourceControl = parentSection.getField(this
					.getSourceControl());
			return sourceControl;
		}
		return null;
	}

	public void defineFieldKey() {
		// If this is a table or list, try to define the key property
		// if it was not already defined.
		if (StringUtils.isEmpty(this.getKey())) {
			if (this.isTable() || this.isList()) {
				String definedKey = Defaults.DEFAULT_TABLE_KEY_ATTRIBUTE_NAME;
				this.setKey(definedKey);
			}
		}
	}

	public boolean isHidden() {
		return hidden;
	}

	public void addFieldDecorator(String name) {
		if (!this.fieldDecorators.contains(name)) {
			this.fieldDecorators.add(name);
		}
	}

	public boolean hasDecorators() {
		return !this.fieldDecorators.isEmpty();
	}

	public List<String> getFieldDecorators() {
		return fieldDecorators;
	}

	public void setFieldDecorators(List<String> fieldDecorators) {
		this.fieldDecorators = fieldDecorators;
	}

	public Controller getFieldSourceController() {
		return fieldSourceController;
	}

	public void setFieldSourceController(Controller fieldSourceController) {
		this.fieldSourceController = fieldSourceController;
	}

	public void addUpdateEvent(InputFieldEvent inputFieldEvent) {
		this.updateEvents.add(inputFieldEvent);
	}

	public List<InputFieldEvent> getUpdateEvents() {
		return this.updateEvents;
	}

	public void setUpdateEvents(List<InputFieldEvent> inputFieldEvents) {
		this.updateEvents = inputFieldEvents;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * Adds a style to the current style. This is, if the current style is
	 * "current" and the style to add is "new" the resulting style for the field
	 * is "current new".
	 * 
	 * @param style
	 *            the style to add to the current style
	 */
	public void addStyle(String style) {
		this.setStyle(StringUtils.trimToEmpty(this.style) + StringUtils.SPACE
				+ style);
	}

	public String getLabelStyle() {
		return labelStyle;
	}

	public void setLabelStyle(String labelStyle) {
		this.labelStyle = labelStyle;
	}

	@Override
	public String toString() {
		return "InputField [formFieldType="
				+ formFieldType + ", type=" + type + ", getName()=" + getName()
				+ "]";
	}

}
