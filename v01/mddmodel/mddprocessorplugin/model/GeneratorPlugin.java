package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A plugin for define file generation models
 * 
 * @author eugenio
 * 
 */
public class GeneratorPlugin extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String modelType;
	private List<BaseModel> modelRef = new ArrayList<BaseModel>();
	private String template;
	private String destination;
	private String extension;

	public GeneratorPlugin(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
	}
	
	public GeneratorPlugin() {
		this.setStereotypeForThis();
		this.initCollections();
	}

	private void initCollections() {
		modelRef = new ArrayList<BaseModel>();
	}

	private void setStereotypeForThis() {
		this.setStereotypeName("generatorplugin");
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public List<BaseModel> getModelRef() {
		return modelRef;
	}

	public void setModelRef(List<BaseModel> modelRef) {
		this.modelRef = modelRef;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

}
