package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Groups controllers for the application. Realizes the Controller part of MVC.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da Purificacao</a>
 * @created 08/03/2009
 * @version 
 */
public class ControlModule extends BaseModel implements TemplateOwner {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -7636463695358331339L;
	private String namespace = StringUtils.EMPTY;
	private List<Controller> controllers = new ArrayList<Controller>();
	private List<PresentationView> views = new ArrayList<PresentationView>();
	/**
	 * View folder is the location for the views for this Control
	 * Module.
	 */
	private String viewFolder = StringUtils.EMPTY;
	
	/**
	 * Default Constructor. 
	 */
	public ControlModule() {
		super("controlmodule");
		this.controllers = new ArrayList<Controller>();
		this.views = new ArrayList<PresentationView>();
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	public List<PresentationView> getViews() {
		if (views == null) {
			views = new ArrayList<PresentationView>();
		}
		return views;
	}
	
	public List<PresentationView> getDslViews() {
		List<PresentationView> dslviews = new ArrayList<PresentationView>();
		for(PresentationView view : this.getViews()) {
			if (view.isFromDsl()) {
				dslviews.add(view);
			}
		}
		return dslviews;
	} 

	public void setViews(List<PresentationView> views) {
		for(PresentationView view : views) {
			view.setParent(this);
		}
		this.views = views;
		this.notifyModelChangeListeners();
	}
	
	public void addView(PresentationView view) {
		view.setParent(this);
		this.getViews().add(view);
		this.notifyModelChangeListeners();
	}
	
	public void removeView(PresentationView view) {
		this.views.remove(view);
		this.notifyModelChangeListeners();
	}
	
	/**
	 * Returns a PresentationView given its path. Paths must be
	 * unique inside a ControlModule.
	 * @param path the path for the presentation view
	 * @return the PresentationView with the given path
	 */
	public PresentationView getViewByPath(String path) {
		for(PresentationView view : this.getViews()) {
			if (StringUtils.isNotEmpty(view.getPath()) && view.getPath().equalsIgnoreCase(path)) {
				return view;
			}
		}
		return null;
	}

	/**
	 * Returns a PresentationView given its name. Names must be
	 * unique inside a ControlModule.
	 * @param name the name for the presentation view
	 * @return the PresentationView with the given name
	 */
	public PresentationView getViewByName(String name) {
		for(PresentationView view : this.getViews()) {
			if (StringUtils.isNotEmpty(view.getName()) && view.getName().equalsIgnoreCase(name)) {
				return view;
			}
		}
		return null;
	}
	
	/**
	 * Returns whether there is already a PresentationView object
	 * registered with the given path inside this ControlModule.
	 * @param path the path for the presentation view
	 * @return true if there is already a PresentationView registered
	 * with the given path.
	 */
	public boolean hasPresentationViewWithPath(String path) {
		return this.getViewByPath(path) != null;
	}

	/**
	 * Returns whether there is already a PresentationView object
	 * registered with the given name inside this ControlModule.
	 * @param name the name for the presentation view
	 * @return true if there is already a PresentationView registered
	 * with the given path.
	 */
	public boolean hasPresentationViewWithName(String name) {
		return this.getViewByName(name) != null;
	}
	
	/**
	 * @param controllers the controllers to set
	 */
	public void setControllers(List<Controller> controllers) {
		for(Controller p : controllers) {
			p.setParent(this);
		}
		this.controllers = controllers;
		this.notifyModelChangeListeners();
	}

	public void addController(Controller controller) {
		controller.setParent(this);
		this.getControllers().add(controller);
		this.notifyModelChangeListeners();
	}
	
	public void removeController(Controller controller) {
		List<Controller> removable = new ArrayList<Controller>();
		for(Controller ctr : this.getControllers()) {
			if (ctr.getName() != null && ctr.getName().equals(controller.getName())) {
				removable.add(ctr);
			}
		}
		this.getControllers().removeAll(removable);
		this.getControllers().remove(controller);
		this.notifyModelChangeListeners();
	}
	
	/**
	 * @return the controllers
	 */
	public List<Controller> getControllers() {
		if (controllers == null) {
			this.controllers = new ArrayList<Controller>();
		}
		return controllers;
	}
	
	public List<Controller> getDslControllers() {
		List<Controller> dslControllers = new ArrayList<Controller>();
		for(Controller controller : this.getControllers()) {
			if (controller.isFromDsl()) {
				dslControllers.add(controller);
			}
		}
		return dslControllers;
	}
	
	public List<Controller> getNonDslControllers() {
		List<Controller> nonDslControllers = new ArrayList<Controller>();
		for(Controller controller : this.getControllers()) {
			if (!controller.isFromDsl()) {
				nonDslControllers.add(controller);
			}
		}
		return nonDslControllers;
	}	
	

	@Override
	public List<StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		children.addAll(this.getControllers());
		children.addAll(this.getViews());
		return children;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Controller) {
			this.addController((Controller) child);
			return true;
		}
		if (child instanceof PresentationView) {
			this.addView((PresentationView) child);
			return true;
		}		
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Controller) {
			this.removeController((Controller) child);
			return true;
		}
		if (child instanceof PresentationView) {
			this.removeView((PresentationView) child);
			return true;
		}				
		return false;
	}

	public String getViewFolder() {
		return viewFolder;
	}

	public void setViewFolder(String viewFolder) {
		this.viewFolder = viewFolder;
	}
	
	public List<String> getManagedEntitiesNames() {
		List<String> entities = new ArrayList<String>();
		for(Controller controller : this.getControllers()) {
			if (!entities.contains(controller.getEntityName())) {
				entities.add(controller.getEntityName());
			}
		}
		return entities;
	}

	public boolean hasController(Controller controller) {
		for(Controller contr : this.getControllers()) {
			if (contr.getName() == null && controller.getName() == null) {
				return true;
			}
			if  (contr.getName() != null && contr.getName().equals(controller.getName())) {
				return true;
			}
		}
		return false;
	}

	public Controller getControllerByName(String controllerName) {
		for(Controller controller : this.controllers) {
			if (StringUtils.trimToEmpty(controllerName).equals(controller.getName())) {
				return controller;
			}
		}
		return null;
	}

	public ModelTarget getModelTargetByName(String targetName) {
		List<ModelTarget> targets = new ArrayList<ModelTarget>();
		targets.addAll(this.controllers);
		targets.addAll(this.views);
		for(ModelTarget target : targets) {
			if (StringUtils.trimToEmpty(targetName).equals(target.getName())) {
				return target;
			}
		}
		return null;
	}
	
}
