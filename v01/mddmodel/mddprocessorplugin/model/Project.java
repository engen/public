package mddprocessorplugin.model;

import java.util.Arrays;
import java.util.List;

public class Project extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -2483814105386764081L;
	private ProjectConfig projectConfig;
	private ModelLayer modelLayer;
	private ControlLayer controlLayer;
	private DataLayer dataLayer;
	private DiagramLayer diagramLayer;
	private PluginLayer pluginLayer;

	public Project() {
		super("project");
		projectConfig = new ProjectConfig(this);
		projectConfig.setParent(this);
		modelLayer = new ModelLayer(this);
		modelLayer.setParent(this);
		controlLayer = new ControlLayer(this);
		controlLayer.setParent(this);
		dataLayer = new DataLayer(this);
		dataLayer.setParent(this);
		diagramLayer = new DiagramLayer(this);
		diagramLayer.setParent(this);
		pluginLayer = new PluginLayer(this);
		pluginLayer.setParent(this);
	}
	
	
	/**
	 * @return the projectConfig
	 */
	public ProjectConfig getProjectConfig() {
		if (projectConfig == null) {
			this.projectConfig = new ProjectConfig();
			this.projectConfig.setParent(this);
		}
		return projectConfig;
	}

	/**
	 * @param projectConfig
	 *            the projectConfig to set
	 */
	public void setProjectConfig(ProjectConfig projectConfig) {
		projectConfig.setParent(this);
		this.projectConfig = projectConfig;
	}

	/**
	 * @return the modelLayer
	 */
	public ModelLayer getModelLayer() {
		if (modelLayer == null) {
			this.modelLayer = new ModelLayer(this);
			this.modelLayer.setParent(this);
		}
		return modelLayer;
	}

	/**
	 * @param modelLayer
	 *            the modelLayer to set
	 */
	public void setModelLayer(ModelLayer modelLayer) {
		modelLayer.setParent(this);
		this.modelLayer = modelLayer;
	}

	/**
	 * @return the controlLayer
	 */
	public ControlLayer getControlLayer() {
		if (this.controlLayer == null) {
			this.controlLayer = new ControlLayer();
			this.controlLayer.setParent(this);
		}
		return controlLayer;
	}

	/**
	 * @param controlLayer
	 *            the controlLayer to set
	 */
	public void setControlLayer(ControlLayer controlLayer) {
		controlLayer.setParent(this);
		this.controlLayer = controlLayer;
	}

	/**
	 * @return the dataLayer
	 */
	public DataLayer getDataLayer() {
		if (this.dataLayer == null) {
			this.dataLayer = new DataLayer();
			this.dataLayer.setParent(this);
		}
		return dataLayer;
	}

	/**
	 * @param dataLayer
	 *            the dataLayer to set
	 */
	public void setDataLayer(DataLayer dataLayer) {
		dataLayer.setParent(this);
		this.dataLayer = dataLayer;
	}

	@Override
	public List<StereotypedItem> getChildren() {
		return Arrays.asList(new StereotypedItem[] { this.getModelLayer(),
				this.getControlLayer(), this.getDataLayer(),
				this.getProjectConfig(), this.getDiagramLayer(), this.getPluginLayer() });
	}


	public DiagramLayer getDiagramLayer() {
		if (diagramLayer == null) {
			diagramLayer = new DiagramLayer(this);
			diagramLayer.setParent(this);
		}
		return diagramLayer;
	}


	public void setDiagramLayer(DiagramLayer diagramLayer) {
		diagramLayer.setParent(this);
		this.diagramLayer = diagramLayer;
	}

	public PluginLayer getPluginLayer() {
		if (pluginLayer == null) {
			pluginLayer = new PluginLayer(this);
			pluginLayer.setParent(this);
		}
		return pluginLayer;
	}


	public void setPluginLayer(PluginLayer pluginLayer) {
		pluginLayer.setParent(this);
		this.pluginLayer = pluginLayer;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof ModelLayer) {
			this.setModelLayer((ModelLayer) child);
			return true;
		}
		if (child instanceof ControlLayer) {
			this.setControlLayer((ControlLayer) child);
			return true;
		}
		if (child instanceof DataLayer) {
			this.setDataLayer((DataLayer) child);
			return true;
		}
		if (child instanceof DiagramLayer) {
			this.setDiagramLayer((DiagramLayer) child);
			return true;
		}
		if (child instanceof ProjectConfig) {
			this.setProjectConfig((ProjectConfig) child);
			return true;
		}
		if (child instanceof PluginLayer) {
			this.setPluginLayer((PluginLayer) child);
			return true;
		}
		return false;
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof ModelLayer) {
			this.setModelLayer(null);
			return true;
		}
		if (child instanceof ControlLayer) {
			this.setControlLayer(null);
			return true;
		}
		if (child instanceof DataLayer) {
			this.setDataLayer(null);
			return true;
		}
		if (child instanceof DiagramLayer) {
			this.setDiagramLayer(null);
			return true;
		}
		if (child instanceof ProjectConfig) {
			this.setProjectConfig(null);
			return true;
		}
		if (child instanceof PluginLayer) {
			this.setPluginLayer(null);
			return true;
		}
		return false;
	}

}
