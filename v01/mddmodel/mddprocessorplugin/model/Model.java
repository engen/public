/*
 * Model.java
 *
 * Created on September 29, 2004, 9:37 AM
 */

package mddprocessorplugin.model;

import java.beans.*;
import java.io.*;

/**
 *
 * @author  Carlos Eugenio P. da Purificacao
 */
public interface Model extends Serializable {
    
    public static final String MODEL_LEAF = "leaf";
    
    public Model getParent();
    
    public void addModelListener(ModelListener listener);
    
    public void removeModelListener(ModelListener listener);
    
    public Model[] getChildren();
    
    public void addChildren(Model model);
    
    public void addChildren(Model[] children);
    
    public void setParent(Model model);
    
    public void addPropertyChangeListener(PropertyChangeListener listener);
    
    public void removePropertyChangeListener(PropertyChangeListener listener);
    
    public PropertyChangeSupport getPropertyChangeSupport();
    
    public void setProperty(String propertyName, Object propertyValue) throws ModelException;
    
    public Object getProperty(String propertyName) throws ModelException;
    
    public boolean hasProperty(String propertyName);
    
}
