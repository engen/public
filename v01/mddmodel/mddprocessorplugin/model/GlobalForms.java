package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

public class GlobalForms extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Form> forms = new ArrayList<Form>();
	
	public GlobalForms() {
		super("globalforms");
		this.setName("Global Forms");
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		if (this.forms == null) {
			this.forms = new ArrayList<Form>();
		}
		return this.forms;
	}

	public List<Form> getForms() {
		return forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}
	
	public void addForm(Form form) {
		form.setParent(this);
		this.getForms().add(form);
		this.notifyModelChangeListeners();
	}
	
	public void removeForm(Form form) {
		this.getForms().remove(form);
		this.notifyModelChangeListeners();
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Form) {
			this.addForm((Form) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Form) {
			this.addForm((Form) child);
			return true;
		}
		return false;
	}
	
}
