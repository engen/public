/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/


package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.views.MddProcessorFixedProjectTreeViewHandler;

/**
 * Groups all diagrams in the model.
 * @author Carlos Eugenio P. da Purificacao
 * Created on Aug 4, 2009
 */
public class DiagramLayer extends BaseModel {

	/**
	 * Generated serial version id
	 */
	private static final long serialVersionUID = -3367046036778406089L;
	private List<BaseDiagram> diagrams = new ArrayList<BaseDiagram>();
	
	public DiagramLayer() {
		this.setStereotypeForThis();
	}
	
	/**
	 * @param project
	 */
	public DiagramLayer(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
	}

	private void setStereotypeForThis() {
		this.setStereotypeName("diagramlayer");
	}
	
	/**
	 * @param diagrams the diagrams to set
	 */
	public void setDiagrams(List<BaseDiagram> diagrams) {
		for(BaseDiagram diagram : diagrams) {
			diagram.setParent(this);
		}
		this.diagrams = diagrams;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the diagrams
	 */
	public List<BaseDiagram> getDiagrams() {
		if (this.diagrams == null) {
			diagrams = new ArrayList<BaseDiagram>();
		}
		return diagrams;
	}

	public void addDiagram(BaseDiagram diagram) {
		diagram.setParent(this);
		this.getDiagrams().add(diagram);
		this.notifyModelChangeListeners();
	}
	
	public void removeDiagram(BaseDiagram diagram) {
		this.getDiagrams().remove(diagram);
		this.notifyModelChangeListeners();
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		return diagrams;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof BaseDiagram) {
			this.addDiagram((BaseDiagram) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof BaseDiagram) {
			this.removeDiagram((BaseDiagram) child);
			return true;
		}
		return false;
	}
	
}
