package mddprocessorplugin.model;

/**
 * Attributes may have initializers which will initialize
 * the attribute of target entity to some value 
 */
public enum InitializerType {

	counter, currentDate, currentTime;
	
	/**
	 * Helper method to get the code templates should use when
	 * rendering the initializer
	 */
	public String getTemplateCode(String entityName, String fieldName) {
		if (this.equals(currentDate)) {
			return "DateUtil.getFormattedJavaUtilDate(new java.util.Date())";
		} else if (this.equals(currentTime)) {
			return "DateUtil.getFormattedJavaUtilTime(new java.util.Date())";
		} else {
			return "CounterUtil.getNext(\"" + entityName + "\", \"" + fieldName + "\")";
		}
	}

	public boolean isDate() {
		return this.equals(currentDate) | this.equals(currentTime);
	}

	public boolean isCounter() {
		return this.equals(counter);
	}
}
