package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.model.layouts.LayoutStyle;
import mddprocessorplugin.model.util.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ControlLayer extends BaseModel {

	private transient Logger log = LoggerFactory.getLogger(ControlLayer.class);
	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 1026639592753844200L;
	private String strutsPackage;
	/**
	 * If set will cause actions that generate controllers to produce a
	 * controller for each name. Names should be separated by commas.
	 */
	private String defaultControllers = Defaults.DEFAULT_CONTROL_CONTROLLERS;
	/**
	 * Array of strings representing the pages the controller will generate.
	 * Generally this will be in the form "Search,List,Create,Update"
	 */
	private String[] defaultControllersArray = new String[0];
	/**
	 * The input page prefix. If it is "Before" the input page for all action
	 * will be BeforeSearch, BeforeList, etc.
	 */
	private String inputPagePrefix = Defaults.INPUT_PAGE_PREFIX;
	/**
	 * The modules this application has. Modules logically group actions or
	 * controllers as they can represent separate pieces of application.
	 */
	private List<ControlModule> controlModules = new ArrayList<ControlModule>();
	/**
	 * The layouts this application uses. A default layout is created when the
	 * corresponding action is called.
	 */
	private List<Layout> layouts = new ArrayList<Layout>();
	/**
	 * The default prefix for control modules. It will generate module names
	 * such as Module[EntityName] if a module name is not defined for an entity.
	 */
	private String defaultModulePrefix = Defaults.DEFAULT_CONTROL_MODULE_PREFIX;

	/*
	 * The max size of a text box in the view. If it pass this limit it will be
	 * rendered as a text area.
	 */
	private String maxTextBoxSize = Defaults.MAX_TEXT_BOX_SIZE;

	private GlobalForms globalForms = new GlobalForms();

	private String defaultTabFieldCountLimit = Defaults.DEFAULT_TAB_FIELD_COUNT_LIMIT;

	public ControlLayer() {
		this.setStereotypeForThis();
		this.initCollections();
		this.globalForms.setParent(this);
	}

	public ControlLayer(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
	}

	private void setStereotypeForThis() {
		this.setStereotypeName("controlconfig");
	}

	/**
	 * @param strutsPackage
	 *            the strutsPackage to set
	 */
	public void setStrutsPackage(String strutsPackage) {
		this.strutsPackage = strutsPackage;
	}

	/**
	 * @return the strutsPackage
	 */
	public String getStrutsPackage() {
		return strutsPackage;
	}

	/**
	 * @param controlModules
	 *            the controlModules to set
	 */
	public void setControlModules(List<ControlModule> controlModules) {
		for (ControlModule cm : controlModules) {
			cm.setParent(this);
		}
		this.controlModules = controlModules;
		this.notifyModelChangeListeners();
	}

	public void addControlModule(ControlModule controlModule) {
		controlModule.setParent(this);
		this.getControlModules().add(controlModule);
		this.notifyModelChangeListeners();
	}

	public void removeControlModule(ControlModule controlModule) {
		controlModule.setParent(null);
		this.getControlModules().remove(controlModule);
		this.notifyModelChangeListeners();
	}

	public ControlModule getControlModule(String name) {
		if (StringUtils.isNullOrEmpty(name)) {
			return null;
		}
		for (ControlModule cm : this.getControlModules()) {
			if (name.equals(cm.getName())) {
				return cm;
			}
		}
		return null;
	}

	/**
	 * @return the controlModules
	 */
	public List<ControlModule> getControlModules() {
		if (this.controlModules == null) {
			controlModules = new ArrayList<ControlModule>();
		}
		return controlModules;
	}

	/**
	 * @param layouts
	 *            the layouts to set
	 */
	public void setLayouts(List<Layout> layouts) {
		for (Layout l : layouts) {
			l.setParent(this);
		}
		this.layouts = layouts;
		this.notifyModelChangeListeners();
	}

	public void addLayout(Layout layout) {
		layout.setParent(this);
		this.getLayouts().add(layout);
		this.notifyModelChangeListeners();
	}

	public void removeLayout(Layout layout) {
		layout.setParent(this);
		this.getLayouts().remove(layout);
		this.notifyModelChangeListeners();
	}

	/**
	 * Returns the layout for the layout name. This method must return a valid
	 * layout as now is mandatory every project have at least one layout
	 * defined.
	 * 
	 * @param name
	 *            the layout name as set in the project configuration
	 * @return the layout matching the name provided
	 */
	public Layout getLayout(String name) {
		if (StringUtils.isEmpty(name)) {
			getLog().warn("The provided layout name is empty!");
			return null;
		}
		getLog().debug("Current layouts: " + this.getLayouts());
		for (Layout ly : this.getLayouts()) {
			if (name.equals(ly.getName())) {
				return ly;
			}
		}
		return null;
	}

	private Logger getLog() {
		if (log == null) {
			log = LoggerFactory.getLogger(ControlLayer.class);
		}
		return log;
	}

	/**
	 * Returns the current {@link Layout} based on the Project Configuration
	 * CurrentLayoutName property
	 * 
	 * @return the current layout
	 */
	public Layout getCurrentLayout() {
		String projectCurrentLayoutName = getEnclosingProject()
				.getProjectConfig().getCurrentLayoutName();
		Layout current = getLayout(projectCurrentLayoutName);
		if (current == null) {
			throw new IllegalStateException("The layout ["
					+ projectCurrentLayoutName
					+ "] was not found on this project!");
		}
		return current;
	}

	/**
	 * Returns the current {@link LayoutStyle} based on the current layout.
	 * 
	 * @return the current {@link LayoutStyle}
	 */
	public LayoutStyle getCurrentLayoutStyle() {
		Layout layout = this.getCurrentLayout();
		if (layout == null) {
			return null;
		}
		return layout.getLayoutStyle();
	}

	/**
	 * @return the layouts
	 */
	public List<Layout> getLayouts() {
		if (this.layouts == null) {
			layouts = new ArrayList<Layout>();
		}
		return layouts;
	}

	/**
	 * @param defaultControllers
	 *            the defaultControllers to set
	 */
	public void setDefaultControllers(String defaultControllers) {
		this.defaultControllers = defaultControllers;
	}

	/**
	 * @return the defaultControllers
	 */
	public String getDefaultControllers() {
		return defaultControllers;
	}

	/**
	 * @param inputPagePrefix
	 *            the inputPagePrefix to set
	 */
	public void setInputPagePrefix(String inputPagePrefix) {
		this.inputPagePrefix = inputPagePrefix;
	}

	/**
	 * @return the inputPagePrefix
	 */
	public String getInputPagePrefix() {
		return inputPagePrefix;
	}

	@Override
	public List<StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		if (this.getGlobalForms() != null) {
			children.add(this.getGlobalForms());
		}
		if (this.getControlModules() != null) {
			children.addAll(this.getControlModules());
		}
		if (this.getLayouts() != null) {
			children.addAll(this.getLayouts());
		}
		return children;
	}

	public String[] getDefaultControllersArray() {
		if (!StringUtils.isNullOrEmpty(this.getDefaultControllers())) {
			this.defaultControllersArray = this.getDefaultControllers().split(
					",");
			String[] temp = new String[this.defaultControllersArray.length];
			int i = 0;
			for (String str : this.defaultControllersArray) {
				String result = str.trim();
				temp[i++] = result;
			}
			this.defaultControllersArray = temp;
			return this.defaultControllersArray;
		} else {
			return new String[0];
		}
	}

	/**
	 * @param defaultModulePrefix
	 *            the defaultModulePrefix to set
	 */
	public void setDefaultModulePrefix(String defaultModulePrefix) {
		this.defaultModulePrefix = defaultModulePrefix;
	}

	/**
	 * @return the defaultModulePrefix
	 */
	public String getDefaultModulePrefix() {
		return defaultModulePrefix;
	}

	/**
	 * @param maxTextBoxSize
	 *            the maxTextBoxSize to set
	 */
	public void setMaxTextBoxSize(String maxTextBoxSize) {
		this.maxTextBoxSize = maxTextBoxSize;
	}

	/**
	 * @return the maxTextBoxSize
	 */
	public String getMaxTextBoxSize() {
		return maxTextBoxSize;
	}

	public void setDefaultTabFieldCountLimit(String defaultTabFieldCountLimit) {
		this.defaultTabFieldCountLimit = defaultTabFieldCountLimit;
	}

	public String getDefaultTabFieldCountLimit() {
		return defaultTabFieldCountLimit;
	}

	/**
	 * @param globalForms
	 *            the globalForms to set
	 */
	public void setGlobalForms(GlobalForms globalForms) {
		globalForms.setParent(this);
		this.globalForms = globalForms;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the globalForms
	 */
	public GlobalForms getGlobalForms() {
		if (globalForms == null) {
			globalForms = new GlobalForms();
			globalForms.setParent(this);
		}
		return globalForms;
	}

	/**
	 * Return the first form with the provided name in this ControlLayer
	 */
	public Form getForm(String formName) {
		if (getLog() == null) {
			log = LoggerFactory.getLogger(ControlLayer.class);
		}
		// log.info("Getting form with name: " + formName);
		for (Form form : this.getGlobalForms().getForms()) {
			if (form.getName().equals(formName)) {
				return form;
			}
		}
		for (StereotypedItem child : this.getChildren()) {
			// log.info("Checking child: " + child.getName() + ", class: "
			// + child.getClass());
			if (child instanceof ControlModule) {
				ControlModule module = (ControlModule) child;
				for (Controller c : module.getControllers()) {
					if (c.getForm() != null
							&& c.getForm().getName().equals(formName)) {
						return c.getForm();
					}
				}
			}
		}
		return null;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof ControlModule) {
			this.addControlModule((ControlModule) child);
			return true;
		}
		if (child instanceof Layout) {
			this.addLayout((Layout) child);
			return true;
		}
		if (child instanceof GlobalForms) {
			this.setGlobalForms((GlobalForms) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof ControlModule) {
			this.removeControlModule((ControlModule) child);
			return true;
		}
		if (child instanceof Layout) {
			this.removeLayout((Layout) child);
			return true;
		}
		if (child instanceof GlobalForms) {
			this.setGlobalForms(null);
			return true;
		}
		return false;
	}

	/**
	 * Returns the first controller found with the given name.
	 * 
	 * @param controllerName
	 *            the controller name to look for
	 * @return the controller with the given name if found or null
	 */
	public Controller getControllerByName(String controllerName) {
		for (ControlModule module : this.controlModules) {
			Controller controller = module.getControllerByName(controllerName);
			if (controller != null) {
				return controller;
			}
		}
		return null;
	}

	public Controller getControllerForEntityAndPageType(Entity entity,
			String inputPageType) {
		for (ControlModule module : this.controlModules) {
			for (Controller controller : module.getControllers()) {
				if (controller != null
						&& controller.getEntity().equals(entity)
						&& controller.getInputPageType().toLowerCase()
								.equals(inputPageType.toLowerCase())) {
					return controller;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the first view found with the given name.
	 * 
	 * @param viewNaPresentationView
	 *            : me the view name to look for
	 * @return the view with the given name if found or null
	 */
	public PresentationView getViewByName(String viewName) {
		for (ControlModule module : this.controlModules) {
			PresentationView view = module.getViewByName(viewName);
			if (view != null) {
				return view;
			}
		}
		return null;
	}

	/**
	 * Returns the first section in the controller layer with the provided name.
	 * PresentationView:
	 * 
	 * @param sectionName
	 * @return
	 */
	public ViewSection getSectionByName(String sectionName) {
		for (ControlModule module : this.controlModules) {
			for (PresentationView view : module.getViews()) {
				for (ViewSection section : view.getSubSections()) {
					if (section.getName().equals(sectionName)) {
						return section;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Returns the first target (View or Controller) found with the given name.
	 * 
	 * @param targetName
	 *            the target name to look for
	 * @return the target with the given name if found or null
	 */
	public ModelTarget getModelTargetByName(String targetName) {
		for (ControlModule module : this.controlModules) {
			ModelTarget target = module.getModelTargetByName(targetName);
			if (target != null) {
				return target;
			}
		}
		return null;
	}

	public void clear() {
		this.initCollections();
		this.notifyModelChangeListeners();
	}

	private void initCollections() {
		this.controlModules = new ArrayList<ControlModule>();
		this.layouts = new ArrayList<Layout>();
		this.globalForms = new GlobalForms();
		this.globalForms.setParent(this);
	}

}
