package mddprocessorplugin.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This enumeration provides the possible configurations for the field in the
 * view. This defines how a field will be rendered by templates
 * 
 * @author eugenio
 * 
 */
public enum FieldType {

	ANY, LABEL, IMAGE, TEXT, TEXTAREA, SELECT, CHECK, RADIO, BUTTON, 
	CANCEL, TABLE, LIST, DATE, COUNTER, TIME, RELATION_ATTRIBUTE_TABLE, 
	NUMBER, HIDDEN, RESET;

	public String toString() {
		String ret = super.toString();
		return ret.toLowerCase();
	}

	public static FieldType get(String value) {
		try {
			return FieldType.valueOf(value.toUpperCase());
		} catch (Exception ex) {
			Logger log = LoggerFactory.getLogger(FieldType.class);
			log.warn("There is NO FieldType defined for [" + value
					+ "]. Returning ANY...");
			//throw new RuntimeException("No field type for value [" + value + "]");
			return ANY;
		}
	}
}
