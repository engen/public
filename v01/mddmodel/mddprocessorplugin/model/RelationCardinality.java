/**
 * 
 */
package mddprocessorplugin.model;

import mddprocessorplugin.model.util.StringUtils;


/**
 * Enumerates all the possible relation cardinalities for the model
 * 
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da
 *         Purificacao</a>
 * @created Jun 24, 2009
 * @version 1.0
 */
public enum RelationCardinality {

	MANY_TO_ONE, ONE_TO_ONE, ONE_TO_MANY;

	private static String STRING_MANY_TO_ONE = "many-to-one";
	private static String STRING_ONE_TO_ONE = "one-to-one";
	private static String STRING_ONE_TO_MANY = "one-to-many";

	public String toString() {
		switch (this) {
		case MANY_TO_ONE:
			return STRING_MANY_TO_ONE;
		case ONE_TO_MANY:
			return STRING_ONE_TO_MANY;
		default:
			return STRING_ONE_TO_ONE;
		}
	}
	
	public boolean equals(String other) {
		if (other == null) {
			return false;
		}
		String thisStringRepresentation = this.toString();
		String otherStringRepresentation = other.toString();
		return StringUtils.equals(thisStringRepresentation, otherStringRepresentation);
	}
}
