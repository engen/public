package mddprocessorplugin.model;


/**
 * Validator for a column.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eug�nio P. da Purifica��o</a>
 * @created 01/03/2009
 * @version 1.0
 */
public class ColumnValidator extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5576555505968176593L;

	public ColumnValidator() {
		super("columnvalidator");
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}
}
