/**
 * 
 */
package mddprocessorplugin.model;

/**
 * A template owner knows its own template path. So specific templates
 * can be attached to them.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da Purificacao</a>
 * @created 09/03/2009
 * @version 
 */
public interface TemplateOwner {

	/**
	 * Gets the path for the template.
	 * @return the path for the template.
	 */
	public String getTemplatePath();
	
	/**
	 * Sets the path for the template.
	 * @param path the new path for the template.
	 */
	public void setTemplatePath(String path);
}
