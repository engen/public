package mddprocessorplugin.model;

public interface Translatable {

	public String internalValue();
	
}
