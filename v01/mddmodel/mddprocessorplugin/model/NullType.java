/*
 * NullType.java
 *
 * Created on May 26, 2006, 4:03 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package mddprocessorplugin.model;

import java.io.Serializable;

/**
 *
 * @author Carlos Eugenio P. da Purificacao
 */
public abstract class NullType implements Serializable {
    private static final String NULL_TYPE_STRING = "";
    private static final String EMPTY_TYPE_STRING = "empty";
    static ThreadLocal<String> nullTypeString = new ThreadLocal<String>() {
        protected synchronized String initialValue() {
            return NULL_TYPE_STRING;
        }
    };
    static ThreadLocal<String> emptyTypeString = new ThreadLocal<String>() {
        protected synchronized String initialValue() {
            return EMPTY_TYPE_STRING;
        }
    };
    
}

