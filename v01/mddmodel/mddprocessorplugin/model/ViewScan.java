/**
 * 
 */
package mddprocessorplugin.model;

/**
 * @author eugenio
 *
 */
public class ViewScan extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BaseModel target;
	private String varName;
	private String targetType;
	
	public ViewScan() {
		super("viewscan");
	}
	
	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}

	public BaseModel getTarget() {
		return target;
	}

	public void setTarget(BaseModel target) {
		this.target = target;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

}
