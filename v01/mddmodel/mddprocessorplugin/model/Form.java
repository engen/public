package mddprocessorplugin.model;

import mddprocessorplugin.model.forms.FormConfigurationStrategy;
import mddprocessorplugin.model.util.StringUtils;

public class Form extends ViewSection implements ViewDefinition {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7727460284253609866L;

	/**
	 * The base form for this form.
	 */
	private String baseForm;

	private String scope = Defaults.DEFAULT_FORM_SCOPE;

	/**
	 * Indicates de form style class. Defaults to "yform"
	 */
	private String styleClass = Defaults.DEFAULT_FORM_STYLE_CLASS;
	/**
	 * The fieldset style class. Defaults to "columnar"
	 */
	private String fieldSetStyleClass = Defaults.DEFAULT_FORM_FIELDSET_STYLE_CLASS;

	/**
	 * The main fieldset id. Defaults to "page_field_set"
	 */
	private String fieldSetId = Defaults.DEFAULT_FORM_FIELDSET_ID;

	public Form() {
		super("form");
	}
	
	public static String getFormName(Entity entity) {
		return StringUtils.capitalize(entity.getName()) + "Form";
	}

	public static String getFormName(StereotypedItem parent) {
		return StringUtils.capitalize(parent.getName());
	}

	private Form(Entity e, ControlLayer parent) {
		this();
		this.setParent(parent);
		this.setName(Form.getFormName(e));
		for (Attribute att : e.getAllAttributes()) {
			// Add a input field for the base form.
			this.addFieldForAttribute(att, false);
		}
	}

	public Form(Controller parent, Form base, Entity e) {
		this();
		this.setParent(parent);
		// Update form information based on controller
		updateFormWithParentAndEntity(parent, base, e);
	}

	public Form(ViewSection parent) {
		this();
		setParent(parent);
	}

	public Form(ControlLayer parent, Entity e) {
		this(e, parent);
	}

	/**
	 * Updates form information. This method will try to find or create a global
	 * form for the entity. Also it will define the form name if it wasn't
	 * already defined. Also it will configure all validation
	 */
	public void updateForm(Entity entity, StereotypedItem holder,
			ControlLayer controlLayer) {
		// Try to find a global form created for this entity.
		Form entityForm = controlLayer.getForm(Form.getFormName(entity));
		// For now, if the form already exists, remove.
		if (entityForm != null) {
			controlLayer.getGlobalForms().getForms().remove(entityForm);
			entityForm = null;
		}
		if (entityForm == null) {
			entityForm = createGlobalForm(entity, controlLayer);
		}
		// The entityForm will be the base form for this form
		updateFormWithParentAndEntity(holder, entityForm, entity);
		// Configure the form
		FormConfigurationStrategy st = new FormConfigurationStrategy();
		st.configure(entityForm);
	}

	private Form createGlobalForm(Entity e, ControlLayer controlLayer) {
		Form entityForm;
		// If a form hasn't be created. Create a new one
		// filled with all entity's attributes.
		entityForm = new Form(controlLayer, e);
		controlLayer.getGlobalForms().addForm(entityForm);
		return entityForm;
	}

	/**
	 * Updates the form information based on an existing global entity form.
	 */
	private void updateFormWithParentAndEntity(StereotypedItem parent, Form base,
			Entity entity) {
		if (StringUtils.isEmpty(this.getName())) {
			// The form name is the controller name plus 'Form'
			this.setName(Form.getFormName(parent));
		}
		log.trace("Setting base form for form [" + this.getName() + "] : base ["
				+ base + "]");
		this.setBaseForm(base.getName());
		// The attributes to be added to the form are all entity
		// attributes, including its super class attributes
		for (Attribute att : entity.getAllAttributes()) {
			this.addFieldForAttribute(att, true);
		}
	}

	private void addFieldForAttribute(Attribute att, boolean existisOnBaseForm) {
		InputField field = this.createFieldForAttribute(att, existisOnBaseForm);
		// Fields are not overwritten
		if (!this.hasField(field.getName())) {
			this.addField(field);
		}
	}

	private InputField createFieldForAttribute(Attribute att,
			boolean existisOnBaseForm) {
		InputField field = new InputField(this, att);
		field.setExistisOnBaseForm(existisOnBaseForm);
		return field;
	}

	/**
	 * @param baseForm
	 *            the baseForm to set
	 */
	public void setBaseForm(String baseForm) {
		this.baseForm = baseForm;
	}

	/**
	 * @return the baseForm
	 */
	public String getBaseForm() {
		return baseForm;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public String getStyleClass() {
		return this.styleClass;
	}

	@Override
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public void setFieldSetStyleClass(String fieldSetStyleClass) {
		this.fieldSetStyleClass = fieldSetStyleClass;
	}

	public String getFieldSetStyleClass() {
		return fieldSetStyleClass;
	}

	public void setFieldSetId(String fieldSetId) {
		this.fieldSetId = fieldSetId;
	}

	public String getFieldSetId() {
		return fieldSetId;
	}

}
