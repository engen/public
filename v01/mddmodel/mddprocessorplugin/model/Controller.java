package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.model.util.StringUtils;

/**
 * Represents a controller inside a ControlModule
 * 
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da
 *         Purificacao</a>
 * @created 08/03/2009
 * @version
 */
public class Controller extends BaseModel implements ModelTarget,
		TemplateOwner {

	private static final String CONTROLLER = "controller";

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 7240749798045893602L;

	/**
	 * The namespace for the controller.
	 */
	private String namespace = StringUtils.EMPTY;
	private String path = StringUtils.EMPTY;
	private String actionName = StringUtils.EMPTY;
	private String inputPage = StringUtils.EMPTY;
	private String entityName = StringUtils.EMPTY;
	/**
	 * This will guide on the template selection for JSP input pages for this
	 * controller.
	 **/
	private String inputPageType = StringUtils.EMPTY;
	/** Paths to view entities on the model **/
	private List<LogicalPath> logicalPaths = new ArrayList<LogicalPath>();
	private String transactionScope = Defaults.DEFAULT_TRANSACTION_SCOPE;
	private Boolean showOnMenu = Defaults.CONTROLLER_SHOW_ON_MENU;
	private String menuLabel = StringUtils.EMPTY;
	/* If true will generate set property configurations on action mappings */
	private Boolean useInlineConfiguration = Defaults.CONTROLLER_USE_IN_LINE_CONFIGURATION;
	private Boolean validate = Defaults.CONTROLLER_VALIDATE;
	/**
	 * The from for this controller
	 */
	private Form form;
	/**
	 * If this controller is associated with an entity this attribute will be
	 * set.
	 */
	private Entity entity;

	private boolean fromDsl = false;

	private String superClass;

	private String typeParameter = StringUtils.EMPTY;

	private List<ControllerCriteria> criterias = new ArrayList<ControllerCriteria>();

	private List<Assignment> bodyStatements = new ArrayList<Assignment>();

	/**
	 * This will be used by templates to indicate the target name variable for
	 * the controller. The target name variable can be used to store the
	 * controller result for later user for the view.
	 */
	private String targetName;

	public Controller() {
		super(CONTROLLER);
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}

	/**
	 * @param actionName
	 *            the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	/**
	 * @return the inputPage
	 */
	public String getInputPage() {
		return inputPage;
	}

	/**
	 * @param inputPage
	 *            the inputPage to set
	 */
	public void setInputPage(String inputPage) {
		this.inputPage = inputPage;
	}

	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setLogicalPaths(List<LogicalPath> pages) {
		for (LogicalPath p : pages) {
			p.setParent(this);
		}
		this.logicalPaths = pages;
		this.notifyModelChangeListeners();
	}

	public void addLogicalPath(LogicalPath page) {
		page.setParent(this);
		this.getLogicalPaths().add(page);
		this.notifyModelChangeListeners();
	}

	public void removeLogicalPath(LogicalPath page) {
		page.setParent(this);
		this.getLogicalPaths().remove(page);
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the pages
	 */
	public List<LogicalPath> getLogicalPaths() {
		if (this.logicalPaths == null) {
			this.logicalPaths = new ArrayList<LogicalPath>();
		}
		return logicalPaths;
	}

	@Override
	public List<StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		if (this.getForm() != null) {
			children.add(this.getForm());
		}
		children.addAll(this.getLogicalPaths());
		return children;
	}

	/**
	 * @param entityName
	 *            the entity name to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the entity name
	 */
	public String getEntityName() {

		return entityName;
	}

	/**
	 * @param inputPageType
	 *            the inputPageType to set
	 */
	public void setInputPageType(String inputPageType) {
		this.inputPageType = inputPageType;
	}

	/**
	 * @return the inputPageType
	 */
	public String getInputPageType() {
		return inputPageType;
	}

	/**
	 * @param transactionScope
	 *            the transactionScope to set
	 */
	public void setTransactionScope(String transactionScope) {
		this.transactionScope = transactionScope;
	}

	/**
	 * @return the transactionScope
	 */
	public String getTransactionScope() {
		return transactionScope;
	}

	/**
	 * @param showOnMenu
	 *            the showOnMenu to set
	 */
	public void setShowOnMenu(Boolean showOnMenu) {
		this.showOnMenu = showOnMenu;
	}

	/**
	 * @return the showOnMenu
	 */
	public Boolean getShowOnMenu() {
		return showOnMenu;
	}

	/**
	 * @param useInlineConfiguration
	 *            the useInlineConfiguration to set
	 */
	public void setUseInlineConfiguration(Boolean useInlineConfiguration) {
		this.useInlineConfiguration = useInlineConfiguration;
	}

	/**
	 * @return the useInlineConfiguration
	 */
	public Boolean getUseInlineConfiguration() {
		return useInlineConfiguration;
	}

	/**
	 * @param validate
	 *            the validate to set
	 */
	public void setValidate(Boolean validate) {
		this.validate = validate;
	}

	/**
	 * @return the validate
	 */
	public Boolean getValidate() {
		return validate;
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public void setForm(Form form) {
		if (form != null) {
			form.setParent(this);
		}
		this.form = form;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the form
	 */
	public Form getForm() {
		return form;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof LogicalPath) {
			this.addLogicalPath((LogicalPath) child);
			return true;
		}
		if (child instanceof Form) {
			this.setForm((Form) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof LogicalPath) {
			this.removeLogicalPath((LogicalPath) child);
			return true;
		}
		if (child instanceof Form) {
			this.setForm(null);
			return true;
		}
		return false;
	}

	/**
	 * @param e
	 */
	public void setEntity(Entity e) {
		this.entity = e;
	}

	/**
	 * Returns the entity this controller is associated to, if any.
	 * 
	 * @return the entity this controller is associated or null
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * Returns the control module this element is child of
	 * 
	 * @return the control module this element is child of
	 */
	public ControlModule getControlModule() {
		if (this.getParent() != null) {
			return (ControlModule) this.getParent();
		}
		return null;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setFromDsl(boolean fromDsl) {
		this.fromDsl = fromDsl;
	}

	public boolean isFromDsl() {
		return fromDsl;
	}

	public void setMenuLabel(String menuLabel) {
		this.menuLabel = menuLabel;
	}

	public String getMenuLabel() {
		return menuLabel;
	}

	public void setSuperClass(String value) {
		this.superClass = value;
	}

	public String getSuperClass() {
		return superClass;
	}

	public String getTypeParameter() {
		return typeParameter;
	}

	public void setTypeParameter(String typeParameter) {
		this.typeParameter = typeParameter;
	}

	public void addCriteria(String comparator, String propertyName,
			String restrictionValue, String parameter) {
		ControllerCriteria criteria = new ControllerCriteria(comparator,
				propertyName, restrictionValue, parameter);
		criterias.add(criteria);
	}

	public List<ControllerCriteria> getCriterias() {
		return criterias;
	}

	public void setCriterias(List<ControllerCriteria> criterias) {
		this.criterias = criterias;
	}

	public void addBodyStatement(Assignment assignment) {
		this.bodyStatements.add(assignment);

	}

	public List<Assignment> getBodyStatements() {
		return bodyStatements;
	}

	public void setBodyStatements(List<Assignment> bodyStatements) {
		this.bodyStatements = bodyStatements;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

}
