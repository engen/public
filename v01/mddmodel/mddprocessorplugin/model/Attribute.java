package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.db.Sql2Java;
import mddprocessorplugin.model.util.AttributeUtil;
import mddprocessorplugin.model.util.StringUtils;

import org.eclipse.jdt.core.JavaModelException;

/**
 * An attribute for a model element. Attributes can also represent an
 * association between to model elements as it implements ModelAssociation
 * interface.
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 6, 2009
 */
public class Attribute extends BaseModel implements Cloneable, ModelAssociation, Comparable<Attribute> {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -5229470502801422498L;
	/**
	 * Attributes may have initializers which will initialize
	 * the attribute of target entity to some value 
	 */
	private InitializerType initializerType;
	/** the description for this attribute **/
	private String description;
	/** If primitive, the type or the interface. **/
	private String type;
	/** type implementation for Lists or Sets for example **/
	private String typeImpl;
	/** if this attribute is a reference not a primitive type **/
	private boolean reference;
	/** if this attribute is not a primitive the interface it refers to **/
	private String referenceType;
	/** this is the implementation type (implementation of referenceType) **/
	private String referenceTypeImpl;
	/** this is the primary key of the related entity **/
	private String referenceTypePrimaryKey;
	/**
	 * the name of the attribute controlling the relation ship on the other side
	 **/
	private String backedRelationAttribute;
	/**
	 * the column name of referenced backedRelationAttribute.
	 */
	private String referencedColumnName;
	/**
	 * the name of the foreign key controlling the relation ship.
	 */
	private String foreignKeyColumn;
	/** identifies if this attribute is a collection. **/
	private boolean collection;
	/** the default value for this attribute. **/
	private String defaultValue;
	/** if this attribute is required in the enclosing entity **/
	private boolean required;
	/**
	 * label used to show this attribute in the view. It is also used to look up
	 * it in the application resource bundle.
	 **/
	private String label;
	/** if this attribute represents the primary key on the enclosed entity **/
	private boolean primaryKey;
	/** if this is a attribute represents a relationship with another entity. **/
	private boolean fromRelation;
	/** the cardinality for this attribute if it is a relation. **/
	private String cardinality;
	/** the position this attribute is show in plug-in or views **/
	private int ordinalPosition;
	/** the size of this field **/
	private String size;
	/**
	 * max number of characters to display in lists for this attribute. If not
	 * defined project level configuration will be used.
	 **/
	private String maxDisplaySize = Defaults.MAX_TEXT_BOX_SIZE;
	/** if created with a relation it should be stored. **/
	private Relation relation;
	/** if the attribute is a business key, meaning it can't be duplicated **/
	private Boolean businessKey = Boolean.FALSE;
	/** the validators for the attribute **/
	private List<AttributeValidator> validators = new ArrayList<AttributeValidator>();
	/**
	 * The attribute in the relation entity that will be used to show in the
	 * view in combo boxes or lists.
	 */
	private String relationAttributeLabel;
	/* The database column name for easy access */
	private String columnName;
	/* If the attribute is part of an index this is the index name */
	private String indexName;
	/* If this attribute should be shown on views */
	private Boolean showOnView = Boolean.TRUE;
	/* If this attribute is a foreign key for other table */
	private Boolean foreignKey = Boolean.FALSE;
	/** The access modifier for this attribute **/
	private AccessModifier accessModifier;
	/** the visibility is a shorthand to accessModifier.toString(). **/
	private String visibility;
	/**
	 * The disabled state for this attribute (if it can be edited). 
	 * This will be reflected on views.
	 */
	private Boolean disabled = Boolean.FALSE;
	/**
	 * Defines the model type as it was defined in the DSL
	 */
	private ModelType modelType;
	
	private boolean hashed;
	
	private boolean hidden;

	/**
	 * Defines the group this attribute is, inside the entity.
	 * Latelly this can define, for example, on each page tab to show the attribute.
	 */
	private String groupId;
	
	public Attribute() {
		this.setStereotypeName("attribute");
	}

	public Attribute(Entity parent, Column column) {
		this();
		this.setParent(parent);
		this.setName(this.getAttributeNameForColumn(column));
		// Sets the position for this attribute in views or plug-in.
		this.setOrdinalPosition(column.getOrdinalPosition());
		this.setCollection(Boolean.FALSE);
		this.setPrimaryKey(column.isPrimaryKey());
		this.setReference(Boolean.FALSE);
		this.setFromRelation(Boolean.FALSE);
		this.setRequired(column.isRequired());
		this.setSize(String.valueOf(column.getSize()));
		// TODO: Verify this setting for a reasonable setting for tables
		this.setMaxDisplaySize(this.getSize());
		this.setType(Sql2Java.getPreferredJavaTypeNoPrimitives(column
				.getTypeCode(), column.getSize(), column.getPrecisionRadix()));
		this.setVisibility(AccessModifier.PRIVATE.toString());
		this.setColumnName(column.getName());
		this.setCommonAttributesAfterNameHasBeenSet();
	}

	public void setCommonAttributesAfterNameHasBeenSet() {
		// The description for this attribute
		this.setDescription("Attribute " + this.getName());
		// Label to show in the view.
		String label = AttributeUtil.createLabelFor(StringUtils.capitalize(this.getName()));
		this.setLabel(label);
		/*
		 * the attribute, which the value will be looked up on the bean to be
		 * used to show in combo or lists
		 */
		this
				.setRelationAttributeLabel(Defaults.DEFAULT_RELATION_ATTRIBUTE_LABEL);
		// Show this attribute on views.
		this.setShowOnView(Boolean.TRUE);
		// This is altered later by the entity if necessary.
		this.setBusinessKey(Boolean.FALSE);
	}

	public Attribute(Entity parent, Relation relation) {
		this();
		if (parent == null) {
			throw new NullPointerException("The parent entity can't be null");
		}
		this.setParent(parent);
		this.setRelation(relation);
		// The role this attribute plays in the relation is the left role.
		// If the left role (this attribute's role) is left and the target
		// for this role is not many, this is a many-to-one relation, where
		// this attribute is a single reference to the other entity.
		// TODO: Rename these names after understood.
		// TODO: Why this is only stopping in debug when import database action
		// is
		// called?
		if (relation != null) {
			RelationshipRole leftRole = relation.getLeftRole();
			if (leftRole.isTargetMany()) {
				createOneToManyAttribute(leftRole);
			} else {
				createManyToOneAttribute(leftRole);
			}
		}
	}

	/**
	 * Creates an attribute that is a collection
	 * 
	 * @param refType
	 * @param rightRole
	 */
	public Attribute(Entity refType, RelationshipRole rightRole) {
		this();
		this.setParent(refType);
		// The role this attribute plays in the relation is the right role.
		createOneToManyAttribute(rightRole);
	}

	/**
	 * Creates an one-to-many attribute attribute.
	 * 
	 * @param role
	 */
	private void createOneToManyAttribute(RelationshipRole role) {
		if (this.getParent() == null) {
			throw new ModelException("Please, run database import before.");
		}
		ForeignKey fk = role.getTarget().getForeignKeyForRole(role);
		if (fk != null) {
			this.setForeignKeyColumn(fk.getFkColumn());
			String fkColumnCamelCase = StringUtils
					.toCamelCase(fk.getFkColumn());
			this.setReferencedColumnName(fk.getFkColumn());
			this.setBackedRelationAttribute(this
					.getAttributeNameForRelationShip(role.getTargetRole(),
							fkColumnCamelCase));
		}
		// The name of this attribute is the
		// target table foreign key java normalized name plus the target table
		// multiplicity (eg.: idFkTables)
		String fkNormalizedJavaName = StringUtils.toCamelCase(this
				.getForeignKeyColumn());
		String entityReferenceTypeName = StringUtils
				.toUppercaseFirstLetter(StringUtils.toCamelCase(role
						.getTarget().getName()));
		String pluralReferenceTypeName = StringUtils
				.toPlural(entityReferenceTypeName);
		this.setName(fkNormalizedJavaName + pluralReferenceTypeName);
		// there is no default value for references.
		// TODO: Check if this is true.
		this.setDefaultValue(StringUtils.EMPTY);
		// Sure this is from relation.
		this.setFromRelation(true);
		// For relations many-to-one always attributes are treated as
		// references.
		this.setReference(true);
		// This is a collection
		this.setCollection(true);
		// TODO: Use the project default type for collections.
		this.setType(Defaults.DEFAULT_COLLECTION_TYPE);
		// Type implementation for collections
		this.setTypeImpl(Defaults.DEFAULT_COLLECTION_TYPE_IMPL);
		// The other end of this relation.
		this.setReferenceType(entityReferenceTypeName);
		// The reference type implementation depends on project
		// configuration
		// In this case we consult the project to see if it has a suffix
		// configured
		// to it. If it has use it, otherwise use the default.TODO: verify
		// if this is
		// the correct behavior.
		boolean allEntitiesAreAbstract = false;// TODO: this is not working.
		// this.getEnclosingProject().getProjectConfig().getIsEntityAbstract();
		if (allEntitiesAreAbstract) {
			this.setReferenceTypeImpl(this.getReferenceType()
					+ this.getEnclosingProject().getProjectConfig()
							.getEntityImplementationSuffix());
		} else {
			// If not defined as abstract entities will have the same
			// reference type implementation
			// as their reference type.
			this.setReferenceTypeImpl(this.getReferenceType());
		}
		// Value to be shown in pages or other places where
		// a default value is required. By default the primary key.
		List<Column> primaryKeyColumns = role.getOrigin()
				.getPrimaryKeyColumns();
		if (primaryKeyColumns != null && primaryKeyColumns.size() > 0) {
			String primaryKeyColumnName = primaryKeyColumns.get(0).getName();
			String primaryKeyAttributeName = StringUtils
					.toCamelCase(primaryKeyColumnName);
			// The default value will be the first primary key column
			this.setDefaultValue(primaryKeyAttributeName);
			this.setReferenceTypePrimaryKey(primaryKeyAttributeName);
		}
		// For now always false. If this is really required it should be
		// treated as so in the business layer.
		// TODO: verify if this requirement still holds true.
		this.setRequired(false);
		this.setCardinality(RelationCardinality.ONE_TO_MANY);
		this.setCommonAttributesAfterNameHasBeenSet();
	}

	private void setCardinality(RelationCardinality one_to_many) {
		this.setCardinality(one_to_many.toString());
	}

	private void createManyToOneAttribute(RelationshipRole role) {
		// Single reference to the other entity.
		// Setting the foreign key that controls this relation ship.
		ForeignKey fk = role.getOrigin().getForeignKeyForRole(role);
		String fkColumnCamelCase = StringUtils.toCamelCase(fk.getFkColumn());
		// TODO: Rename these methods when understood.
		this.setBackedRelationAttribute(fkColumnCamelCase);
		// The foreign key column name.
		this.setForeignKeyColumn(fk.getFkColumn());
		// The primary key on the referenced table
		this.setReferencedColumnName(fk.getPkColumn());
		// The name of this attribute is the Foreign key in this table
		// referencing the id in the target table plus the target table
		// name in singular.
		this.setName(getAttributeNameForRelationShip(role, fkColumnCamelCase));
		// there is no default value for references. TODO: Check if this is
		// true.
		this.setDefaultValue(StringUtils.EMPTY);
		// This is the many-to-one side of the relation.
		this.setCollection(false);
		// Sure this is from relation.
		this.setFromRelation(true);
		// For relations many-to-one always attributes are treated as
		// references.
		this.setReference(true);
		// The type of this attribute is the type of his
		// mother file.
		String uType = this.getRelationShipReferenceTypeName(role);
		// Sets the type accordingly.
		// TODO: Update the type name space.
		this.setType(uType);
		// TODO: The reference type is the same for now but has to reflect the
		// correct type.
		this.setReferenceType(this.getType());
		// The reference type implementation depends on project
		// configuration
		// In this case we consult the project to see if it has a suffix
		// configured
		// to it. If it has use it, otherwise use the default.TODO: verify
		// if this is
		// the correct behavior.
		boolean allEntitiesAreAbstract = false;// TODO: should get from project.
		// this is not working.
		// this.getEnclosingProject().getProjectConfig().getIsEntityAbstract();
		if (allEntitiesAreAbstract) {
			this.setReferenceTypeImpl(this.getReferenceType()
					+ this.getEnclosingProject().getProjectConfig()
							.getEntityImplementationSuffix());
		} else {
			// If not defined as abstract entities will have the same
			// reference type implementation
			// as their reference type.
			this.setReferenceTypeImpl(this.getReferenceType());
		}
		// Value to be shown in pages or other places where
		// a default value is required. By default the primary key.
		List<Column> primaryKeyColumns = role.getOrigin()
				.getPrimaryKeyColumns();
		if (primaryKeyColumns != null && primaryKeyColumns.size() > 0) {
			String primaryKeyColumnName = primaryKeyColumns.get(0).getName();
			String primaryKeyAttributeName = StringUtils
					.toCamelCase(primaryKeyColumnName);
			// The default value will be the first primary key column
			this.setDefaultValue(primaryKeyAttributeName);
			this.setReferenceTypePrimaryKey(primaryKeyAttributeName);
		}
		String properAttributeName = StringUtils.toCamelCase(this
				.getBackedRelationAttribute());
		Attribute fkAttribute = ((Entity) this.getParent())
				.getAttribute(properAttributeName);
		if (fkAttribute != null) {
			this.setRequired(fkAttribute.isRequired());
		} else {
			// Set to required is false when the attribute is not found.
			this.setRequired(Boolean.FALSE);
		}
		// TODO: Make it a enumeration.
		this.setCardinality(RelationCardinality.MANY_TO_ONE);
		this.setCommonAttributesAfterNameHasBeenSet();
	}

	/**
	 * Returns a type name for the target table in the relationship.
	 * 
	 * @param role
	 *            the relationship to inquiry the target table
	 * @return the reference type name proper capitalized
	 */
	private String getRelationShipReferenceTypeName(RelationshipRole role) {
		String entityName = StringUtils.toCamelCase(role.getTarget().getName());
		String rightSide = StringUtils.capitalize(entityName);
		return rightSide;
	}

	/**
	 * @param role
	 * @param fkColumnCamelCase
	 * @return
	 */
	private String getAttributeNameForRelationShip(RelationshipRole role,
			String fkColumnCamelCase) {
		String leftSide = fkColumnCamelCase;
		return leftSide + getRelationShipReferenceTypeName(role);
	}

	/**
	 * Returns the attribute name based on a column name. It will transform the
	 * attribute to camel case.
	 * 
	 * @param column
	 *            the column to be used as a source for the name
	 * @return the camel case name for the attribute.
	 */
	private String getAttributeNameForColumn(Column column) {
		return StringUtils.toCamelCase(column.getName());
	}

	@Override
	public List<AttributeValidator> getChildren() {
		if (this.getValidators() == null) {
			this.validators = new ArrayList<AttributeValidator>();
		}
		return this.getValidators();
	}

	/**
	 * @return the validators
	 */
	public List<AttributeValidator> getValidators() {
		return validators;
	}

	/**
	 * @param validators
	 *            the validators to set
	 */
	public void setValidators(List<AttributeValidator> validators) {
		for (AttributeValidator validator : validators) {
			validator.setParent(this);
		}
		this.validators = validators;
		this.notifyModelChangeListeners();
	}

	public void addValidator(AttributeValidator validator) {
		validator.setParent(this);
		this.getValidators().add(validator);
		this.notifyModelChangeListeners();
	}

	public void removeValidator(AttributeValidator validator) {
		this.getValidators().remove(validator);
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the reference
	 */
	public boolean isReference() {
		return reference;
	}

	/**
	 * @param reference
	 *            the reference to set
	 */
	public void setReference(boolean reference) {
		this.reference = reference;
	}

	/**
	 * @return the referenceType
	 */
	public String getReferenceType() {
		return referenceType;
	}

	/**
	 * @param referenceType
	 *            the referenceType to set
	 */
	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	/**
	 * @return the referenceTypeImpl
	 */
	public String getReferenceTypeImpl() {
		return referenceTypeImpl;
	}

	/**
	 * @param referenceTypeImpl
	 *            the referenceTypeImpl to set
	 */
	public void setReferenceTypeImpl(String referenceTypeImpl) {
		this.referenceTypeImpl = referenceTypeImpl;
	}

	/**
	 * @return the referenceTypePrimaryKey
	 */
	public String getReferenceTypePrimaryKey() {
		return referenceTypePrimaryKey;
	}

	/**
	 * @param referenceTypePrimaryKey
	 *            the referenceTypePrimaryKey to set
	 */
	public void setReferenceTypePrimaryKey(String referenceTypePrimaryKey) {
		this.referenceTypePrimaryKey = referenceTypePrimaryKey;
	}

	/**
	 * @return the collection
	 */
	public boolean isCollection() {
		return collection;
	}

	/**
	 * Identifies if this attribute represents a collection of references. If
	 * true this is attribute represents a one-to-many association.
	 * 
	 * @param collection
	 *            the collection to set
	 */
	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue
	 *            the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the visibility
	 */
	public String getVisibility() {
		if (accessModifier != null) {
			this.visibility = accessModifier.toString();
		}
		return visibility;
	}

	/**
	 * @param visibility
	 *            the visibility to set
	 */
	public void setVisibility(String visibility) {
		if (!StringUtils.isEmpty(visibility)) {
			AccessModifier mod = AccessModifier
					.valueOfUncapAccessModifier(visibility);
			if (mod != null) {
				this.setAccessModifier(mod);
			}
		}
		this.visibility = visibility;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required
	 *            the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the primaryKey
	 */
	public boolean isPrimaryKey() {
		return primaryKey;
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * @return the fromRelation
	 */
	public boolean getFromRelation() {
		return fromRelation;
	}

	/**
	 * @param fromRelation
	 *            the fromRelation to set
	 */
	public void setFromRelation(boolean fromRelation) {
		this.fromRelation = fromRelation;
	}

	/**
	 * @return the cardinality
	 */
	public String getCardinality() {
		return cardinality;
	}

	/**
	 * Sets the cardinality of this attribute.
	 * 
	 * @param cardinality
	 *            the cardinality to set
	 */
	public void setCardinality(String cardinality) {
		this.cardinality = cardinality;
	}

	/**
	 * @param ordinalPosition
	 *            the ordinalPosition to set
	 */
	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}

	/**
	 * @return the ordinalPosition
	 */
	public int getOrdinalPosition() {
		return ordinalPosition;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param maxDisplaySize
	 *            the maxDisplaySize to set
	 */
	public void setMaxDisplaySize(String maxDisplaySize) {
		this.maxDisplaySize = maxDisplaySize;
	}

	/**
	 * @return the maxDisplaySize
	 */
	public String getMaxDisplaySize() {
		return maxDisplaySize;
	}

	/**
	 * @param relation
	 *            the relation to set
	 */
	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	/**
	 * @return the relation
	 */
	public Relation getRelation() {
		return relation;
	}

	/**
	 * The name of the attribute controlling the relation ship on the other side
	 * of this relation. This attribute must represent a relation attribute.
	 * TODO: This attribute should be an association instead
	 * 
	 * @param backedRelationAttribute
	 *            the backedRelationAttribute to set
	 */
	public void setBackedRelationAttribute(String backedRelationAttribute) {
		this.backedRelationAttribute = backedRelationAttribute;
	}

	/**
	 * @return the backedRelationAttribute
	 */
	public String getBackedRelationAttribute() {
		return backedRelationAttribute;
	}

	/**
	 * @param typeImpl
	 *            the typeImpl to set
	 */
	public void setTypeImpl(String typeImpl) {
		this.typeImpl = typeImpl;
	}

	/**
	 * @return the typeImpl
	 */
	public String getTypeImpl() {
		return typeImpl;
	}

	/**
	 * @param businessKey
	 *            the businessKey to set
	 */
	public void setBusinessKey(Boolean businessKey) {
		this.businessKey = businessKey;
	}

	public Boolean getBusinessKey() {
		return this.businessKey;
	}

	/**
	 * @return the businessKey
	 */
	public Boolean isBusinessKey() {
		return businessKey;
	}

	/**
	 * @param relationAttributeLabel
	 *            the relationAttributeLabel to set
	 */
	public void setRelationAttributeLabel(String relationAttributeLabel) {
		this.relationAttributeLabel = relationAttributeLabel;
	}

	/**
	 * @return the relationAttributeLabel
	 */
	public String getRelationAttributeLabel() {
		return relationAttributeLabel;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @param indexName
	 *            the indexName to set
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	/**
	 * @return the indexName
	 */
	public String getIndexName() {
		return indexName;
	}

	/**
	 * @param foreignKeyColumn
	 *            the foreignKeyColumn to set
	 */
	public void setForeignKeyColumn(String foreignKeyColumn) {
		this.foreignKeyColumn = foreignKeyColumn;
	}

	/**
	 * @return the foreignKeyColumn
	 */
	public String getForeignKeyColumn() {
		return foreignKeyColumn;
	}

	/**
	 * @param showOnView
	 *            the showOnView to set
	 */
	public void setShowOnView(Boolean showOnView) {
		this.showOnView = showOnView;
	}

	/**
	 * @return the showOnView
	 */
	public Boolean getShowOnView() {
		return showOnView;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * @param foreignKey
	 *            the foreignKey to set
	 */
	public void setForeignKey(Boolean foreignKey) {
		this.foreignKey = foreignKey;
	}

	/**
	 * @return the foreignKey
	 */
	public Boolean getForeignKey() {
		return foreignKey;
	}

	/**
	 * @param referencedColumnName
	 *            the referencedColumnName to set
	 */
	public void setReferencedColumnName(String referencedColumnName) {
		this.referencedColumnName = referencedColumnName;
	}

	/**
	 * @return the referencedColumnName
	 */
	public String getReferencedColumnName() {
		return referencedColumnName;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof AttributeValidator) {
			this.addValidator((AttributeValidator) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof AttributeValidator) {
			this.removeValidator((AttributeValidator) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.ModelAssociation#getTarget()
	 */
	@Override
	public BaseModel getTarget() {
		// The target must be computed dynamically as at this attribute
		// addition to its container the target may not have been added
		// yet.
		ModelLayer ml = this.getEnclosingProject().getModelLayer();
		return ml.getEntityByName(this.getReferenceType());
	}

	/*
	 * @see mddprocessorplugin.model.ModelAssociation#getSource()
	 */
	@Override
	public BaseModel getSource() {
		return (BaseModel) this.getParent();
	}

	/**
	 * @param accessModifierForJDT
	 */
	public void setAccessModifier(AccessModifier accessModifierForJDT) {
		this.visibility = accessModifierForJDT.toString();
		this.accessModifier = accessModifierForJDT;
	}

	public AccessModifier getAccessModifier() {
		return accessModifier;
	}

	/**
	 * @return
	 * @throws JavaModelException
	 */
	public static Attribute createDefaultAttribute(String name,
			AccessModifier modifier) {
		Attribute att = new Attribute();
		att.setName(name);
		att.setAccessModifier(modifier);
		att.setLabel(StringUtils.toPhrase(StringUtils
				.toUppercaseFirstLetter(att.getName())));
		att.setDescription("Attribute " + att.getName());
		att
				.setRelationAttributeLabel(Defaults.DEFAULT_RELATION_ATTRIBUTE_LABEL);
		att.setReference(false);
		att.setBusinessKey(false);
		att.setCollection(false);
		att.setColumnName(name);
		att.setForeignKey(false);
		att.setFromRelation(false);
		att.setPrimaryKey(false);
		att.setRequired(false);
		att.setShowOnView(true);
		return att;
	}

	/**
	 * Returns true only if this attribute is not from relationships
	 * and does not represents a primary key
	 * @return true if is not from relationship and does not represents
	 * a primary key 
	 */
	public boolean isNotRelationOrPrimaryKey() {
		if ((!this.getFromRelation()) && (!this.isPrimaryKey())) {
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(Attribute other) {
		//Attributes are sorted by their ordinal position
		if (other != null) {
			if (other.ordinalPosition > this.ordinalPosition) {
				return -1;
			} else {
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 * Returns whether this field is a Date field
	 * @return
	 */
	public boolean isDate() {
		if (this.getModelType() == null) {
			return false;
		}
		return AttributeUtil.isJavaDateType(this.getModelType().toString());
	}
	
	public boolean isTime() {
		if (this.getModelType() == null) {
			return false;
		}
		return AttributeUtil.isJavaTimeType(this.getModelType().toString());
	}
	
	public boolean isNumber() {
		if (this.getModelType() == null) {
			return false;
		}
		return AttributeUtil.isJavaNumberType(this.getModelType().toString());
	}
	

	public boolean isDateOrTime() {
		return this.isDate() || this.isTime();
	}
	public void setInitializerType(InitializerType initializerType) {
		this.initializerType = initializerType;
	}

	public InitializerType getInitializerType() {
		return initializerType;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setModelType(ModelType modelType) {
		this.modelType = modelType;
	}

	public ModelType getModelType() {
		return modelType;
	}

	public void setHashed(boolean hashed) {
		this.hashed = hashed;		
	}

	public boolean isHashed() {
		return hashed;
	}

	public void setHidden(boolean hideOnView) {
		this.hidden = hideOnView;
	}

	public boolean isHidden() {
		return hidden;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

}
