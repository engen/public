package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

public class BusinessKey extends BaseModel {

	/**
	 * 
	 */
	private transient static final long serialVersionUID = 1L;

	transient org.slf4j.Logger log = org.slf4j.LoggerFactory
			.getLogger(BusinessKey.class);

	private List<Attribute> attributes = new ArrayList<Attribute>();
	
	public BusinessKey() {
		this.setStereotypeName("businesskey");
		attributes = new ArrayList<Attribute>();
	}
	
	public void addAttribute(final Attribute att) {
		if (att == null) {
			throw new IllegalArgumentException(
					"The attribute to add can't be null");
		}
		if (this.hasAttribute(att.getName())) {
			throw new IllegalArgumentException("This entity [" + this.getName()
					+ "] already has an attribute with this name ["
					+ att.getName() + "]. Please change the name first.");
		}
		att.setParent(this);
		this.getAttributes().add(att);
		this.notifyModelChangeListeners();
	}
	
	public void removeAttribute(final Attribute att) {
		this.getAttributes().remove(att);
		this.notifyModelChangeListeners();
	}

	@Override
	public List<BaseModel> getChildren() {
		List<BaseModel> children = new ArrayList<BaseModel>();
		children.addAll(this.getAttributes());
		// children.addAll(this.getAssociations());
		return children;
	}
	
	/**
	 * @return the attributes
	 */
	public List<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void setAttributes(final List<Attribute> attributes) {
		for (final Attribute att : attributes) {
			att.setParent(this);
		}
		this.attributes = attributes;
		this.notifyModelChangeListeners();
	}

	/**
	 * Returns whether this entity has an attribute with the given name
	 * 
	 * @param name
	 * @return
	 */
	public boolean hasAttribute(String name) {
		for(Attribute att : attributes) {
			if (att.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof Attribute) {
			this.addAttribute((Attribute) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof Attribute) {
			this.removeAttribute((Attribute) child);
			return true;
		}
		return false;
	}

}
