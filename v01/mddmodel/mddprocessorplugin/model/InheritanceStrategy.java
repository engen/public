/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/
package mddprocessorplugin.model;

/**
 * Defines the inheritance strategies allowed for the project. JOINED is the
 * default.
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 9, 2009
 */
public enum InheritanceStrategy {
	/**
	 * Use no explicit inheritance mapping, and default runtime polymorphic
	 * behavior
	 **/
	TABLE_PER_CONCRETE_CLASS_IMPLICIT,
	/** Discard polymorphism and inheritance relationship from schema **/
	TABLE_PER_CONCRETE_CLASS,
	/** Denormalize schema and use discriminator column for identify subclasses **/
	TABLE_PER_CLASS,
	/** Use joined tables representing is a relationship as a has a relationship **/
	TABLE_PER_SUBCLASS,
	SINGLE_TABLE,
	JOINED;

}
