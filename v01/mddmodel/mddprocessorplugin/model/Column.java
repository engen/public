package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Column extends BaseModel implements Ordered {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 3454590356702043372L;
	private int size;
	private int scale;
	private String type;
	private int typeCode;
	private boolean required;
	private String defaultValue;
	private String sqlExtendType;
	private boolean primaryKey;
	private int precisionRadix;
	private int ordinalPosition;

	private List<ColumnValidator> validators = new ArrayList<ColumnValidator>();
	
	public Column() {
		this.setStereotypeName("column");
	}
	
	@Override
	public List<ColumnValidator> getChildren() {
		if (this.getValidators() == null) {
			this.validators = new ArrayList<ColumnValidator>();
		}
		return this.getValidators();
	}

   /**
	 * @return the validators
	 */
	public List<ColumnValidator> getValidators() {
		return validators;
	}

	/**
	 * @param validators the validators to set
	 */
	public void setValidators(List<ColumnValidator> validators) {
		for(ColumnValidator validator : validators) {
			validator.setParent(this);
		}
		this.validators = validators;
		this.notifyModelChangeListeners();
	}
	
	public void addValidator(ColumnValidator validator) {
		validator.setParent(this);
		this.getValidators().add(validator);
		this.notifyModelChangeListeners();
	}
	
	public void removeValidator(ColumnValidator validator) {
		this.getValidators().remove(validator);
		this.notifyModelChangeListeners();
	}
	
	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}
	/**
	 * @param scale the scale to set
	 */
	public void setScale(int scale) {
		this.scale = scale;
	}
	/**
	 * @return the scale
	 */
	public int getScale() {
		return scale;
	}
	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @param typeCode the typeCode to set
	 */
	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}
	/**
	 * @return the typeCode
	 */
	public int getTypeCode() {
		return typeCode;
	}
	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}
	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param sqlExtendType the sqlExtendType to set
	 */
	public void setSqlExtendType(String sqlExtendType) {
		this.sqlExtendType = sqlExtendType;
	}
	/**
	 * @return the sqlExtendType
	 */
	public String getSqlExtendType() {
		return sqlExtendType;
	}
	/**
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
		if (primaryKey) {
			this.setStereotypeName("pkcolumn");
		} else {
			this.setStereotypeName("column");
		}
	}
	/**
	 * @return the primaryKey
	 */
	public boolean isPrimaryKey() {
		return primaryKey;
	}
	/**
	 * @param precisionRadix the precisionRadix to set
	 */
	public void setPrecisionRadix(int precisionRadix) {
		this.precisionRadix = precisionRadix;
	}
	/**
	 * @return the precisionRadix
	 */
	public int getPrecisionRadix() {
		return precisionRadix;
	}
	/**
	 * @param ordinalPosition the ordinalPosition to set
	 */
	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}
	/**
	 * @return the ordinalPosition
	 */
	public int getOrdinalPosition() {
		return ordinalPosition;
	}
	
	public ColumnComparator getComparator() {
		return new ColumnComparator();
	}
	
	@SuppressWarnings({ "unchecked" })
	public static final class ColumnComparator implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			if (o1 instanceof Relation) {
				//Relations should come at the end.
				return 1;
			} else if (o2 instanceof Relation) {
				//Relations should come at the end.
				return -1;
			}
			Column c1 = (Column)o1;
			Column c2 = (Column)o2;
			return c1.getOrdinalPosition() - c2.getOrdinalPosition();
		}			
		
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof ColumnValidator) {
			this.addValidator((ColumnValidator) child);
			return true;
		}		
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof ColumnValidator) {
			this.removeValidator((ColumnValidator) child);
			return true;
		}				
		return false;
	}
}
