package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

public class FunctionCall implements Translatable {

	private Entity entity;
	private String function;
	private List<String> parameters = new ArrayList<String>();

	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public void addParameter(String value) {
		this.parameters.add(value);
	}

	public List<String> getParameters() {
		return parameters;
	}

	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String internalValue() {
		// The internal value for the FunctionCall will be calculated by the
		// FunctionCallTranslator class
		Translator translator;
		try {
			translator = TranslatorFactory
					.get(FunctionCall.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return translator.translate(this);
	}

	@Override
	public String toString() {
		return "FunctionCall [entity=" + entity + ", function=" + function
				+ ", parameters=" + parameters + "]";
	}

}
