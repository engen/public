package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a library used by the project.
 * @author Carlos Eugenio P. da Purificacao
 *
 */
public class Library extends BaseModel implements TemplateOwner {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8359384974053500855L;

	private String path;
	private List<Url> urls = new ArrayList<Url>();
	
	public Library() {
		super("library");
		this.urls = new ArrayList<Url>();
	}
	
 	@Override
	protected boolean addChild(StereotypedItem child) {
 		if (child instanceof Url) {
 			this.addUrl((Url)child);
 			return true;
 		}
		return false;
	}

	public void addUrl(Url url) {
		url.setParent(this);
		this.getUrls().add(url);
		this.notifyModelChangeListeners();
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
 		if (child instanceof Url) {
 			this.removeUrl((Url)child);
 			return true;
 		}		
		return false;
	}

	public void removeUrl(Url url) {
		url.setParent(null);
		this.getUrls().remove(url);
		this.notifyModelChangeListeners();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<Url> getUrls() {
		return urls;
	}

	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}

}
