package mddprocessorplugin.model;

/**
 * Represents a target in the model.
 * @author eugenio
 *
 */
public interface ModelTarget extends StereotypedItem {

	/**
	 * Returns the control module this element is child of
	 * @return the control module this element is child of
	 */
	abstract ControlModule getControlModule();

	abstract String getNamespace();

	abstract String getActionName();

}
