package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class ModelLayer extends BaseDiagram {

	private transient org.slf4j.Logger log = org.slf4j.LoggerFactory
			.getLogger(ModelLayer.class);
	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 3731192909850854976L;
	private String namespace = Defaults.MODEL_NAMESPACE;
	/* If true the generated classes are abstract */
	private Boolean generateEntitiesAsAbstract = Boolean.FALSE;
	/* If true the user entities source files skeleton are created. */
	private Boolean generateEntitySrcSkeleton = Boolean.FALSE;
	/**
	 * If new model layer attributes for entities are required by default when
	 * added to them
	 */
	private Boolean newAttributeIsRequired = Boolean.FALSE;
	/**
	 * The model layer can have modules. Entities can be stored in modules to
	 * provide more fine control on the entity name space.
	 */
	private List<Module> modules = new ArrayList<Module>();
	private List<Entity> entities = new ArrayList<Entity>();
	private List<ModelPackage> packages = new ArrayList<ModelPackage>();
	/**
	 * Customize the counter entity name which keeps track of all custom
	 * counters for entities in this project
	 */
	private String counterEntityName = Defaults.DEFAULT_COUNTER_ENTITY_NAME;

	public ModelLayer() {
		super("modellayer");
		initCollections();
	}

	/**
	 * Initialize all the collections for this layer.
	 */
	private void initCollections() {
		modules = new ArrayList<Module>();
		entities = new ArrayList<Entity>();
		packages = new ArrayList<ModelPackage>();
	}

	public ModelLayer(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
	}

	public void clear() {
		this.initCollections();
		this.notifyModelChangeListeners();
	}

	private String setStereotypeForThis() {
		String stereotype = "modellayer";
		this.setStereotypeName(stereotype);
		return stereotype;
	}

	public Map<String, Entity> getEntityMap() {
		Map<String, Entity> map = new HashMap<String, Entity>();
		for (Entity e : this.getAllEntities()) {
			if (e.getName() != null) {
				map.put(e.getName().toLowerCase(Locale.ENGLISH), e);
			} else {
				throw new NullPointerException(
						"The entity name must not be null.: " + e);
			}
		}
		return map;
	}

	/**
	 * @return the entities
	 */
	public List<Entity> getEntities() {
		return Collections.unmodifiableList(entities);
	}

	/**
	 * @param entities
	 *            the entities to set
	 */
	public void setEntities(List<Entity> entities) {
		for (Entity entity : entities) {
			entity.setParent(this);
		}
		this.entities = entities;
		this.notifyModelChangeListeners();
	}

	public void addEntity(Entity entity) {
		entity.setParent(this);
		entity.setModelLayer(this);
		this.entities.add(entity);
		this.notifyModelChangeListeners();
	}

	public boolean hasEntity(Entity entity) {
		return this.getEntities().contains(entity);
	}

	public boolean hasEntity(String name) {
		return this.getEntityMap().containsKey(name);
	}

	public Entity getEntityByName(String name) {
		for (Entity e : this.getAllEntities()) {
			if (org.apache.commons.lang.StringUtils.equals(name, e.getName())) {
				return e;
			}
		}
		return null;
	}

	public Attribute getAttributeByName(String name) {
		for (Iterator<Entity> it = this.getAllEntities().iterator(); it
				.hasNext();) {
			Entity e = it.next();
			for (Attribute att : e.getAllAttributes()) {
				if (org.apache.commons.lang.StringUtils.trimToNull(name) != null
						&& name.equals(att.getName())) {
					return att;
				}
			}
		}
		return null;
	}

	private void removeEntity(Entity entity) {
		this.entities.remove(entity);
		entity.setModelLayer(null);
		this.notifyModelChangeListeners();
	}

	public List<Entity> getAllEntities() {
		List<Entity> allEntities = new ArrayList<Entity>();
		allEntities.addAll(this.getEntities());
		if (this.getPackages() != null) {
			for (ModelPackage pack : this.getPackages()) {
				allEntities.addAll(pack.getAllEntities());
			}
		}
		return allEntities;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the modules
	 */
	public List<Module> getModules() {
		return modules;
	}

	/**
	 * @param modules
	 *            the modules to set
	 */
	public void setModules(List<Module> modules) {
		this.modules = modules;
		this.notifyModelChangeListeners();
	}

	public void addModule(Module module) {
		module.setParent(this);
		this.modules.add(module);
		this.notifyModelChangeListeners();
	}

	public void removeModule(Module module) {
		this.modules.remove(module);
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the packages
	 */
	public List<ModelPackage> getPackages() {
		return packages;
	}

	/**
	 * @param packages
	 *            the packages to set
	 */
	public void setPackages(List<ModelPackage> packages) {
		for (ModelPackage _package : packages) {
			_package.setParent(this);
		}
		this.packages = packages;
		this.notifyModelChangeListeners();
	}

	public void addPackage(ModelPackage pack) {
		pack.setParent(this);
		this.packages.add(pack);
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the generateEntitiesAsAbstract
	 */
	public Boolean getGenerateEntitiesAsAbstract() {
		return generateEntitiesAsAbstract;
	}

	/**
	 * @param generateEntitiesAsAbstract
	 *            the generateEntitiesAsAbstract to set
	 */
	public void setGenerateEntitiesAsAbstract(Boolean generateEntitiesAsAbstract) {
		this.generateEntitiesAsAbstract = generateEntitiesAsAbstract;
	}

	/**
	 * @return the generateEntitySrcSkeleton
	 */
	public Boolean getGenerateEntitySrcSkeleton() {
		return generateEntitySrcSkeleton;
	}

	/**
	 * @param generateEntitySrcSkeleton
	 *            the generateEntitySrcSkeleton to set
	 */
	public void setGenerateEntitySrcSkeleton(Boolean generateEntitySrcSkeleton) {
		this.generateEntitySrcSkeleton = generateEntitySrcSkeleton;
	}

	public void removePackage(ModelPackage pack) {
		this.packages.remove(pack);
		this.notifyModelChangeListeners();
	}

	@Override
	public List<StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		children.addAll(this.modules);
		children.addAll(this.entities);
		children.addAll(this.packages);
		return children;
	}

	/**
	 * Returns all the Attributes in the model that have the entity with the
	 * name provided as a target entity.
	 * 
	 * @param entityName
	 *            the target entity
	 * @return list of Attributes for the target entity
	 */
	public List<Attribute> getRelationsToTargetEntity(String entityName) {
		// log.debug("Verifying relations for " + entityName);
		List<Attribute> relationsToEntity = new ArrayList<Attribute>();
		for (Entity source : this.getAllEntities()) {
			for (Attribute relation : source.getRelationAttributesList()) {
				// Verify if the target for the relation is the
				// same that was provided
				if (relation.getReferenceType().equalsIgnoreCase(entityName)) {
					// log.debug("Relation attribute found: " + relation
					// + " , referencedType: "
					// + relation.getReferenceType()
					// + " , referecedTypeImpl: "
					// + relation.getReferenceTypeImpl());
					relationsToEntity.add(relation);
				}
			}
		}
		// log.debug("Attribute Relations found for entity [" + this + "] : "
		// + relationsToEntity);
		return relationsToEntity;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof Module) {
			this.addModule((Module) child);
			return true;
		}
		if (child instanceof Entity) {
			this.addEntity((Entity) child);
			return true;
		}
		if (child instanceof ModelPackage) {
			this.addPackage((ModelPackage) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof Module) {
			this.removeModule((Module) child);
			return true;
		}
		if (child instanceof Entity) {
			this.removeEntity((Entity) child);
			return true;
		}
		if (child instanceof ModelPackage) {
			this.removePackage((ModelPackage) child);
			return true;
		}
		return false;
	}

	public Boolean getNewAttributeIsRequired() {
		return newAttributeIsRequired;
	}

	public void setNewAttributeIsRequired(Boolean newAttributeIsRequired) {
		this.newAttributeIsRequired = newAttributeIsRequired;
	}

	public ModelPackage getPackageByName(String packageName) {
		for (ModelPackage pack : this.packages) {
			if (StringUtils.isNotEmpty(pack.getName())
					&& pack.getName().equals(packageName)) {
				return pack;
			}
		}
		return null;
	}

	/**
	 * If at least one entity has an attribute with a counter initializer than
	 * an Counter entity will be created to keep track of last counter for one
	 * entity. The table is composed of a current field which keeps track of
	 * current max counter and an entity which is the name of the entity who has
	 * a counter associated. Also for each entity a field is specified so the
	 * entity may have independently count fields TODO: What if an entity has
	 * more than one counter associated?
	 */
	public void createCounterEntity() {
		String counterName = this.getCounterEntityName();
		if (StringUtils.isEmpty(counterName)) {
			counterName = Defaults.DEFAULT_COUNTER_ENTITY_NAME;
		}
		if (!this.hasEntity(counterName)) {
			// Counter not found. Creating an entity with
			// the counter name so it can be created at file
			// generation. Also, a database table must be created
			// if the strategy is not use the table-generated
			Entity counterEntity = new Entity();
			counterEntity.setName(counterName);
			counterEntity.setTableName(counterName);
			// The counter primary key field
			Attribute counterId = new Attribute();
			counterId.setPrimaryKey(Boolean.TRUE);
			counterId.setName("id");
			counterId.setColumnName("ID");
			counterId.setType("Integer");
			counterId.setTypeImpl("Integer");
			counterEntity.add(counterId);
			// The entity name attribute
			Attribute counterNameAtt = new Attribute();
			counterNameAtt.setName("entity");
			counterNameAtt.setColumnName("ENTITY");
			counterNameAtt.setType("String");
			counterNameAtt.setTypeImpl("String");
			counterEntity.add(counterNameAtt);
			// The entity field name attribute
			Attribute counterFieldNameAtt = new Attribute();
			counterFieldNameAtt.setName("field");
			counterFieldNameAtt.setColumnName("FIELD");
			counterFieldNameAtt.setType("String");
			counterFieldNameAtt.setTypeImpl("String");
			counterEntity.add(counterFieldNameAtt);
			// The entity current attribute
			Attribute counterCurrentAtt = new Attribute();
			counterCurrentAtt.setName("current");
			counterCurrentAtt.setColumnName("CURRENT");
			counterCurrentAtt.setType("Long");
			counterCurrentAtt.setTypeImpl("Long");
			counterEntity.add(counterCurrentAtt);
			// Adding the new Counter entity to the model layer
			this.add(counterEntity);
		}
	}

	public void setCounterEntityName(String counterEntityName) {
		this.counterEntityName = counterEntityName;
	}

	public String getCounterEntityName() {
		return counterEntityName;
	}

}
