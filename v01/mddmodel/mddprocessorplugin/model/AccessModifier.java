/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/

package mddprocessorplugin.model;

import java.util.Locale;

import mddprocessorplugin.model.util.StringUtils;

import org.eclipse.jdt.core.Flags;

/**
 * Defines an abstract access modifier contract for types.
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 7, 2009
 */
public enum AccessModifier {

	PUBLIC, PRIVATE, PROTECTED, PACKAGE;

	private static org.slf4j.Logger log = org.slf4j.LoggerFactory
			.getLogger(AccessModifier.class);

	@Override
	public String toString() {
		return super.toString().toLowerCase(Locale.ENGLISH);
	}

	/**
	 * Returns an AccessModifier for a type using the JDT flags for that type.
	 * 
	 * @param flags
	 *            the JDT flags
	 * @return a suitable AccessModifier
	 */
	public static AccessModifier getEntityAccessModifierForJDT(int flags) {
		if (Flags.isPackageDefault(flags)) {
			return AccessModifier.PACKAGE;
		}
		if (Flags.isPrivate(flags)) {
			return AccessModifier.PRIVATE;
		}
		if (Flags.isProtected(flags)) {
			return AccessModifier.PROTECTED;
		}
		return AccessModifier.PUBLIC;
	}

	/**
	 * Returns an AccessModifier for a type using the JDT flags for that type.
	 * 
	 * @param flags
	 *            the JDT flags
	 * @return a suitable AccessModifier
	 */
	public static AccessModifier getFieldAccessModifierForJDT(int flags) {
		if (Flags.isPackageDefault(flags)) {
			return AccessModifier.PACKAGE;
		}
		if (Flags.isPublic(flags)) {
			return AccessModifier.PUBLIC;
		}
		if (Flags.isProtected(flags)) {
			return AccessModifier.PROTECTED;
		}
		return AccessModifier.PRIVATE;
	}
	
	/**
	 * Returns an access modifier based on a uncapitalized access modifier
	 * string like "private" or null if it is an invalid modifier.
	 * 
	 * @param uncapMod
	 *            uncapitalized access modifier string like "private"
	 * @return an AccessModifier for the string
	 */
	public static AccessModifier valueOfUncapAccessModifier(String uncapMod) {
		try {
			return valueOf(StringUtils.upperCase(uncapMod));
		} catch (Exception ex) {
			log.warn("Invalid modifier: " + uncapMod);
			return null;
		}
	}
}
