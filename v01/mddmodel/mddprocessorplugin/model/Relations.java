package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.db.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Container for all relations in a table.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eug�nio P. da Purifica��o</a>
 * @created 15/03/2009
 * @version 1.0
 */
public class Relations extends BaseModel implements Ordered {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -8156546218378090077L;
	
	private static final Logger log = LoggerFactory.getLogger(Relations.class);
	
	private Table table;
	private List<Relation> relations = new ArrayList<Relation>();
	
	public Relations() {
		super("relations");
		this.setName("Relations");
	}
	
	public List<Relation> getChildren() {
		return this.getRelations();
	}

	public void addRelations() {
		Table source = this.getTable();
		for (ForeignKey fk : source.getForeignKeys()) {
			//Find the target table for this foreign key.
			Table target = this.getTable().getDatabase().findTable(fk.getReferencedTable());
			//Columns on the source table.
			List<ColumnMap> colsOrigin = new ArrayList<ColumnMap>();
			List<ColumnMap> colsTarget = new ArrayList<ColumnMap>();
			//For each reference we add a ColumnMap, mapping the local column to the target
			//column in the foreign key.
			for (Reference ref : fk.getReferences()) {
				colsOrigin.add(new ColumnMap(ref.getSourceForeignKeyField(), ref.getTargetReferencedKey()));
				colsTarget.add(new ColumnMap(ref.getTargetReferencedKey(), ref.getSourceForeignKeyField()));
			}
			try {
				Relation relation = new Relation(this.getTable(), target, null, colsOrigin,
						colsTarget, "", "");
				addRelation(relation);
			} catch (Exception ex) {
				log.error("ERRO. RelationShip not created for ForeignKey ("
						+ fk + ". Table: " + fk.getReferencedTable() + "): " + ex, ex);
			}
		}
	}

	public void addRelation(Relation relation) {
		relation.setParent(this);
		this.getRelations().add(relation);
		this.notifyModelChangeListeners();
	}

	/**
	 * @param relations
	 *            the relations to set
	 */
	public void setRelations(List<Relation> relations) {
		for (Relation relation : relations) {
			relation.setParent(this);
		}
		this.relations = relations;
		this.notifyModelChangeListeners();
	}
	
	public void removeRelation(Relation relation) {
		this.getRelations().remove(relation);
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the relations
	 */
	public List<Relation> getRelations() {
		return relations;
	}
	
	public Table getTable() {
		return this.table;
	}

	/**
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Comparator getComparator() {
		return new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				// The relations folder is always bellow of any column.
				// It always will be the last item inside a table.
				return 1;
			}			
		};
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Relation) {
			this.addRelation((Relation) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Relation) {
			this.removeRelation((Relation) child);
			return true;
		}
		return false;
	}	
}
