package mddprocessorplugin.model;

public enum UrlType {

	CSS, JS;

	public boolean equals(String type) {
		return this.toString().toLowerCase().equals(type);
	}
}
