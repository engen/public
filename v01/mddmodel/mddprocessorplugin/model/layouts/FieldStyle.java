package mddprocessorplugin.model.layouts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mddprocessorplugin.model.BaseModel;
import mddprocessorplugin.model.StereotypedItem;

public class FieldStyle extends BaseModel {

	private static final long serialVersionUID = 1L;

	private Integer span;
	private String style;
	/**
	 * All the types this field style applies to
	 */
	private List<String> fieldTypes = new ArrayList<String>();
	
	private Map<String, List<String>> styles = new HashMap<String, List<String>>();

	private String extendsFieldStyle;
	private String rowStyle;
	private String labelStyle;
	private String parentStyle;

	public FieldStyle() {
		super("fieldstyle");
	}

	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}

	public Integer getSpan() {
		return span;
	}

	public void setSpan(Integer span) {
		this.span = span;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getExtendsFieldStyle() {
		return extendsFieldStyle;
	}

	public void setExtendsFieldStyle(String extendsFieldStyle) {
		this.extendsFieldStyle = extendsFieldStyle;
	}

	public String getRowStyle() {
		return rowStyle;
	}

	public void setRowStyle(String rowStyle) {
		this.rowStyle = rowStyle;
	}

	public String getLabelStyle() {
		return labelStyle;
	}

	public void setLabelStyle(String labelStyle) {
		this.labelStyle = labelStyle;
	}

	public List<String> getFieldTypes() {
		return fieldTypes;
	}

	public void setFieldTypes(List<String> fieldTypes) {
		this.fieldTypes = fieldTypes;
	}

	/**
	 * Returns the styles configured for the specified element name
	 * 
	 * @param elementName
	 *            the name of the element
	 * @return the list of styles configured for the given element
	 */
	public List<String> getStyles(String elementName) {
		return styles.get(elementName);
	}

	public String getParentStyle() {
		return parentStyle;
	}

	public void setParentStyle(String parentStyle) {
		this.parentStyle = parentStyle;
	}

	@Override
	public String toString() {
		return "FieldStyle [span=" + span + ", style=" + style
				+ ", fieldTypes=" + fieldTypes + ", styles=" + styles
				+ ", extendsFieldStyle=" + extendsFieldStyle + ", rowStyle="
				+ rowStyle + ", labelStyle=" + labelStyle + ", parentStyle="
				+ parentStyle + "]";
	}

}
