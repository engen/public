package mddprocessorplugin.model.layouts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mddprocessorplugin.model.BaseModel;
import mddprocessorplugin.model.FieldType;
import mddprocessorplugin.model.InputField;
import mddprocessorplugin.model.StereotypedItem;
import mddprocessorplugin.model.ViewSectionType;

import org.apache.commons.lang3.StringUtils;
import org.engeny.velocity.TraceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a layout_style as defined in the DSL.
 * 
 * @author Carlos Eugenio P. da Purificacao
 * 
 */
public class LayoutStyle extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected transient static Logger log = LoggerFactory
			.getLogger(LayoutStyle.class);
	private List<FieldStyle> fieldStyles = new ArrayList<FieldStyle>();
	/**
	 * Number of blocks in each row for this style
	 */
	private Integer blocks;
	/**
	 * The style for each row
	 */
	private String rowStyle;
	/**
	 * The style for every field in this layout. Subsequent styles may only add
	 * styles to this.
	 */
	private String fieldStyle;
	/**
	 * Style for each block in a row
	 */
	private List<String> blockStyles = new ArrayList<String>();
	/**
	 * Specific section type IDs. The key is the section type, e.g.: "body" and
	 * the value is the id associated with that section by the layout model
	 */
	private Map<String, String> sectionIds = new HashMap<String, String>();

	/**
	 * Holds style classes definitions for all elements in the model or target
	 * architecture view. The key is the element type, e.g.: "body" and the
	 * value are the style classes associated with that section by the layout
	 * model
	 */
	private Map<String, List<String>> styleClasses = new HashMap<String, List<String>>();

	public LayoutStyle() {
		super("layoutstyle");
		fieldStyles = new ArrayList<FieldStyle>();
		styleClasses = new HashMap<String, List<String>>();
		sectionIds = new HashMap<String, String>();
	}

	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof FieldStyle) {
			this.addFieldStyle((FieldStyle) child);
			return true;
		}
		return false;
	}

	private void addFieldStyle(FieldStyle child) {
		child.setParent(this);
		this.getFieldStyles().add(child);
		this.notifyModelChangeListeners();
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof FieldStyle) {
			this.removeFieldStyle((FieldStyle) child);
			return true;
		}
		return false;
	}

	private void removeFieldStyle(FieldStyle child) {
		child.setParent(null);
		this.getFieldStyles().remove(child);
		this.notifyModelChangeListeners();
	}

	/**
	 * Returns the correct {@link FieldStyle} for the {@link InputField}
	 * checking its type for determining it.
	 */
	public FieldStyle getStyleForField(InputField field) {
		String type = field.getFormFieldType();
		try {
			FieldType fieldType = FieldType.get(type);
			if (fieldType == null) {
				throw new RuntimeException("Fieldtype not found for->" + type);
			}
			FieldStyle fs = getFieldStyleForFieldType(fieldType);
			log.debug("FieldStyle for field " + field + ", found: " + fs);
			field.setStyle(fs.getStyle());
			return fs;
		} catch (Exception ex) {
			log.error("Exception for field: " + field.getName()
					+ ", converting type: " + type + ". : " + ex.toString(), ex);
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Returns a String of all Style Classes associated with a section type
	 * 
	 * @param sectionType
	 *            the section type to return the style classes
	 * @return the style classes associated with the given section type
	 */
	public String getStyleForSectionType(ViewSectionType sectionType) {
		return getStyleFor(sectionType.toString());
	}

	/**
	 * Returns the configured style class for an element
	 * 
	 * @param elementName
	 *            the element to return the style for
	 * @return the configured style for the given element name
	 */
	public String getStyleFor(String elementName) {
		StringBuilder sb = new StringBuilder();
		log.debug("Getting styles for element [" + elementName
				+ "].\nCurrent: " + styleClasses + "\nContains: "
				+ styleClasses.containsKey(elementName) + "\nElement: "
				+ styleClasses.get(elementName));
		List<String> elementStyles = styleClasses.get(elementName);
		if (elementStyles != null) {
			for (String styleClass : elementStyles) {
				if (sb.length() > 0) {
					sb.append(" ");
				}
				sb.append(styleClass);
			}
		}
		return sb.toString();
	}

	/**
	 * Returns the correct {@link FieldStyle} for the given {@link FieldType}
	 * 
	 * @param fieldType
	 *            the {@link FieldType} to find the {@link FieldStyle}
	 * @return the {@link FieldStyle} for the type
	 */
	public FieldStyle getFieldStyleForFieldType(FieldType fieldType) {
		log.debug("Searching style for fieldType: " + fieldType);
		log.debug("All field styles: " + fieldStyles);
		for (FieldStyle style : fieldStyles) {
			List<String> fieldTypes = style.getFieldTypes();
			// log.info("Testing current field type [" + fieldType
			// + "] against types: " + fieldTypes);
			if (fieldTypes.contains(fieldType.toString())) {
				return style;
			}
		}
		if (!FieldType.ANY.equals(fieldType)) {
			// FieldType Not found. Try the ANY field type
			FieldStyle fieldStyleForAnyField = getFieldStyleForFieldType(FieldType.ANY);
			if (fieldStyleForAnyField != null) {
				// That's OK. We found a FieldType "any" declaration which will
				// be applied to any field type which doesn't have a specific
				// field style definition in the DSL
				return fieldStyleForAnyField;
			} else {
				RuntimeException exception = new RuntimeException(
						"FieldStyle for ["
								+ fieldType
								+ "] not found in configuration and Generic ANY "
								+ "field type style not defined.");
				TraceUtil.getTraceString(exception);
				throw exception;
			}
		} else {
			// Using fieldType Any didn't work. Return from recursion and report
			return null;
		}
	}

	/**
	 * This method now is just a shortcut for calling
	 * {@link #getStyleForField(InputField)}
	 * 
	 * @param field
	 *            the field to define the style for
	 */
	public void defineFieldStyle(InputField field) {
		FieldStyle fs = getStyleForField(field);
		if (fs != null) {
			field.setStyle(fs.getStyle());
			field.setLabelStyle(fs.getLabelStyle());
		}
	}

	/**
	 * Called to define the style property for a {@link InputField}. This
	 * operation will be considering first the generic style defined for the
	 * layout (in DSL the <code>layout_style</code> the <code>field</code>
	 * property of <code>style</code> sub-element), then the
	 * <code>field_style</code> with type "any" defined will be applied if
	 * defined. Finally the specific field style will be applied if it was
	 * defined. If none has been defined an empty style called
	 * "no_style_defined" will be applied to the field.
	 * 
	 * @param field
	 * @deprecated
	 */
	private void _defineFieldStyle(InputField field) {
		// First reset the field style to an empty string
		field.setStyle(StringUtils.EMPTY);
		// The initial style is the field style defined for the layout
		// (layout_style { style { field:"styleX" } }). Additional styles will
		// be appended to it according to each field style (field_style {
		// style="styleX" })
		String _fieldStyle = StringUtils.trimToEmpty(this.getFieldStyle());
		field.addStyle(_fieldStyle);
		// After defining the initial style, compute the style based on
		// LayoutStyle rules
		String fieldStyleComputed = StringUtils.EMPTY;
		// First set the generic ANY style to the field if it is defined
		FieldStyle genericAnyStyle = this
				.getFieldStyleForFieldType(FieldType.ANY);
		if (genericAnyStyle != null) {
			fieldStyleComputed = genericAnyStyle.getStyle();
			field.setLabelStyle(genericAnyStyle.getLabelStyle());
			if (log.isDebugEnabled()) {
				log.debug("The generic ANY style produced computed style :["
						+ fieldStyleComputed + "]");
				log.debug("Getting the correct enumeration value for InputField type: ["
						+ field.getType() + "]");
			}
		} else {
			log.warn("A generic ANY field style should have been defined!");
		}
		// Use the enumeration to get the correct field type
		FieldType fieldType = FieldType.get(field.getType());
		if (log.isDebugEnabled()) {
			log.debug("FieldType defined by Enumeration: [" + fieldType + "]");
		}
		// Now get the specific field style for this field type
		FieldStyle style = this.getFieldStyleForFieldType(fieldType);
		// If a style for this field type was defined then set field properties.
		if (style != null) {
			if (StringUtils.isNotEmpty(style.getStyle())) {
				fieldStyleComputed = style.getStyle();
			} else {
				// TODO: Check if this is really a requirement.
				throw new IllegalArgumentException(
						"A style for this field type ["
								+ fieldType
								+ "] was defined but its empty."
								+ "\n Although it should be valid "
								+ "(to define a generic style) it is not recomended.");
			}
			field.setLabelStyle(style.getLabelStyle());
		} else {
			log.warn("A field style was not defined for field type: "
					+ fieldType);
		}
		// The final style computed is added to the field style, as the
		// layout style cannot be overridden here. If needed use the style
		// property for the field section (section { field { style="styleX" } })
		log.debug("Final Style computed for field [" + field.getName()
				+ "], with current style[" + field.getStyle() + "]: "
				+ fieldStyleComputed);
		if (StringUtils.isNotEmpty(fieldStyleComputed)) {
			field.addStyle("FieldStyleComputed " + fieldStyleComputed);
		} else {
			log.warn("FieldStyleComputed is empty for field: ["
					+ field.getName() + "] type [" + field.getType() + "]");
			field.addStyle("EMPTYStyleComputed");
		}
		if (StringUtils.isEmpty(field.getStyle())) {
			log.error("This field [" + field.getName() + "] with type ["
					+ field.getType() + "] has an empty style definition.");
			String message = "Check if it is by desing, as it should, at least, "
					+ "have the same as ANY definition";
			log.error(message);
			throw new RuntimeException(message);
		}
	}

	public List<FieldStyle> getFieldStyles() {
		return fieldStyles;
	}

	public void setFieldStyles(List<FieldStyle> fieldStyles) {
		this.fieldStyles = fieldStyles;
	}

	public Integer getBlocks() {
		return blocks;
	}

	public void setBlocks(Integer blocks) {
		this.blocks = blocks;
	}

	public String getRowStyle() {
		return rowStyle;
	}

	public void setRowStyle(String rowStyle) {
		this.rowStyle = rowStyle;
	}

	public List<String> getBlockStyles() {
		return blockStyles;
	}

	public void setBlockStyles(List<String> blockStyles) {
		this.blockStyles = blockStyles;
	}

	public Map<String, List<String>> getStyleClasses() {
		return styleClasses;
	}

	public void setStyleClasses(Map<String, List<String>> styleClasses) {
		this.styleClasses = styleClasses;
	}

	public Map<String, String> getSectionIds() {
		return sectionIds;
	}

	public void setSectionIds(Map<String, String> sectionIds) {
		this.sectionIds = sectionIds;
	}

	public String getFieldStyle() {
		return fieldStyle;
	}

	public void setFieldStyle(String fieldStyle) {
		this.fieldStyle = fieldStyle;
	}

	@Override
	public String toString() {
		return "LayoutStyle [fieldStyles=\n" + fieldStyles + "\n, blocks="
				+ blocks + ", rowStyle=" + rowStyle + ", fieldStyle="
				+ fieldStyle + "\n, blockStyles=" + blockStyles + "\n, Styles="
				+ styleClasses + "]\n";
	}

	/**
	 * Add the given styleClasses to the element
	 * 
	 * @param elementName
	 * @param array
	 */
	public void addStyleClasses(String elementName, String[] array) {
		styleClasses.put(elementName, Arrays.asList(array));
	}

}
