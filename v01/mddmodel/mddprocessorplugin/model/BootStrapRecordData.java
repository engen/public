package mddprocessorplugin.model;

public class BootStrapRecordData {

	private String entityName;
	private String attributeValue;
	private String attributeType;
	private String attributeName;
	
	@Override
	public String toString() {
		return "BootStrapRecordData [entityName=" + entityName
				+ ", attributeValue=" + attributeValue + ", attributeType="
				+ attributeType + ", attributeName=" + attributeName + "]";
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	
}
