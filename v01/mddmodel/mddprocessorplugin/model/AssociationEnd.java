/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/

package mddprocessorplugin.model;

/**
 * Represents an end point for an association.
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Aug 1, 2009
 */
public class AssociationEnd extends BaseModel {

	/**
	 * Generated serial version id
	 */
	private static final long serialVersionUID = 4906090841380231138L;

	/** The association this end belongs to. To easy access, can be parent also. **/
	private Association source;
	/** This association end target **/
	private Entity target;

	public AssociationEnd() {
		this.setStereotypeName("associationend");
	}
	
	public Association getSource() {
		return source;
	}

	public void setSource(Association source) {
		this.source = source;
	}

	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}

}
