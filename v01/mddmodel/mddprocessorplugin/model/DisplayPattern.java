package mddprocessorplugin.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * The pattern used to display fields in the view.
 * For example for ModelType.date it can be "dd/mm/yyyy" so dates will
 * be displayed using this pattern.
 * @author eugenio
 *
 */
public enum DisplayPattern {

	date, time;
	private static Map<ModelType, String> patterns = new HashMap<ModelType, String>();
	static {
		patterns.put(ModelType.DATE, "dd/MM/yyyy");
		patterns.put(ModelType.TIME, "HH:mm:ss");
	}
	public static String getPatternFor(ModelType modelType) {
		if (modelType == null) {
			return StringUtils.EMPTY;
		}
		return patterns.get(modelType);
	}

	
}
