/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/


package mddprocessorplugin.model;

/**
 * The contract for an association in the model. Model objects that
 * represents an association between two model elements should implement
 * this interface in order to be usable in graphical environments where
 * this association can be done and undone.
 * @author Carlos Eugenio P. da Purificacao
 * Created on Aug 6, 2009
 */
public interface ModelAssociation {

	/**
	 * Returns the source for this association. Source of associations
	 * usually control the association life-cycle.
	 * TODO: Shouldn't we have a AssociationSource interface?
	 * @return the source for the association.
	 */
	BaseModel getSource();
	
	/**
	 * Returns the target model for this connection.
	 * TODO: Shouldn't we have a AssociationTarget interface?
	 * @return the target for the association.
	 */
	BaseModel getTarget();
	
}
