package mddprocessorplugin.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Decouple DSL PredefinedTypes from ModelTypes so model
 * is not dependent on DSL
 * @author eugenio
 *
 */
public enum ModelType {

	  /**
	   * The '<em><b>String</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #STRING_VALUE
	   * @generated
	   * @ordered
	   */
	  STRING(0, "string", "string"),

	  /**
	   * The '<em><b>Longstring</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #LONGSTRING_VALUE
	   * @generated
	   * @ordered
	   */
	  LONGSTRING(1, "longstring", "longstring"),

	  /**
	   * The '<em><b>Int</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #INT_VALUE
	   * @generated
	   * @ordered
	   */
	  INT(2, "int", "int"),

	  /**
	   * The '<em><b>Long</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #LONG_VALUE
	   * @generated
	   * @ordered
	   */
	  LONG(3, "long", "long"),

	  /**
	   * The '<em><b>Date</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #DATE_VALUE
	   * @generated
	   * @ordered
	   */
	  DATE(4, "date", "date"),

	  /**
	   * The '<em><b>Char</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #CHAR_VALUE
	   * @generated
	   * @ordered
	   */
	  CHAR(5, "char", "char"),

	  /**
	   * The '<em><b>Boolean</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #BOOLEAN_VALUE
	   * @generated
	   * @ordered
	   */
	  BOOLEAN(6, "boolean", "boolean"),

	  /**
	   * The '<em><b>Time</b></em>' literal object.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @see #TIME_VALUE
	   * @generated
	   * @ordered
	   */
	  TIME(7, "time", "time"),

	  OBJECT(8, "object", "object"),
	  
	  IMAGE(9, "image", "image");
	  /**
	   * The '<em><b>String</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>String</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #STRING
	   * @model name="string"
	   * @generated
	   * @ordered
	   */
	  public static final int STRING_VALUE = 0;

	  /**
	   * The '<em><b>Longstring</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Longstring</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #LONGSTRING
	   * @model name="longstring"
	   * @generated
	   * @ordered
	   */
	  public static final int LONGSTRING_VALUE = 1;

	  /**
	   * The '<em><b>Int</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Int</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #INT
	   * @model name="int"
	   * @generated
	   * @ordered
	   */
	  public static final int INT_VALUE = 2;

	  /**
	   * The '<em><b>Long</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Long</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #LONG
	   * @model name="long"
	   * @generated
	   * @ordered
	   */
	  public static final int LONG_VALUE = 3;

	  /**
	   * The '<em><b>Date</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Date</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #DATE
	   * @model name="date"
	   * @generated
	   * @ordered
	   */
	  public static final int DATE_VALUE = 4;

	  /**
	   * The '<em><b>Char</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Char</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #CHAR
	   * @model name="char"
	   * @generated
	   * @ordered
	   */
	  public static final int CHAR_VALUE = 5;

	  /**
	   * The '<em><b>Boolean</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Boolean</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #BOOLEAN
	   * @model name="boolean"
	   * @generated
	   * @ordered
	   */
	  public static final int BOOLEAN_VALUE = 6;

	  /**
	   * The '<em><b>Time</b></em>' literal value.
	   * <!-- begin-user-doc -->
	   * <p>
	   * If the meaning of '<em><b>Time</b></em>' literal object isn't clear,
	   * there really should be more of a description here...
	   * </p>
	   * <!-- end-user-doc -->
	   * @see #TIME
	   * @model name="time"
	   * @generated
	   * @ordered
	   */
	  public static final int TIME_VALUE = 7;
	  
	  public static final int OBJECT_VALUE = 8;
	  
	  public static final int IMAGE_VALUE = 9;
	  /**
	   * An array of all the '<em><b>Predefined Type</b></em>' enumerators.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  private static final ModelType[] VALUES_ARRAY =
	    new ModelType[]
	    {
	      STRING,
	      LONGSTRING,
	      INT,
	      LONG,
	      DATE,
	      CHAR,
	      BOOLEAN,
	      TIME,
	      OBJECT,
	      IMAGE
	    };

	  /**
	   * A public read-only list of all the '<em><b>Predefined Type</b></em>' enumerators.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public static final List<ModelType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	  /**
	   * Returns the '<em><b>Predefined Type</b></em>' literal with the specified literal value.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public static ModelType get(String literal)
	  {
	    for (int i = 0; i < VALUES_ARRAY.length; ++i)
	    {
	    	ModelType result = VALUES_ARRAY[i];
	      if (result.toString().equals(literal))
	      {
	        return result;
	      }
	    }
	    return null;
	  }

	  /**
	   * Returns the '<em><b>Predefined Type</b></em>' literal with the specified name.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public static ModelType getByName(String name)
	  {
	    for (int i = 0; i < VALUES_ARRAY.length; ++i)
	    {
	    	ModelType result = VALUES_ARRAY[i];
	      if (result.getName().equals(name))
	      {
	        return result;
	      }
	    }
	    return null;
	  }

	  /**
	   * Returns the '<em><b>Predefined Type</b></em>' literal with the specified integer value.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public static ModelType get(int value)
	  {
	    switch (value)
	    {
	      case STRING_VALUE: return STRING;
	      case LONGSTRING_VALUE: return LONGSTRING;
	      case INT_VALUE: return INT;
	      case LONG_VALUE: return LONG;
	      case DATE_VALUE: return DATE;
	      case CHAR_VALUE: return CHAR;
	      case BOOLEAN_VALUE: return BOOLEAN;
	      case TIME_VALUE: return TIME;
	      case OBJECT_VALUE: return OBJECT;
	      case IMAGE_VALUE: return IMAGE;
	    }
	    return null;
	  }

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  private final int value;

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  private final String name;

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  private final String literal;

	  /**
	   * Only this class can construct instances.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  private ModelType(int value, String name, String literal)
	  {
	    this.value = value;
	    this.name = name;
	    this.literal = literal;
	  }

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public int getValue()
	  {
	    return value;
	  }

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public String getName()
	  {
	    return name;
	  }

	  /**
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  public String getLiteral()
	  {
	    return literal;
	  }

	  /**
	   * Returns the literal value of the enumerator, which is its string representation.
	   * <!-- begin-user-doc -->
	   * <!-- end-user-doc -->
	   * @generated
	   */
	  @Override
	  public String toString()
	  {
	    return literal;
	  }

}
