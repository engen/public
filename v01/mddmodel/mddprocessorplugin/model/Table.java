package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.db.Index;
import mddprocessorplugin.db.Reference;

import org.apache.commons.lang.StringUtils;

public class Table extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -8607027925846654467L;
	
	private Database database;

	private List<Column> columns = new ArrayList<Column>();

	private List<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();

	private List<Index> indexes = new ArrayList<Index>();

	private final List<RelationshipRole> relationsipRoles = new ArrayList<RelationshipRole>();

	//private Relations relations = new Relations();
	private List<Relation> relations = new ArrayList<Relation>();

	/*
	 * private final static org.apache.commons.collections.Predicate
	 * UNIQUE_PREDICATE = new org.apache.commons.collections.Predicate() {
	 * public boolean evaluate(Object input) { return ((Index)input).isUnique();
	 * } };
	 */

	public Table() {
		this.setStereotypeName("table");
	}

	/**
	 * @return the columns
	 */
	public List<Column> getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(List<Column> columns) {
		for (Column column : columns) {
			column.setParent(this);
		}
		this.columns = columns;
		this.notifyModelChangeListeners();
	}

	public void addColumn(Column column) {
		column.setParent(this);
		this.columns.add(column);
		this.notifyModelChangeListeners();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List getChildren() {
		List children = new ArrayList();
		children.addAll(this.getColumns());
		children.addAll(this.getRelations());
		return children;
	}

	/**
	 * @param indexes
	 *            the indexes to set
	 */
	public void setIndexes(List<Index> indexes) {
		// For now index is not part of the mddprocessor model hierarchy
		/*
		 * for(Index index : indexes) { index.setParent(this); }
		 */
		this.indexes = indexes;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the indexes
	 */
	public List<Index> getIndexes() {
		return indexes;
	}

	public void addIndex(Index next) {
		this.indexes.add(next);
		this.notifyModelChangeListeners();
	}

	/**
	 * @param foreignKeys
	 *            the foreignKeys to set
	 */
	public void setForeignKeys(List<ForeignKey> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	/**
	 * @return the foreignKeys
	 */
	public List<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}
	
	/**
	 * Returns the foreign key for the relation role defined.
	 * @param role the relation ship role to be looked for.
	 * @return the foreign key corresponding to this role.
	 */
	public ForeignKey getForeignKeyForRole(RelationshipRole role) {
		//The target role relation
		Relation targetRoleRelation = role.getTargetRole().getRelation();
		if (targetRoleRelation != null) {
			if (targetRoleRelation.getRightColumnsMap() != null) {
				List<ColumnMap> targetRoleRightColumnMap = targetRoleRelation.getRightColumnsMap();
				if (targetRoleRightColumnMap!=null && targetRoleRightColumnMap.iterator().hasNext()) {
					ColumnMap columnMap = targetRoleRightColumnMap.iterator().next();
					//Getting the foreignKey defined for the targetRole relation
					String foreignKeyForRole = columnMap.getForeignKey();
					if (StringUtils.trimToNull(foreignKeyForRole) != null) {
						for(ForeignKey fk : this.getForeignKeys()) {
							//Verify if this foreign key is referencing the same table as the target for the role.
							if (StringUtils.trimToEmpty(fk.getFkColumn()).equals(foreignKeyForRole)) {
								return fk;
							}
						}
					}					
				}
			}
		}
		return null;
	}

	public void addForeignKey(ForeignKey next) {
		this.foreignKeys.add(next);
		this.notifyModelChangeListeners();
	}

	/**
	 * Go through all the foreign keys for this table and return
	 * the all foreign' key references if this table has the same name
	 * as the referenced table.
	 * @param table
	 * @return
	 */
	public List<Reference> getReferencesForTable(Table table) {
		for (ForeignKey fk : getForeignKeys()) {
			if (table.getName().equals(fk.getReferencedTable())) {
				return fk.getReferences();
			}
		}
		return Collections.emptyList();
	}

	public int getCountReferencesForTable(Table table) {
		return getReferencesForTable(table).size();
	}

	public Column getColumn(String columnName) {
		for (Column c : this.columns) {
			if (c.getName() != null && c.getName().equals(columnName)) {
				return c;
			}
		}
		return null;
	}

	public void addRelationshipRole(RelationshipRole relationshipRole) {
		relationsipRoles.add(relationshipRole);
		this.notifyModelChangeListeners();
	}

	public int getCountPrimaryKeyColumns() {
		return getPrimaryKeyColumns().size();
	}

	public List<Column> getPrimaryKeyColumns() {
		List<Column> answer = new ArrayList<Column>();
		for (Column column : getColumns()) {
			if (column.isPrimaryKey()) {
				answer.add(column);
			}
		}
		return answer;
	}

	/**
	 * Returns whether this table has a column.
	 * 
	 * @param c
	 *            the column
	 * @return true if this table already contains the column.
	 */
	public boolean hasTable(Column c) {
		return this.columns.contains(c);
	}

	public void removeColumn(Column c) {
		this.columns.remove(c);
	}

	/**
	 * Finds the table with the specified name, using case insensitive matching.
	 * Note that this method is not called getColumn(String) to avoid
	 * introspection problems.
	 * 
	 * @param name
	 *            Describe what the parameter does @todo-javadoc Write javadocs
	 *            for method parameter
	 * @return Describe the return value @todo-javadoc Write javadocs for return
	 *         value
	 */
	public Column findColumn(String name) {
		for (Column column : getColumns()) {
			// column names are typically case insensitive
			if (column.getName().equalsIgnoreCase(name)) {
				return column;
			}
		}
		return null;
	}

	/**
	 * @param database
	 *            the database to set
	 */
	public void setDatabase(Database database) {
		this.database = database;
	}

	/**
	 * @return the database
	 */
	public Database getDatabase() {
		return database;
	}	
	

	public void addRelation(Relation relation) {
		relation.setParent(this);
		this.getRelations().add(relation);
		this.notifyModelChangeListeners();
	}

	/**
	 * @param relations
	 *            the relations to set
	 */
	public void setRelations(List<Relation> relations) {
		for (Relation relation : relations) {
			relation.setParent(this);
		}
		this.relations = relations;
		this.notifyModelChangeListeners();
	}
	
	public void removeRelation(Relation relation) {
		this.getRelations().remove(relation);
		this.notifyModelChangeListeners();
	}
		
	/**
	 * @return the relations
	 */
	public List<Relation> getRelations() {
		if (relations == null) {
			relations = new ArrayList<Relation>();
		}
		return relations;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Relation) {
			this.addRelation((Relation) child);
			return true;
		}
		if (child instanceof Column) {
			this.addColumn((Column) child);
			return true;
		}		
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Relation) {
			this.removeRelation((Relation) child);
			return true;
		}
		if (child instanceof Column) {
			this.removeColumn((Column) child);
			return true;
		}		
		return false;
	}

}
