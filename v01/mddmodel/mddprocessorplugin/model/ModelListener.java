/*
 * ModelListener.java
 *
 * Created on September 29, 2004, 9:35 AM
 */

package mddprocessorplugin.model;

/**
 *
 * @author  Carlos Eugenio P. da Purificacao
 */
public interface ModelListener {
    
    public void modelChanged(ModelEvent evt);
}
