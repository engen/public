/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/


package mddprocessorplugin.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a diagram of entities in the model. The entities are
 * place holders or proxies for entities which have their own location and
 * size separated from the real entity. So diagrams can have their own layout
 * apart from the real entity layout.
 * @author Carlos Eugenio P. da Purificacao
 * Created on Aug 3, 2009
 */
public class EntityDiagram extends BaseDiagram implements PropertyChangeListener {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -3591892649055775971L;

	private List<VisualEntityProxy> visualEntityProxies = new ArrayList<VisualEntityProxy>();
	private DiagramLayer diagramLayer;
	/**
	 * Creates an empty diagram.
	 */
	public EntityDiagram() {
		super("entitydiagram");
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		return this.getVisualEntityProxies();
	}

	/**
	 * Returns all visual proxied entities in the diagram 
	 * @return all visual proxied entities in the diagram
	 */
	public List<VisualEntityProxy> getVisualEntityProxies() {
		if (this.visualEntityProxies == null) {
			this.visualEntityProxies = new ArrayList<VisualEntityProxy>();
		}
		return visualEntityProxies;
	}

	/**
	 * Bunk sets all passed entities in the diagram. This will
	 * NOT affect the model.
	 * @param entities
	 */
	public void setVisualEntityProxies(List<VisualEntityProxy> entities) {
		for(VisualEntityProxy entity : entities) {
			entity.setParent(this);
		}
		this.visualEntityProxies = entities;
		this.notifyModelChangeListeners();
	}

	/**
	 * Adds a proxied visual entity to the diagram. 
	 * This will NOT add the proxied entity to the model.
	 * The diagram will listen to changes in the proxied model.
	 * If it is deleted then the visual entity proxy will be
	 * deleted as well as it can't exist without a model.
	 * @param entity the visual proxied entity to be added to the
	 * diagram.
	 */
	private void addVisualEntityProxy(VisualEntityProxy entity) {
		entity.setParent(this);
		this.getVisualEntityProxies().add(entity);
		this.notifyModelChangeListeners();
	}
	
	/**
	 * Removes a visual proxy for entity from the diagram.
	 * This will NOT remove the proxied entity from the model.
	 * @param entity the proxied entity to remove from the diagram.
	 */
	public void removeVisualEntityProxy(VisualEntityProxy entity) {
		log.debug("Removing: " + entity + " . CurrentSize: " + this.getVisualEntityProxies().size());
		this.getVisualEntityProxies().remove(entity);
		log.debug("Removed: " + entity + ". CurrentSize: " + this.getVisualEntityProxies().size());
		assert !this.getVisualEntityProxies().contains(entity);
		this.notifyModelChangeListeners();
	}

	/**
	 * Adds a real entity to the diagram. If the entity have not already
	 * been added to the model layer then it will be.
	 * A new VisualEntityProxy will be created to hold this entity.
	 * @param entity the real entity to be added to the diagram.
	 */
	public void addEntity(Entity entity) {
		//Verify if this entity is already in the model layer.
		ModelLayer modelLayer = this.getModelLayer();
		if (!modelLayer.hasEntity(entity)) {
			modelLayer.addEntity(entity);
		}
		VisualEntityProxy fake = new VisualEntityProxy();
		fake.setEntity(entity);
		this.addVisualEntityProxy(fake);
	}
	
	/**
	 * This will add BOTH the visual proxy entity to the diagram
	 * and the real proxied entity to the model
	 * @param proxy
	 */
	public void addVisualProxyAndEntity(VisualEntityProxy proxy) {
		//Verify if this entity is already in the model layer.
		ModelLayer modelLayer = this.getModelLayer();
		if (!modelLayer.hasEntity(proxy.getEntity())) {
			modelLayer.add(proxy.getEntity());
		}
		this.add(proxy);
	}
	
	/**
	 * Returns the model layer for this project.
	 * @return
	 */
	private ModelLayer getModelLayer() {
		return this.getEnclosingProject().getModelLayer();
	}

	/**
	 * Removes a real entity and its representation from the diagram. [
	 * This WILL have the side effect of removing the entity to the model.
	 * So use with care.
	 * @param entity the entity to be removed from the model and
	 * diagram.
	 */
	public void removeEntity(Entity entity) {
		VisualEntityProxy toRemove = null;
		for(VisualEntityProxy fake : visualEntityProxies) {
			if (fake.getEntity().equals(entity)) {
				toRemove = fake;
				break;
			}
		}
		if (toRemove != null) {
			//Removes the entity from the model
			this.getModelLayer().remove(entity);
			//Removes the visual proxy from the diagram.
			this.removeVisualEntityProxy(toRemove);
		}
	}
	
	/**
	 * Returns the diagram layer associated with the project.
	 * @return the diagram layer associated with the project.
	 */
	public DiagramLayer getDiagramLayer() {
		if (diagramLayer == null) {
			diagramLayer = this.getEnclosingProject().getDiagramLayer();
		}
		return diagramLayer;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof VisualEntityProxy) {
			this.addVisualEntityProxy((VisualEntityProxy) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof VisualEntityProxy) {
			this.removeVisualEntityProxy((VisualEntityProxy) child);
			return true;
		}
		return false;
	}

	/*
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		log.debug("Changed property: " + evt);
		String property = evt.getPropertyName();
		log.debug("Property: " + property);
		if (BaseModel.CHILD_REMOVED.equals(property)) {
			Object removed = evt.getOldValue();
			log.debug("Removed: " + removed);
			for(VisualEntityProxy proxy : this.getVisualEntityProxies()) {
				if (removed.equals(proxy.getEntity())) {
					log.debug("Found a proxy pointing to the entity. Remove it. Before: " + this.getVisualEntityProxies().size());
					this.remove(proxy);
					log.debug("Removed. After: " + this.getVisualEntityProxies().size());
				}
			}
		} else if (BaseModel.CHILD_ADDED.equals(property)) {
			Object added = evt.getNewValue();
			log.debug("Added: " + added);
		}
	}

	@Override
	public void setParent(StereotypedItem parent) {
		super.setParent(parent);
		log.debug("Adding listener.");
		getModelLayer().addPropertyChangeListener(this);		
	}

}

