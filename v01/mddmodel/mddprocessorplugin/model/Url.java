package mddprocessorplugin.model;

import mddprocessorplugin.model.util.StringUtils;

public class Url extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String value;
	private String type;

	public Url() {
		super("url");
	}

	public Url(String path) {
		this();
		this.value = path;
		this.setName(path);
		if (StringUtils.endsWithIgnoreCase(path, UrlType.CSS.toString())) {
			this.type = UrlType.CSS.toString();
		}
		if (StringUtils.endsWithIgnoreCase(path, UrlType.JS.toString())) {
			this.type = UrlType.JS.toString();
		}
	}

	@Override
	protected boolean addChild(StereotypedItem child) {
		return false;
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		return false;
	}

	public boolean isCss() {
		return UrlType.CSS.toString().equalsIgnoreCase(getType());
	}

	public boolean isJavaScript() {
		return UrlType.JS.toString().equalsIgnoreCase(getType());
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
