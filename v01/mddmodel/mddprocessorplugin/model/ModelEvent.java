/*
 * ModelEvent.java
 *
 * Created on September 29, 2004, 9:36 AM
 */

package mddprocessorplugin.model;

/**
 *
 * @author  Carlos Eugenio P. da Purificacao
 */
public class ModelEvent {
    
    private String propertyName;
    private Object value;
    /** Creates a new instance of ModelEvent */
    public ModelEvent() {
    }
    
    /**
     * Getter for property propertyName.
     * @return Value of property propertyName.
     */
    public java.lang.String getPropertyName() {
        return propertyName;
    }
    
    /**
     * Setter for property propertyName.
     * @param propertyName New value of property propertyName.
     */
    public void setPropertyName(java.lang.String propertyName) {
        this.propertyName = propertyName;
    }
    
    /**
     * Getter for property value.
     * @return Value of property value.
     */
    public java.lang.Object getValue() {
        return value;
    }
    
    /**
     * Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(java.lang.Object value) {
        this.value = value;
    }
    
}
