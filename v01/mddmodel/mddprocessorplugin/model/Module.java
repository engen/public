package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

public class Module extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -964955391311812751L;
	private List<Entity> entities = new ArrayList<Entity>();
	
	public Module() {
		this.setStereotypeName("moduleconfig");
	}

	/**
	 * @return the entities
	 */
	public List<Entity> getEntities() {
		return entities;
	}
	/**
	 * @param entities the entities to set
	 */
	public void setEntities(List<Entity> entities) {
		for(Entity entity : entities) {
			entity.setParent(this);
		}
		this.entities = entities;
		this.notifyModelChangeListeners();
	}
	
	public void addEntity(Entity entity) {
		entity.setParent(this);
		this.getEntities().add(entity);
		this.notifyModelChangeListeners();
	}
	
	public boolean hasEntity(Entity entity) {
		return this.getEntities().contains(entity);
	}
	
	public void removeEntity(Entity entity) {
		this.getEntities().remove(entity);
		this.notifyModelChangeListeners();
	}

	@Override
	public List<Entity> getChildren() {
		return this.getEntities();
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Entity) {
			this.addEntity((Entity) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Entity) {
			this.removeEntity((Entity) child);
			return true;
		}
		return false;
	}


}
