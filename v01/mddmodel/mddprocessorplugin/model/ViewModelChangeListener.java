package mddprocessorplugin.model;

/*
 * Interface for changes in the model.
 */
public interface ViewModelChangeListener {

	/**
	 * Notifies a change in the model. If dirty is false it has already
	 * been saved.
	 * @param model the root model for the change
	 * @param dirty if false the manager has already saved the model.
	 */
	public abstract void modelChanged(BaseModel model, boolean dirty);

	/**
	 * Notifies the listener that a property changed in the model
	 * @param baseModel the model object
	 * @param oldValue the property old value
	 * @param value the new value for the property
	 */
	public abstract void modelChanged(BaseModel baseModel, Object oldValue,
			Object value);
	
}
