/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. Todos os direitos reservados.
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/


package mddprocessorplugin.model;

/**
 * @author Carlos Eugenio P. da Purificacao
 * Created on Aug 1, 2009
 */
public enum Cardinality {
	ONE_TO_MANY ( "one-to-many" ),
	MANY_TO_ONE ( "many-to-one" ), 
	MANY_TO_MANY ( "many-to-many" );

	private String stringValue;
	Cardinality (String str) {
		stringValue = str;
	}
	
	public String toString() {
		return stringValue;
	}
	
}
