package mddprocessorplugin.model;

public interface ModelElementType {

	public ModelElementType valueOf(String type);
	
}
