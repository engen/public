package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A layer for various plugins in the system
 * 
 * @author eugenio
 * 
 */
public class PluginLayer extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Generators for this project.
	 */
	List<GeneratorPlugin> generators;

	public PluginLayer() {
		this.setStereotypeForThis();
		this.initCollections();
	}

	public PluginLayer(StereotypedItem parent) {
		super(parent);
		this.setStereotypeForThis();
		this.initCollections();
	}

	public List<GeneratorPlugin> getGenerators() {
		return generators;
	}

	public void setGenerators(List<GeneratorPlugin> generators) {
		for (GeneratorPlugin plugin : generators) {
			plugin.setParent(this);
		}
		this.generators = generators;
		this.notifyModelChangeListeners();
	}

	private void initCollections() {
		generators = new ArrayList<GeneratorPlugin>();
	}

	private void setStereotypeForThis() {
		this.setStereotypeName("pluginlayer");
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		return generators;
	}

	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof GeneratorPlugin) {
			this.generators.add((GeneratorPlugin) child);
			return true;
		}
		return false;
	}

	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof GeneratorPlugin) {
			this.generators.remove((GeneratorPlugin) child);
			return true;
		}
		return false;
	}

	public void clear() {
		this.generators.clear();
	}

}
