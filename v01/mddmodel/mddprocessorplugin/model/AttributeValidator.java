/**
 * 
 */
package mddprocessorplugin.model;


/**
 * Validator for a model attribute.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da Purificacao</a>
 * @created 01/03/2009
 * @version 1.0
 */
public class AttributeValidator extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6086397146732602069L;

	private String rule;
	private String attributeName;
	private String datePattern;
	
	public AttributeValidator() {
		super("attributevalidator");
	}
	
	public AttributeValidator(ColumnValidator cv) {
		this();
		this.setName(cv.getName());
		this.setAttributeName(cv.getName());
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}

	/**
	 * @return the rule
	 */
	public String getRule() {
		return rule;
	}

	/**
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}

	public void setDatePattern(String datePattern) {
		this.datePattern = datePattern;
	}

	public String getDatePattern() {
		return datePattern;
	}
}
