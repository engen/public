package mddprocessorplugin.model;

/*
 * Interface for changes in the model.
 */
public interface ModelChangeListener {

	/*
	 * Notifies the listener that something in the model
	 * has changed.
	 */
	public abstract void modelChanged();

	/**
	 * Notifies the listener that a property value has changed in the
	 * model.
	 * @param baseModel
	 * @param oldValue
	 * @param value
	 */
	public abstract void modelChanged(BaseModel baseModel, Object oldValue,
			Object value);
	
}
