package mddprocessorplugin.model;

public class ControllerCriteria {

	private String property;
	private String comparator;
	private String value;
	private String parameter;

	public ControllerCriteria() {
		
	}
	public ControllerCriteria(String comparator, String propertyName,
			String restrictionValue, String parameter) {
		this.property = propertyName;
		this.comparator = comparator;
		this.value = restrictionValue;
		this.parameter = parameter;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getComparator() {
		return comparator;
	}

	public void setComparator(String comparator) {
		this.comparator = comparator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	
}
