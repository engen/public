package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.db.ForeignKey;
import mddprocessorplugin.db.Reference;
import mddprocessorplugin.vdsl.BootStrapRecord;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Database extends BaseDiagram {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 9129397821496882403L;
	private static final Logger log = LoggerFactory.getLogger(Database.class);
	private String name = Defaults.DATABASE_NAME;
	private String driverClass = Defaults.DATABASE_DRIVER;
	private String dialect = Defaults.DATABASE_DIALECT;
	private String url = Defaults.DATABASE_URL;
	private String userName = Defaults.DATABASE_USER_NAME;
	private String password = StringUtils.EMPTY;
	private String schema = Defaults.DATABASE_SCHEMA;
	private String catalog = StringUtils.EMPTY;
	private String databaseProductName = StringUtils.EMPTY;
	private String databaseProductVersion = StringUtils.EMPTY;
	private String driverName = Defaults.DATABASE_DRIVER_NAME;
	private String driverVersion = StringUtils.EMPTY;
	private String tableNamePattern = StringUtils.EMPTY;
	private String definitionTypes = Defaults.DATABASE_DEFINITION_TYPES;
	/**
	 * Auto export/update schema using hbm2ddl tool. Valid values are
	 * <tt>update</tt>, <tt>create</tt>, <tt>create-drop</tt> and
	 * <tt>validate</tt> or <tt>empty string</tt>.
	 */
	private String createSchema = Defaults.DATABASE_CREATE_SCHEMA;
	private List<Table> tables = new ArrayList<Table>();
	/*
	 * The substitution keys are pairs of strings, comma separated, defining
	 * what string to look for and what string to substitute.
	 */
	private String substitutionKeys = Defaults.DATABASE_SUBSTITUTIONS;
	private List<List<BootStrapRecord>> bootStrapScripts = new ArrayList<List<BootStrapRecord>>();

	public Database() {
		super("database");
	}
	
	public String toString() {
		return "Database scripts: " + bootStrapScripts;
	}

	public Database(String databaseProductName, String databaseProductVersion,
			String driverName, String driverVersion) {
		super("database");
		this.setDatabaseProductName(databaseProductName);
		this.setDatabaseProductVersion(databaseProductVersion);
		this.setDriverName(driverName);
		this.setDriverVersion(driverVersion);
	}

	public void addRelations() throws Exception {
		for (Table source : this.getTables()) {
			for (ForeignKey fk : source.getForeignKeys()) {
				// Find the target table for this foreign key.
				Table target = this.findTable(fk.getReferencedTable());
				// Columns on the source table.
				List<ColumnMap> colsOrigin = new ArrayList<ColumnMap>();
				List<ColumnMap> colsTarget = new ArrayList<ColumnMap>();
				// For each reference we add a ColumnMap, mapping the local
				// column to the target
				// column in the foreign key, and vice-versa.
				for (Reference ref : fk.getReferences()) {
					colsOrigin.add(new ColumnMap(
							ref.getSourceForeignKeyField(), ref
									.getTargetReferencedKey()));
					colsTarget.add(new ColumnMap(ref.getTargetReferencedKey(),
							ref.getSourceForeignKeyField()));
				}
				try {
					Relation relation = new Relation(source, target, null,
							colsOrigin, colsTarget, "", "");
					source.addRelation(relation);
				} catch (Exception ex) {
					log.error("ERRO. RelationShip not created for ForeignKey ("
							+ fk + ". Table: " + fk.getReferencedTable()
							+ "): " + ex, ex);
					throw ex;
				}
			}
		}
	}

	/**
	 * Returns all the Relations in the database that have the table with the
	 * name provided as a target table.
	 * 
	 * @param tableName
	 *            the target table
	 * @return list of Relations for the target table
	 */
	public List<Relation> getRelationsToTargetTable(String tableName) {
		List<Relation> relationsToTargetTable = new ArrayList<Relation>();
		for (Table source : this.getTables()) {
			for (Relation relation : source.getRelations()) {
				// Verify if the target for the relation is the
				// same that was provided
				if (relation.getTargetTable().getName().equalsIgnoreCase(
						tableName)) {
					relationsToTargetTable.add(relation);
				}
			}
		}
		return relationsToTargetTable;
	}

	/**
	 * @return the dialect
	 */
	public String getDialect() {
		return dialect;
	}

	/**
	 * @param dialect
	 *            the dialect to set
	 */
	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	/**
	 * @return the tables
	 */
	public List<Table> getTables() {
		if (tables == null) {
			tables = new ArrayList<Table>();
		}
		return tables;
	}

	/**
	 * @param tables
	 *            the tables to set
	 */
	public void setTables(List<Table> tables) {
		for (Table table : tables) {
			table.setParent(this);
		}
		this.tables = tables;
		this.notifyModelChangeListeners();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the driverClass
	 */
	public String getDriverClass() {
		return driverClass;
	}

	/**
	 * @param driverClass
	 *            the driverClass to set
	 */
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * @param schema
	 *            the schema to set
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * @return the catalog
	 */
	public String getCatalog() {
		return catalog;
	}

	/**
	 * @param catalog
	 *            the catalog to set
	 */
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	@Override
	public List<Table> getChildren() {
		return this.getTables();
	}

	public void addTable(Table tb) {
		tb.setParent(this);
		this.tables.add(tb);
		this.notifyModelChangeListeners();
	}

	public void removeTable(Table tb) {
		if (this.tables.contains(tb)) {
			this.tables.remove(tb);
			this.notifyModelChangeListeners();
		}
	}

	/**
	 * Returns whether this database has a table.
	 * 
	 * @param t
	 *            the table
	 * @return true if this database already contains the table.
	 */
	public boolean hasTable(Table t) {
		return this.tables.contains(t);
	}

	/**
	 * @param databaseProductName
	 *            the databaseProductName to set
	 */
	public void setDatabaseProductName(String databaseProductName) {
		this.databaseProductName = databaseProductName;
	}

	/**
	 * @return the databaseProductName
	 */
	public String getDatabaseProductName() {
		return databaseProductName;
	}

	/**
	 * @param databaseProductVersion
	 *            the databaseProductVersion to set
	 */
	public void setDatabaseProductVersion(String databaseProductVersion) {
		this.databaseProductVersion = databaseProductVersion;
	}

	/**
	 * @return the databaseProductVersion
	 */
	public String getDatabaseProductVersion() {
		return databaseProductVersion;
	}

	/**
	 * @param driverName
	 *            the driverName to set
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	/**
	 * @return the driverName
	 */
	public String getDriverName() {
		return driverName;
	}

	/**
	 * @param driverVersion
	 *            the driverVersion to set
	 */
	public void setDriverVersion(String driverVersion) {
		this.driverVersion = driverVersion;
	}

	/**
	 * @return the driverVersion
	 */
	public String getDriverVersion() {
		return driverVersion;
	}

	public Table findTable(String name) {
		for (Table table : this.tables) {
			// table names are typically case insensitive
			if (table.getName().equalsIgnoreCase(name)) {
				return table;
			}
		}
		return null;
	}

	/**
	 * @param tableNamePattern
	 *            the tableNamePattern to set
	 */
	public void setTableNamePattern(String tableNamePattern) {
		this.tableNamePattern = tableNamePattern;
	}

	/**
	 * @return the tableNamePattern
	 */
	public String getTableNamePattern() {
		return tableNamePattern;
	}

	/**
	 * @param definitionTypes
	 *            the definitionTypes to set
	 */
	public void setDefinitionTypes(String definitionTypes) {
		this.definitionTypes = definitionTypes;
	}

	/**
	 * @return the definitionTypes
	 */
	public String getDefinitionTypes() {
		return definitionTypes;
	}

	/**
	 * @return the createSchema
	 */
	public String getCreateSchema() {
		return createSchema;
	}

	/**
	 * @param createSchema
	 *            the createSchema to set
	 */
	public void setCreateSchema(String createSchema) {
		this.createSchema = createSchema;
	}

	/**
	 * @param substitutionKeys
	 *            the substitutionKeys to set
	 */
	public void setSubstitutionKeys(String substitutionKeys) {
		this.substitutionKeys = substitutionKeys;
	}

	/**
	 * @return the substitutionKeys
	 */
	public String getSubstitutionKeys() {
		return substitutionKeys;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		if (child instanceof Table) {
			this.addTable((Table) child);
			return true;
		}
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		if (child instanceof Table) {
			this.removeTable((Table) child);
			return true;
		}
		return false;
	}

	public void setBootStrapScripts(List<List<BootStrapRecord>> bootStrapScripts2) {
		this.bootStrapScripts = bootStrapScripts2;
	}

	public List<List<BootStrapRecord>> getBootStrapScripts() {
		return bootStrapScripts;
	}

}
