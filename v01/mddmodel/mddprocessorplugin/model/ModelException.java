/*
 * ModelException.java
 *
 * Created on September 29, 2004, 10:47 AM
 */

package mddprocessorplugin.model;

/**
 *
 * @author  Carlos Eugenio P. da Purificacao
 */
public class ModelException extends RuntimeException {
    
    /**
	 * Auto-generated.
	 */
	private static final long serialVersionUID = -933905004860492671L;

	/** Creates a new instance of ModelException */
    public ModelException(String msg) {
        super(msg);
    }
    
    public ModelException(String msg, Throwable root) {
    	super(msg, root);
    }
    
}
