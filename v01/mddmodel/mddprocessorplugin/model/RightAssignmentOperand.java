package mddprocessorplugin.model;

import org.apache.commons.lang.StringUtils;

/**
 * The Translatable interface defines that this implementation has a Translator
 * registered in TranslatorFactory class
 * 
 * @author eugenio
 * 
 */
public class RightAssignmentOperand implements Translatable {

	private String value;
	private FunctionCall functionCall;

	public boolean isSimpleValue() {
		return StringUtils.isNotEmpty(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public FunctionCall getFunctionCall() {
		return functionCall;
	}

	public void setFunctionCall(FunctionCall functionCall) {
		this.functionCall = functionCall;
	}

	@Override
	public String internalValue() {
		if (this.isSimpleValue()) {
			return this.getValue();
		}
		// This is not a simple value, its internal value will include its
		// function name and parameters
		return this.functionCall.internalValue();
	}
}
