package mddprocessorplugin.model;

public class Assignment extends Statement {

	private String leftOperand;
	private RightAssignmentOperand rightAssignmentOperand;

	public void setLeftOperand(String value) {
		this.leftOperand = value;
	}

	public String getLeftOperand() {
		return leftOperand;
	}

	public RightAssignmentOperand getRightAssignmentOperand() {
		return rightAssignmentOperand;
	}

	public void setRightAssignmentOperand(RightAssignmentOperand rightAssignmentOperand) {
		this.rightAssignmentOperand = rightAssignmentOperand;
	}

}
