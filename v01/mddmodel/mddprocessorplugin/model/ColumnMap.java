package mddprocessorplugin.model;

/**
 * Mapping between a primary key column in a table and the foreign column in the source table.
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eug�nio P. da Purifica��o</a>
 * @created 15/03/2009
 * @version
 */
public class ColumnMap extends BaseModel {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 553903664746801103L;
	
	private String primaryKey;
    private String foreignKey;
    
    
	public ColumnMap(String primaryKey, String foreignKey) {
    	super("columnmap");
        this.setPrimaryKey(primaryKey);
        this.foreignKey = foreignKey;
    }

    
    /**
	 * @return the foreignKey
	 */
	public String getForeignKey() {
		return foreignKey;
	}


	/**
	 * @param foreignKey the foreignKey to set
	 */
	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}


	/**
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}


	/**
	 * @return the primaryKey
	 */
	public String getPrimaryKey() {
		return primaryKey;
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}

    
    
}
