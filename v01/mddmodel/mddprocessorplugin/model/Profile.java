package mddprocessorplugin.model;

import java.util.List;

public class Profile extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2581706661058550724L;

	@Override
	public List<StereotypedItem> getChildren() {
		return null;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}

	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}

}
