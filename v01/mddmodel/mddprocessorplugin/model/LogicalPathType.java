package mddprocessorplugin.model;

public enum LogicalPathType {

	JSP ( "jsp" ), 
	CONTROLLER ( "do" );
	
	private String extension;
	LogicalPathType(String ext) {
		this.extension = ext;
	}
	
	@Override
	public String toString() {
		String ret = this.getExtension();
		return ret;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Returns the extension for this resource. Typically
	 * a 'jsp' or 'do' (controllers)
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}
}
