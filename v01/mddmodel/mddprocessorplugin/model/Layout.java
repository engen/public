package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import mddprocessorplugin.model.layouts.LayoutStyle;
import mddprocessorplugin.model.util.StringUtils;

/**
 * Layout is a definition on how sections of a page will be displayed.
 * 
 * @author Carlos Eugenio P. da Purificacao
 * @created 08/03/2009
 * @version
 */
public class Layout extends BaseModel implements TemplateOwner {

	/**
	 * Generated version id.
	 */
	private static final long serialVersionUID = 2307707355117668369L;

	/** Define which layout this layout extends **/
	private String baseLayout = StringUtils.EMPTY;
	/** The path to a file with the layout definition **/
	private String path = StringUtils.EMPTY;

	/** Sections needed within this layout **/
	private List<LayoutSection> sections = new ArrayList<LayoutSection>();

	private LayoutStyle layoutStyle;
	
	public Layout() {
		super("layout");
		this.sections = new ArrayList<LayoutSection>();
	}

	@Override
	public List<LayoutSection> getChildren() {
		return this.getSections();
	}

	/**
	 * @param baseLayout
	 *            the baseLayout to set
	 */
	public void setBaseLayout(String baseLayout) {
		this.baseLayout = baseLayout;
	}

	/**
	 * @return the baseLayout
	 */
	public String getBaseLayout() {
		return baseLayout;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param sections
	 *            the sections to set
	 */
	public void setSections(List<LayoutSection> sections) {
		for (LayoutSection l : sections) {
			l.setParent(this);
		}
		this.sections = sections;
		this.notifyModelChangeListeners();
	}

	public void addSection(LayoutSection section) {
		section.setParent(this);
		this.getSections().add(section);
		this.notifyModelChangeListeners();
	}

	public void removeSection(LayoutSection layout) {
		layout.setParent(null);
		this.getSections().remove(layout);
		this.notifyModelChangeListeners();
	}

	public LayoutSection getSection(String name) {
		if (StringUtils.isNullOrEmpty(name)) {
			return null;
		}
		for (LayoutSection section : this.getSections()) {
			if (name.equals(section.getName())) {
				return section;
			}
		}
		return null;
	}

	/**
	 * @return the sections
	 */
	public List<LayoutSection> getSections() {
		return sections;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof LayoutSection) {
			this.addSection((LayoutSection) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof LayoutSection) {
			this.removeSection((LayoutSection) child);
			return true;
		}
		return false;
	}

	public LayoutStyle getLayoutStyle() {
		return layoutStyle;
	}

	public void setLayoutStyle(LayoutStyle layoutStyle) {
		this.layoutStyle = layoutStyle;
	}


}
