package mddprocessorplugin.model;

/**
 * Generic information for view widgets
 * @author eugenio
 *
 */
public interface ViewDefinition {

	/**
	 * Returns the CSS style class used by the widget
	 * @return the CSS style class to be used by the widget
	 */
	public String getStyleClass();
	
	/**
	 * Changes the CSS style class used by the widget
	 * @param styleClass the new CSS style class used by the widget
	 */
	public void setStyleClass(String styleClass);
}
