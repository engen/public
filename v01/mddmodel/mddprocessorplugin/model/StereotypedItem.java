package mddprocessorplugin.model;

import java.io.Serializable;
import java.util.List;

/**
 * Itenms that can be added to a Mdd Project.
 * 
 * @author Carlos Eugenio P. da Purificacao</a>
 * @created 01/03/2009
 * @version 1.0
 */
public interface StereotypedItem extends Serializable {

	public abstract String getStereotypeName();

	public abstract void setStereotypeName(String name);

	/**
	 * Returns the name for the element.
	 * @return
	 */
	public abstract String getName();

	public abstract void setName(String name);

	/**
	 * Returns a list of child elements for this object.
	 * 
	 * @return a list of child elements for this object.
	 */
	public abstract List<? extends StereotypedItem> getChildren();

	public abstract void addModelChangeListener(ModelChangeListener listener);

	public abstract void removeModelChangeListener(ModelChangeListener listener);

	public abstract List<ModelChangeListener> getModelChangeListeners();

	public Project getEnclosingProject();

	public StereotypedItem getParent();

	public void setParent(StereotypedItem parent);

	/**
	 * Adds a child object to this model. This method is implemented by base
	 * model so it always fire a property change on child addition.
	 * 
	 * @param child
	 *            the child to add
	 */
	void add(StereotypedItem child);

	/**
	 * Removes a child object from this model. This method is implemented by
	 * base model so it always fire a property change on child removal.
	 * 
	 * @param child
	 *            the child to remove
	 */
	void remove(StereotypedItem child);

	/**
	 * Freeze this model element configuration and all of its children
	 * recursively
	 */
	void freeze();

	/**
	 * Will resolve all proxy references on model.
	 */
	void resolveProxyRefs();

}
