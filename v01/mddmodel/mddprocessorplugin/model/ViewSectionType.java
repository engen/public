package mddprocessorplugin.model;


/**
 * Defines the type for the section in the current view model. Sections can be a
 * form, tab or display.
 * 
 * @author eugenio
 * 
 */
public enum ViewSectionType {
	/**
	 * Used when the view model defines the section type explicitly to form
	 * (e.g.: section SecName : form { ...}).
	 */
	form,
	/**
	 * Used when the view model defines the section type explicitly to tab
	 * (e.g.: section SecName : tab { ...}).
	 */
	tab,
	/**
	 * This is the default section type. If none is defined in the view model
	 * (e.g.: section SecName { ... }), then "display" will be used.
	 */
	display;

	public static ViewSectionType get(String elementTypeName) {
		for(ViewSectionType type : values()) {
			if (type.toString().equals(elementTypeName)) {
				return type;
			}
		}
		return null;
	}
	

}
