/*******************************************************************************
 * Copyright (c) 2009 Engeny Informatica Ltda
 * All rights reserved. 
 * Contributors:
 *    Carlos Eugenio P. da Purificacao - initial API and implementation
 *******************************************************************************/

package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a view artifact like a web page
 * 
 * @author Carlos Eugenio P. da Purificacao Created on Jul 27, 2009
 */
public class PresentationView extends BaseModel implements ModelTarget {

	/**
	 * Generated serial version id
	 */
	private static final long serialVersionUID = -2836787160004751917L;

	private String namespace;
	private String path;
	private String actionName;
	private boolean fromDsl;
	private String label;
	/**
	 * The view is composed by sections.
	 */
	private List<ViewSection> subSections = new ArrayList<ViewSection>();

	private Map<String, Entity> references = new HashMap<String, Entity>();

	/**
	 * Scans that can be accessed by the section and their children
	 */
	private List<ViewScan> scans = new ArrayList<ViewScan>();

	private LogicalPath logicalPath;
	
	/**
	 * 
	 */
	private PresentationView() {
		super("presentationview");
		subSections = new ArrayList<ViewSection>();
		references = new HashMap<String, Entity>();
		scans = new ArrayList<ViewScan>();
	}

	public PresentationView(StereotypedItem parent) {
		super(parent, "presentationview");
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the SubSections
	 */
	public List<ViewSection> getSubSections() {
		return subSections;
	}

	/**
	 * @param sections
	 *            the SubSections to set
	 */
	public void setSubSections(List<ViewSection> sections) {
		for (ViewSection section : subSections) {
			section.setParent(this);
		}
		this.subSections = sections;
		this.notifyModelChangeListeners();
	}

	public void addSubSection(ViewSection section) {
		section.setParent(this);
		this.getSubSections().add(section);
		this.notifyModelChangeListeners();
	}

	public void removeSubSection(ViewSection section) {
		this.getSubSections().remove(section);
		this.notifyModelChangeListeners();
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof ViewSection) {
			this.addSubSection((ViewSection) child);
			return true;
		}
		if (child instanceof ViewScan) {
			this.addViewScan((ViewScan) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof ViewSection) {
			this.removeSubSection((ViewSection) child);
			return true;
		}
		if (child instanceof ViewScan) {
			this.removeViewScan((ViewScan) child);
			return true;
		}
		return false;
	}

	@Override
	public List<? extends StereotypedItem> getChildren() {
		List<StereotypedItem> children = new ArrayList<StereotypedItem>();
		if (this.scans != null) {
			children.addAll(this.scans);
		}
		if (this.subSections != null) {
			children.addAll(this.subSections);
		}
		return children;
	}

	public void setReference(String entityName, Entity entity) {
		this.references.put(entityName, entity);
	}

	public Entity getReference(String entityName) {
		return this.references.get(entityName);
	}

	public void setFromDsl(boolean fromDsl) {
		this.fromDsl = fromDsl;
	}

	public boolean isFromDsl() {
		return fromDsl;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * Returns the control module this element is child of
	 * 
	 * @return the control module this element is child of
	 */
	public ControlModule getControlModule() {
		if (this.getParent() != null) {
			return (ControlModule) this.getParent();
		}
		return null;
	}

	public List<ViewScan> getScans() {
		return scans;
	}

	/**
	 * @param scan
	 *            the ViewScan to set
	 */
	public void setScans(List<ViewScan> subScans) {
		for (ViewScan scan : subScans) {
			scan.setParent(this);
		}
		this.scans = subScans;
		this.notifyModelChangeListeners();
	}

	public void addViewScan(ViewScan scan) {
		scan.setParent(this);
		this.getScans().add(scan);
		this.notifyModelChangeListeners();
	}

	public void removeViewScan(ViewScan scan) {
		this.getScans().remove(scan);
		this.notifyModelChangeListeners();
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public LogicalPath getLogicalPath() {
		return logicalPath;
	}

	public void setLogicalPath(LogicalPath logicalPath) {
		this.logicalPath = logicalPath;
	}

	/**
	 * Returns a list of ViewSection which are forms
	 * @return
	 */
	public List<ViewSection> getForms() {
		List<ViewSection> forms = new ArrayList<ViewSection>();
		List<ViewSection> subSections = getSubSections();
		fillFormsForSubSections(forms, subSections);
		return forms;
	}

	private void fillFormsForSubSections(List<ViewSection> forms,
			List<ViewSection> subSections) {
		for (ViewSection section : subSections) {
			// Recurse on subsections
			fillFormsForSubSections(forms, section.getSubSections());
			// Adds the form section to the forms collection
			if (section.isForm()) {
				forms.add(section);
			}
		}
	}

}
