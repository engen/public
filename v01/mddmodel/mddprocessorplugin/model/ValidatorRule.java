/**
 * 
 */
package mddprocessorplugin.model;

/**
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da Purificacao</a>
 * @created May 24, 2009
 * @version 
 */
public enum ValidatorRule {

	REQUIRED,
	MAXLENGTH,
	MINLENGTH, INTEGER, BYTE, SHORT, LONG, FLOAT, DOUBLE, DATE, TIME,
	INTRANGE, FLOATRANGE, DOUBLERANGE, MASK, CREDITCARD, EMAIL, URL;

	@Override
	public String toString() {
		String ret = super.toString();
		return ret.toLowerCase();
	}
	
	
}
