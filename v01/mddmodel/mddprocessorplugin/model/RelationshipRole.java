
package mddprocessorplugin.model;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;

import mddprocessorplugin.db.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Esta classe representa uma rela��o entre duas tabelas. Se a rela��o e de 1:1
 * ou de 1:n, a tabela esquerda deve sempre ser o lado 1 com a chave preliminar
 *
 */
/**
 * Represents a relation between two tables.
 */
public class RelationshipRole extends BaseModel {
    /**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -1571336036245121134L;

	private static int contador = 1;

    private RelationshipRole targetRole;
    private Relation relation;
    private final Table origin;    
    private final Table target;    
    private final List<ColumnMap> columnsMap;
    private boolean isTargetMany;
    private final String suffix;
    private String namePattern;
    private boolean foreignKeyIsPrimaryKeyOnTarget;
    private static Logger log = LoggerFactory.getLogger(RelationshipRole.class);    
    
    /**
     * Creates a new instance of RelationshipRole
     */
    public RelationshipRole(Table origin, Table target,  List<ColumnMap> columnsMap,Relation relation, String suffix) {
        contador ++;
        if (suffix == null) {
            throw new IllegalArgumentException("Sufix cant be null!");
        }
        if (relation == null) {
            throw new IllegalArgumentException("Relation cant be null!");
        }        
        this.origin = origin;
        this.target = target;
        
        this.columnsMap = columnsMap;
        
        this.relation = relation;
        this.suffix = suffix;
        
        if (getOrigin() == getTarget()) {            
            if (getRelation().getLeftRole() == null) {
                namePattern = "{0}-1-has-{1}-2" + suffix;
            } else {
                namePattern = "{0}-2-has-{1}-1" + suffix;
            }
        } else {
            namePattern = "{0}-has-{1}" + suffix;
        }        
        setMultiplicity(origin, target, columnsMap, relation);
    }


	private void setMultiplicity(Table origin, Table target, List<ColumnMap> columnsMap,
			Relation relation) {
		// Set multiplicity
        if (relation.getJoinTable() == null) {
        	//Verify if the target table have references to the origin table.
            if (target.getCountReferencesForTable(origin) != 0) {                
                int targePrimaryKeysCount = target.getCountPrimaryKeyColumns();
                int originForeignKeyReferencesToTargetTableCount = origin.getCountReferencesForTable(target);
                //Verify if all primary keys on the target table referenced by this foreign key
                //are referenced as foreign keys in the origin table.
                if (targePrimaryKeysCount != originForeignKeyReferencesToTargetTableCount) {
                    foreignKeyIsPrimaryKeyOnTarget = false;
                } else {
                    foreignKeyIsPrimaryKeyOnTarget = true;                    
                    for (ColumnMap cm : columnsMap ) {
                    	//The column name on the target table of the relation ship.
                        String columnName =  cm.getForeignKey();
                        //If the target column isn't a primary key not all fields in the target
                        //table primary key are being referenced by this foreign key.
                        if (!target.getColumn(columnName).isPrimaryKey()) {
                            //_log.trace("NAO E PK COLUMN NAME:"+columnName);
                            foreignKeyIsPrimaryKeyOnTarget = false;
                            break;
                        }
                    }
                }                
                setTargetMany(!foreignKeyIsPrimaryKeyOnTarget);
            } else {
                setTargetMany(false);
            }
        } else {
            // many-to-many if a joint table exists
            setTargetMany(true);
        }
	}
    
    
    /**
     * Altera o relacionamento destino.
     *
     * @param targetRole The new TargetRole value
     */
    public void setTargetRole(RelationshipRole targetRole) {
    	this.targetRole = targetRole;
    }
    
    
    /**
     * Setter for property isFkPk.
     *
     * @param isFkPk New value of property isFkPk.
     */
    public void setFkPk(boolean foreignKeyIsPrimaryKeyOnTarget) {
    	this.foreignKeyIsPrimaryKeyOnTarget = foreignKeyIsPrimaryKeyOnTarget;
    }
    
    
    /**
     * Setter for property relation.
     *
     * @param relation New value of property relation.
     */
    public void setRelation(Relation relation) {
    	this.relation = relation;
    }
    
    
    
    
    /**
     * Setter for property isTargetMany.
     *
     * @param isTargetMany New value of property isTargetMany.
     */
    public void setTargetMany(boolean isTargetMany) {
        this.isTargetMany = isTargetMany;
        //_log.trace("***************>>>   "+_isTargetMany+" = "+isTargetMany);
    }
    
    
    /**
     * Setter for property namePattern.
     *
     * @param namePattern New value of property namePattern.
     */
    public void setNamePattern(java.lang.String namePattern) {
        this.namePattern = namePattern;
    }
    
    
    /**
     * Gets the relation name.
     *
     * @return The Name value
     */
    public String getName() {
        String name = MessageFormat.format(namePattern, (Object[])new String[]
        {origin.getName()+"."+getOriginColumnsName()[0], target.getName()+"."+getTargetColumnsName()[0]});
        //  xxx++;
        return name;
    }
    
    
    /**
     * Obt�m o relacionamento destino
     *
     * @return uma inst�ncia de RelationshipRole.
     */
    public RelationshipRole getTargetRole() {
        // _log.trace("TARGET ROLE DE "+getName()+" E:"+_targetRole);
        return targetRole;
    }
    
    
    /**
     * Getter for property isFkPk.
     *
     * @return Value of property isFkPk.
     */
    public boolean isForeignKeyPrimaryKeyOnTarget() {
        return foreignKeyIsPrimaryKeyOnTarget;
    }
    
    
    /**
     * Getter for property relation.
     *
     * @return Value of property relation.
     */
    public Relation getRelation() {
        return relation;
    }
    
    
    /**
     * Getter for property origin.
     *
     * @return Value of property origin.
     */
    public Table getOrigin() {
        return origin;
    }
    
    
    
    /**
     * Getter for property target.
     *
     * @return Value of property target.
     */
    public Table getTarget() {
        return target;
    }
    
    
    
    
    /**
     * Getter for property isTargetMany.
     *
     * @return Value of property isTargetMany.
     */
    public boolean isTargetMany() {
        return isTargetMany;
    }
    
    
    /**
     * Getter for property suffix.
     *
     * @return Value of property suffix.
     */
    public java.lang.String getSuffix() {
        return suffix;
    }
    
    
    
    /**
     * Getter for property namePattern.
     *
     * @return Value of property namePattern.
     */
    public java.lang.String getNamePattern() {
        return namePattern;
    }
    
    
    /**
     * Gets the OriginMany attribute of the RelationshipRole object
     *
     * @return The OriginMany value
     */
    public boolean isOriginMany() {
        if(getTargetRole() != null)
            return getTargetRole().isTargetMany();
        else
            return  false;
    }
    
    
    /**
     * Gets the OriginPrimaryKey attribute of the RelationshipRole object
     *
     * @return The OriginPrimaryKey value
     */
    public boolean isOriginPrimaryKey() {
        return target.getCountReferencesForTable(origin) != 0;
    }
    
    
    /**
     * Gets the TargetPrimaryKey attribute of the RelationshipRole object
     *
     * @return The TargetPrimaryKey value
     */
    public boolean isTargetPrimaryKey() {
        return origin.getCountReferencesForTable(target) != 0;
    }
    
    
    /**
     * Um relationhsipRole eh obrigatorio se pertencer ao lado do fk e ao menos
     * uma das colunas do fk nao puder ser nula.
     *
     * @return true ou false
     */
    public boolean isMandatory() {
        boolean result = false;        
        if (isOriginPrimaryKey()) {
            return result;
        } else {
            try {
                for (Reference ref : this.target.getReferencesForTable(this.origin)) {
                    Column sourceForeignKeyColumn = (Column)origin.findColumn((ref).getTargetReferencedKey());
                    if (sourceForeignKeyColumn.isRequired()) {
                        result = true;
                        break;
                    }
                }
            } catch (Exception e) {
                log.error("isMandatory: "+e, e);
                throw new RuntimeException("Exceptio: " + e);
            }
            
        }
        return result;
    }
    
    
    /**
     * Um relationhsipRole � obrigat�rio se pertencer ao lado do fk e ao menos
     * uma das colunas do fk n�o puder ser nula.
     *
     * @return true ou false
     */
    public String[] getOriginColumnsName() {
        java.util.Collection cols= new java.util.ArrayList();
        //Table table = getOrigin();
        boolean result = false;
        try {
            //_log.trace("%%%%%%%%%%%%% ORIGINS REFERENCIAS: ORI:"+_origin.getName()+"  TAR:"+_target.getName());
            //            for (Iterator iter = _target.getReferencesForTable(_origin).iterator(); iter.hasNext(); )
            for (Iterator iter = columnsMap.iterator(); iter.hasNext(); )
                
            {
                Object obj = iter.next();
                // _log.trace("      %%%% "+obj);
                
                //                            Reference ref =(Reference)iter.next();
                //            Column co = (Column)_origin.findColumn(ref.getForeign());
                //            Column ct = (Column)_target.findColumn(ref.getForeign());
                ColumnMap columnMap = (ColumnMap)obj;//_origin.findColumn(((Reference)iter.next()).getForeign());
                // _log.trace("      %%%% "+columnMap.getPrimaryKey());
                if(columnMap != null)
                    cols.add(columnMap.getPrimaryKey());
                
            }
        } catch (Exception e) {
            log.error("getOriginColumnsName: "+e);
        }
        return (String[])cols.toArray(new String[0]);
        
    }
    
    public String[] getTargetColumnsName() {
        java.util.Collection cols =new java.util.ArrayList();
        
        boolean result = false;
        try {
            //_log.trace("%%%%%%%%%%%%% TARGETS REFERENCIAS: ORI:"+_origin.getName()+"  TAR:"+_target.getName());
            //            for (Iterator iter = _target.getReferencesForTable(_origin).iterator(); iter.hasNext(); )
            for (Iterator iter = columnsMap.iterator(); iter.hasNext(); ) {
                Object obj = iter.next();
                //_log.trace("      %%%% "+obj);
                ColumnMap columnMap = (ColumnMap)obj;//_origin.findColumn(((Reference)iter.next()).getForeign());
                //_log.trace("      %%%% "+columnMap.getForeignKey());
                if(columnMap != null)
                    cols.add(columnMap.getForeignKey());
            }
        } catch (Exception e) {
            log.error("getTargetColumnsName: "+e);
        }
        return (String[])cols.toArray(new String[0]);
    }
    
    /**
     * Describe what the method does @todo-javadoc Write javadocs for method
     *
     * @param o Describe what the parameter does @todo-javadoc Write javadocs for
     *      method parameter
     * @return Describe the return value @todo-javadoc Write javadocs for return
     *      value
     */
    public boolean equals(Object o) {
        if (o instanceof RelationshipRole) {
            RelationshipRole other = (RelationshipRole)o;
            return getName().equals(other.getName());
        } else {
            return false;
        }
    }
    
    
    /**
     * Describe what the method does
     *
     * @todo-javadoc Write javadocs for method
     * @todo-javadoc Write javadocs for return value
     * @return Describe the return value
     */
    public int hashCode() {
        return (getName()+contador).hashCode();
    }
    
    
    /** Describe what the method does @todo-javadoc Write javadocs for method */
    public void print() {
        
        //_log.trace("getName(): "+getName());
        // _log.trace("isIsFkPk(): "+isFkPk());
        //_log.trace("isTargetMany(): "+isTargetMany());
        // _log.trace("getSuffix(): "+getSuffix());
        // _log.trace("getNamePattern(): "+getNamePattern());
        // _log.trace("isOriginMany(): "+isOriginMany());
        // _log.trace("isOriginPrimaryKey(): "+isOriginPrimaryKey());
        // _log.trace("isTargetPrimaryKey(): "+isTargetPrimaryKey());
        // _log.trace("isMandatory(): "+isMandatory());
        
        //  _log.trace("");
        //  _log.trace("");
        //  _log.trace("");
    }


	/*
	 * @see mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean addChild(StereotypedItem child) {
		return false;
	}


	/*
	 * @see mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model.StereotypedItem)
	 */
	@Override
	protected
	boolean removeChild(StereotypedItem child) {
		return false;
	}
}
