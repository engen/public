package mddprocessorplugin.model;

import java.util.Comparator;

/**
 * Model objects that are ordered inside their containers return their
 * comparators so the objects can be ordered by the view.
 * 
 * @author <a href="mailto:eugenio@engeny.com.br">Carlos Eugenio P. da
 *         Purificacao</a>
 * @created 15/03/2009
 * @version
 */
public interface Ordered<T> {

	Comparator<T> getComparator();

}
