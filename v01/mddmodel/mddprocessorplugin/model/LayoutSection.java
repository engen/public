package mddprocessorplugin.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * LayoutSections are composite patterns for layouts.
 * 
 * @author Carlos Eugenio P. da Purificacao
 * @created 08/03/2009
 * @version 1.0
 */
public class LayoutSection extends BaseModel implements TemplateOwner {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = -6701052129641358013L;
	/**
	 * The value, usually the path (for a page for example), for this section to
	 * render.
	 */
	private String path = StringUtils.EMPTY;
	/**
	 * Controls the order this section should be rendered in the layout
	 */
	private String order = StringUtils.EMPTY;
	/**
	 * Controls how much columns this section will span
	 */
	private String columnSpan = StringUtils.EMPTY;
	/**
	 * Controls how many rows this section will span.
	 */
	private String rows = StringUtils.EMPTY;
	
	private String before = StringUtils.EMPTY;
	
	private String after = StringUtils.EMPTY;
	
	private List<Library> libraries = new ArrayList<Library>();
	
	/**
	 * LayoutSections may have inner sections.
	 */
	private List<LayoutSection> sections = new ArrayList<LayoutSection>();

	public LayoutSection() {
		super("section");
		this.setLibraries(new ArrayList<Library>());
	}

	@Override
	public List<Library> getChildren() {
		return this.getLibraries();
	}

	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * @return the columns
	 */
	public String getColumns() {
		return columnSpan;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(String columns) {
		this.columnSpan = columns;
	}

	/**
	 * @return the rows
	 */
	public String getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(String rows) {
		this.rows = rows;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#addChild(mddprocessorplugin.model.
	 * StereotypedItem)
	 */
	@Override
	protected boolean addChild(StereotypedItem child) {
		if (child instanceof Library) {
			this.addLibrary((Library) child);
			return true;
		}
		if (child instanceof LayoutSection) {
			this.addSection((LayoutSection) child);
			return true;
		}
		return false;
	}

	/*
	 * @see
	 * mddprocessorplugin.model.BaseModel#removeChild(mddprocessorplugin.model
	 * .StereotypedItem)
	 */
	@Override
	protected boolean removeChild(StereotypedItem child) {
		if (child instanceof Library) {
			this.removeLibrary((Library) child);
			return true;
		}
		if (child instanceof LayoutSection) {
			this.removeSection((LayoutSection) child);
			return true;
		}
		return false;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void addLibrary(Library library) {
		library.setParent(this);
		this.getLibraries().add(library);
		this.notifyModelChangeListeners();
	}
	
	public void removeLibrary(Library library) {
		library.setParent(null);
		this.getLibraries().remove(library);
		this.notifyModelChangeListeners();
	}
	
	public Library getLibrary(final String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		for(Library library : this.getLibraries()) {
			if (name.equals(library.getName())) {
				return library;
			}
		}
		return null;
	}

	public List<Library> getLibraries() {
		return libraries;
	}

	public void setLibraries(List<Library> libraries) {
		this.libraries = libraries;
	}

	/**
	 * @param sections
	 *            the sections to set
	 */
	public void setSections(List<LayoutSection> sections) {
		for (LayoutSection l : sections) {
			l.setParent(this);
		}
		this.sections = sections;
		this.notifyModelChangeListeners();
	}

	public void addSection(LayoutSection section) {
		section.setParent(this);
		this.getSections().add(section);
		this.notifyModelChangeListeners();
	}

	public void removeSection(LayoutSection layout) {
		layout.setParent(null);
		this.getSections().remove(layout);
		this.notifyModelChangeListeners();
	}

	public LayoutSection getSection(String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		for (LayoutSection section : this.getSections()) {
			if (name.equals(section.getName())) {
				return section;
			}
		}
		return null;
	}

	/**
	 * @return the sections
	 */
	public List<LayoutSection> getSections() {
		return sections;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}
	
}
